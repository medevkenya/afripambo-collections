<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Shopproductsizes extends Model
{
    protected $table = 'shopproductsizes';

    public static function getAll($shopProductId)
    {
          return Shopproductsizes::select('shopproductsizes.*','sizes.sizeName')
          ->where('shopproductsizes.shopProductId',$shopProductId)
          ->leftJoin('sizes','shopproductsizes.sizeId','=','sizes.id')
          ->where('shopproductsizes.isDeleted',0)->get();
    }

}
