<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Userpermissions;
use App\Activities;

class Customers extends Model
{
    protected $table = 'customers';

    public static function getAll()
    {
        return Customers::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();
    }

    public static function storeone($customerName,$email,$contacts,$amountOwed)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Customers;
        $model->customerName = $customerName;
        $model->email = $email;
        $model->contacts = $contacts;
        $model->amountOwed = $amountOwed;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
        Activities::saveLog("Added new customer [".$customerName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $customerName,$email,$contacts,$amountOwed)
    {
        $model = Customers::find($id);
        $model->customerName = $customerName;
        $model->email = $email;
        $model->contacts = $contacts;
        $model->amountOwed = $amountOwed;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited customer [".$customerName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Customers::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted customer [".$id."]");
        }
        return false;
    }

}
