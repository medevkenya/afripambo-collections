<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Shopproductcolors extends Model
{
    protected $table = 'shopproductcolors';

    public static function getAll($shopProductId)
    {
          return Shopproductcolors::select('shopproductcolors.*','colors.colorName')
          ->where('shopproductcolors.shopProductId',$shopProductId)
          ->leftJoin('colors','shopproductcolors.colorId','=','colors.id')
          ->where('shopproductcolors.isDeleted',0)->get();
    }

}
