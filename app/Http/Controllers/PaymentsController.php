<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Costs;
use App\Payments;
use App\Appointments;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class PaymentsController extends Controller {

	public function payments()
	{
		$list = Payments::select('payments.*','users.firstName','users.lastName','services.name as serviceName')
					->leftJoin('users','payments.patientId','=','users.id')
					->leftJoin('appointments','payments.appointmentId','=','appointments.id')
					->leftJoin('services','appointments.serviceId','=','services.id')
					->where('payments.adminId',Auth::user()->adminId)
					->whereRaw('Date(payments.created_at) = CURDATE()')
					->where('payments.isDeleted',0)
					->orderBy('payments.id','DESC')
					->get();
		return view('payments',['list'=>$list]);
	}

	public function paymentsReport(Request $request)
	{
			$fromdate = $request->fromdate;
			$fromdate = date('Y-m-d', strtotime("-1 day", strtotime($fromdate)));
			$fromdate = date("Y-m-d", strtotime($fromdate))." 23:59:59";
			$todate = $request->todate;
			$todate = date('Y-m-d', strtotime("+1 day", strtotime($todate)));
			$todate = date("Y-m-d", strtotime($todate))." 23:59:59";

			$list = Payments::select('payments.*','users.firstName','users.lastName','services.name as serviceName')
			      ->leftJoin('users','payments.patientId','=','users.id')
			      ->leftJoin('appointments','payments.appointmentId','=','appointments.id')
			      ->leftJoin('services','appointments.serviceId','=','services.id')
			      ->where('payments.adminId',Auth::user()->adminId)
						->whereBetween('payments.created_at', [$fromdate, $todate])
			      ->where('payments.isDeleted',0)
			      ->orderBy('payments.id','DESC')
			      ->get();

			return view('payments',['list'=>$list,'fromdate'=>$fromdate,'todate'=>$todate]);
	}

}
