<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Suppliers;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class SuppliersController extends Controller {

	public function suppliers()
	{
		$list = Suppliers::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
		return view('suppliers',['list'=>$list]);
	}

	public function addsupplier(Request $request)
	{
			$adminId	= Auth::user()->adminId;
			$supplierName = $request->supplierName;
			$email = $request->email;
			$contacts = $request->contacts;

			$check = Suppliers::where('supplierName', $supplierName)->where('adminId',$adminId)->where('isDeleted', 0)->first();
			if ($check) {
					return Redirect::back()->with(['status0'=>'Record already exists.']);
			} else {
					$add = Suppliers::storeone($supplierName,$email,$contacts);
					if ($add) {
							return Redirect::back()->with(['status1'=>'New record was created successfully.']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while creating record.']);
					}
			}
	}


	public function editsupplier(Request $request)
	{
			$id = $request->id;
			$supplierName = $request->supplierName;
			$email = $request->email;
			$contacts = $request->contacts;

					$update = Suppliers::updateone($id, $supplierName,$email,$contacts);
					if ($update) {
							return Redirect::back()->with(['status1'=>'The record was updated successfully']);
					} else {
							return Redirect::back()->with(['status0'=>'Error occurred while updating record']);
					}

	}


	public function deletesupplier(Request $request)
	{
			$id = $request->id;
			$delete = Suppliers::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
