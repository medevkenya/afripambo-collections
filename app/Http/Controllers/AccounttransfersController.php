<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Accounttransfers;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class AccounttransfersController extends Controller {

	public function accounttransfers()
	{
		$list = Accounttransfers::getAll();
		return view('accounttransfers',['list'=>$list]);
	}

	public function addaccounttransfer(Request $request)
	{

			$accountId = $request->accountId;
			$amount = $request->amount;

			$add = Accounttransfers::storeone($accountId,$amount);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New account transfer was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating account transfer.']);
			}

	}


	public function editaccounttransfer(Request $request)
	{
			$id = $request->id;
			$accountId = $request->accountId;
			$amount = $request->amount;
			$update = Accounttransfers::updateone($id,$amount);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The account transfer was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating account transfer']);
			}

	}

	public function deleteaccounttransfer(Request $request)
	{
			$id = $request->id;
			$delete = Accounttransfers::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
