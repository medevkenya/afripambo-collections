<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Orders;
use Mail;
use App\Payments;

class C2BController extends Controller
{

    /* MPESA To be whitelisted
    196.201.214.200
    196.201.214.206
    196.201.214.207
    196.201.214.208
    196.201.213.44
    196.201.213.114
    */

    /**
    * This is used to generate tokens for the live environment
    * @return mixed
    */
    public static function generateLiveToken() {

        try {
            $consumer_key = env("C2B_CONSUMER_KEY");
            $consumer_secret = env("C2B_CONSUMER_SECRET");
        } catch (\Throwable $th) {
            $consumer_key = self::env("C2B_CONSUMER_KEY");
            $consumer_secret = self::env("C2B_CONSUMER_SECRET");
        }

        if(!isset($consumer_key)||!isset($consumer_secret)){
            die("please declare the consumer key and consumer secret as defined in the documentation");
        }
        $url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        $credentials = base64_encode($consumer_key.':'.$consumer_secret);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic '.$credentials)); //setting a custom header
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $curl_response = curl_exec($curl);

        return json_decode($curl_response)->access_token;

    }

    public function registerC2BUrls()
    {
        $live = env("application_status");

        if ($live == "live") {
          $url = 'https://api.safaricom.co.ke/mpesa/c2b/v1/registerurl';
          $token = self::generateLiveToken();
        } elseif ($live == "sandbox") {
          $url = 'https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl';
          $token = self::generateSandBoxToken();
        } else {
          return json_encode(["Message" => "invalid application status"]);
        }

        $ValidationURL = env("C2B_VALIDATION_URL");
        $ConfirmationURL = env("C2B_CONFIRMATION_URL");
        $ResponseType = "Completed";
        $ShortCode = env("C2B_SHORTCODE");

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $token));
        $curl_post_data = array(
          'ValidationURL' => $ValidationURL,
          'ConfirmationURL' => $ConfirmationURL,
          'ResponseType' => $ResponseType,
          'ShortCode' => $ShortCode
        );
        $data_string = json_encode($curl_post_data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $curl_response = curl_exec($curl);

        Log::info("raw registerurls---" . json_encode($curl_response));

        var_dump($curl_response);
        //return json_decode($curl_response);
    }

    public function C2Bvalidation()
    {
      header('Access-Control-Allow-Origin: *');
      $postdata = file_get_contents("php://input");
      $request = json_decode($postdata);

      Log::info("raw c2bvalidation---" . json_encode($request));

        $ResultCode = 0;
        $ResultDesc = "Accepted";

	     log::info("c2b validation---ResultCode-".$ResultCode."---".$ResultDesc);

      return array("ResultCode" => $ResultCode, "ResultDesc" => $ResultDesc);

  }

  public function C2Bconfirmation()
  {
    header('Access-Control-Allow-Origin: *');
    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    Log::info("raw c2bconfirmation From MPESA---" . json_encode($request));

    if(isset($request->TransID))
    {

		    $transactionType=@$request->TransactionType;
        $transID=$request->TransID;
        $transTime=$request->TransTime;
        $transAmount=$request->TransAmount;
        $businessShortCode=$request->BusinessShortCode;
        $billRefNumber=$request->BillRefNumber;
        $invoiceNumber=$request->InvoiceNumber;
        $orgAccountBalance=$request->OrgAccountBalance;
        $thirdPartyTransID=$request->ThirdPartyTransID;
        $MSISDN=$request->MSISDN;
        $firstName=$request->FirstName;
        $middleName=$request->MiddleName;
        $lastName=$request->LastName;
        $os_name = $firstName." ".$middleName." ".$lastName;

        Orders::where('orderNo',$billRefNumber)->update(['paid'=>'paid']);

        Payments::updatePaymentStatus($billRefNumber,0,$transID,json_encode($request));

        return array("C2BPaymentConfirmationResult" => "Success");

    }

  }

  public function requestMpesaStk(Request $request)
  {

    if(!empty($request->password) && !empty($request->email)) {
      User::saveNew($request->mobileNo,$request->email,$request->password,$request->firstName,$request->lastName);
    }

    Orders::updateOrderByNo("mpesa",$request->orderNo,$request->address,$request->latitude,$request->longitude,$request->mobileNo,$request->email,$request->firstName,$request->lastName);

    $mobileNo = "254" . substr($request->mobileNo, -9);

    $BusinessShortCode = env("C2B_SHORTCODE");
    $LipaNaMpesaPasskey = env("LipaNaMpesaPasskey");
    $TransactionType = "CustomerPayBillOnline";
    $PartyB = env("C2B_SHORTCODE");
    $PartyA = $mobileNo;
    $CallBackURL = env("C2B_CONFIRMATION_URL");
    $Remark = "Payment";

    $url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
    $token = self::generateLiveToken();

    log::info("generateLiveToken----".$token);

    $timestamp = '20' . date("ymdhis");
    $password = base64_encode($BusinessShortCode . $LipaNaMpesaPasskey . $timestamp);
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $token));
    $curl_post_data = array(
      'BusinessShortCode' => $BusinessShortCode,
      'Password' => $password,
      'Timestamp' => $timestamp,
      'TransactionType' => $TransactionType,
      'Amount' => $request->total,
      'PartyA' => $mobileNo,
      'PartyB' => $PartyB,
      'PhoneNumber' => $mobileNo,
      'CallBackURL' => $CallBackURL,
      'AccountReference' => $request->orderNo,
      'TransactionDesc' => $Remark,
      'Remark' => $Remark
    );
    log::info("curl_post_data----".json_encode($curl_post_data));
    $data_string = json_encode($curl_post_data);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($curl, CURLOPT_HEADER, false);
    $curl_response = curl_exec($curl);
    $result = json_decode($curl_response, true);
    curl_close($curl);
    Log::info("raw STKPush-----" . json_encode($curl_response));
    //Error occurred
    if (isset($result['errorCode'])) {
      $errorCode = $result['errorCode'];
      $errorMessage = $result['errorMessage'];
      $CheckoutRequestID = 0;
      $MerchantRequestID = 0;
      $array = array("CheckoutRequestID" => $CheckoutRequestID, "MerchantRequestID" => $MerchantRequestID, "ResultCode" => 1, "ResultDesc" => "Sorry your request was not processed. Plese try again.");

      $response = array(
          'status' => '1',
          'message'=>'Request successfully. Check '.$request->mobileNo.' for the prompt and complete payment',
          'email' => $request->mobileNo
      );

    }
    else
    {

      $CheckoutRequestID = $result['CheckoutRequestID'];
      $CustomerMessage = $result['CustomerMessage'];
      $MerchantRequestID = $result['MerchantRequestID'];
      $ResponseDescription = $result['ResponseDescription'];
      $ResponseCode = $result['ResponseCode'];
      $array = array("CheckoutRequestID" => $CheckoutRequestID, "MerchantRequestID" => $MerchantRequestID, "ResultCode" => $ResponseCode, "ResultDesc" => "Check your phone ( " . $PartyA . " ) and complete the transaction");

      $response = array(
          'status' => '0',
          'message'=>'Failed to request M-PESA prompt. Please try again or use a different payment method',
          'email' => $request->mobileNo
      );

      Payments::initiatePayment($request->total,$request->orderNo);

    }

    return response()->json($response);

  }

  public function checkPayment(Request $request) {
    if($request->ajax())
    {
    $orderNo = $request->orderNo;
    log::info("checkPayment  ---- mpesaAccountNoId---".$orderNo);

    $details = Orders::where('orderNo',$orderNo)->where('status','paid')->whereDate('created_at', DB::raw('CURDATE()'))->first();
    if($details) {
    return 200;
  }
  else {
    return 500;
  }
  }
  else {
    return 500;
  }
  }

}
