<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Products;
use App\Batches;
use App\Shopproducts;
use App\Photos;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;
use Intervention\Image\Facades\Image as Image;

class ProductsController extends Controller {

	public function getProducts(Request $request)
	{
		if (!$request->categoryId) {
		$products = '<option value="">Select Category First</option>';
		}
		else {

		$products = '<option value="">Select</option>';
		$data = Products::select('id','productName')->where('categoryId',$request->categoryId)->where('isDeleted',0)->get();
		foreach ($data as $dt) {
				$products .= '<option value="'.$dt->id.'">'.$dt->productName.'</option>';
		}

		}
		log::info("products--".$products);
		return response()->json(['products'=>$products]);
	}

	public function getShopProducts(Request $request)
	{
		if (!$request->shopId) {
		$products = '<option value="">Select Shop First</option>';
		}
		else {

			if ($request->id && !empty($request->id)) {
		$details =	Shopproducts::select('shopproducts.id','products.productName')
			->leftJoin('products','shopproducts.productId','=','products.id')
			->where('shopproducts.id',$request->id)->first();

		$products = '<option value='.$details->id.'>'.$details->productName.'</option>';
	}
	else {
		$products = '<option value="">Select</option>';
	}

		$data = Shopproducts::select('shopproducts.id','products.productName')
		->leftJoin('products','shopproducts.productId','=','products.id')
		->where('shopproducts.isDeleted',0)->orderBy('shopproducts.id','DESC')->get();
		foreach ($data as $dt) {
				$products .= '<option value="'.$dt->id.'">'.$dt->productName.'</option>';
		}

		}
		log::info("products--".$products);
		return response()->json(['products'=>$products]);
	}

	public function getShopProductsForSale(Request $request)
	{
		if (!$request->shopId) {
		$products = '<option value="">Select Shop First</option>';
		}
		else {

		$products = '<option value="">Select</option>';

		$data = Shopproducts::select('shopproducts.id','products.productName')
		->leftJoin('products','shopproducts.productId','=','products.id')
		->where('shopproducts.categoryId',$request->categoryId)->where('shopproducts.shopId',$request->shopId)
		->where('shopproducts.isDeleted',0)->orderBy('shopproducts.id','DESC')->get();
		foreach ($data as $dt) {
				$products .= '<option value="'.$dt->id.'">'.$dt->productName.'</option>';
		}

		}
		log::info("products--".$products);
		return response()->json(['products'=>$products]);
	}

	public function getProduct(Request $request)
	{
		if (!$request->productId)
		{
			$outPrice = 0;
			$availableStock = 0;
		}
		else
		{
			$data = Shopproducts::select('id','outPrice','quantity')->where('id',$request->productId)->where('status','In Stock')->where('isDeleted',0)->orderBy('id','DESC')->first();
			//$availableStock = ($data->quantity - $data->sold);
			return response()->json(['outPrice'=>$data->outPrice,'availableStock'=>$data->quantity]);
		}
		return response()->json(['outPrice'=>$outPrice,'availableStock'=>$availableStock]);
	}

	public function products()
	{
		$list = Products::getAll(0);
		return view('products',['list'=>$list]);
	}

	public function addproduct(Request $request) {

	if($request->hasFile('photoUrl1')) {
		$request->validate([
					'photoUrl1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
			]);
			//get filename with extension
			$filenamewithextension = $request->file('photoUrl1')->getClientOriginalName();
			//get filename without extension
			$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
			//get file extension
			$extension = $request->file('photoUrl1')->getClientOriginalExtension();
			//filename to store
			$filenametostore = $filename.'_'.time().'.'.$extension;
			//Upload File
			$destinationPath = public_path('/photos');
			$request->photoUrl1->move($destinationPath, $filenametostore); // uploading file to given path
			$thumbnailpath = public_path('/photos'.'/'.$filenametostore);
			$img = Image::make($thumbnailpath)->resize(400, 500, function($constraint) {
					$constraint->aspectRatio();
			});
			$img->save($thumbnailpath);
			// $imgwatermark = Image::make(public_path('/photos'.'/'.$filenametostore));
			// /* insert watermark at bottom-right corner with 10px offset */
			// $imgwatermark->insert(public_path('images/logo.png'), 'bottom-right', 10, 10);
			// $imgwatermark->save(public_path('/photos'.'/'.$filenametostore));
			$photoUrl1 = $filenametostore;
			}
			else { $photoUrl1 = null; }

		if($request->hasFile('photoUrl2')) {
	$request->validate([
				'photoUrl2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);
		$filenamewithextension2 = $request->file('photoUrl2')->getClientOriginalName();
		$filename2 = pathinfo($filenamewithextension2, PATHINFO_FILENAME);
		$extension2 = $request->file('photoUrl2')->getClientOriginalExtension();
		$filenametostore2 = $filename2.'_'.time().'.'.$extension2;
		$destinationPath2 = public_path('/photos');
		$request->photoUrl2->move($destinationPath2, $filenametostore2); // uploading file to given path
		$thumbnailpath2 = public_path('/photos'.'/'.$filenametostore2);
		$img = Image::make($thumbnailpath2)->resize(400, 500, function($constraint) {
				$constraint->aspectRatio();
		});
		$img->save($thumbnailpath2);
		$photoUrl2 = $filenametostore2;
	}
	else { $photoUrl2 = null; }

	if($request->hasFile('photoUrl3')) {
$request->validate([
		'photoUrl3' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
]);
$filenamewithextension3 = $request->file('photoUrl3')->getClientOriginalName();
$filename3 = pathinfo($filenamewithextension3, PATHINFO_FILENAME);
$extension3 = $request->file('photoUrl3')->getClientOriginalExtension();
$filenametostore3 = $filename3.'_'.time().'.'.$extension3;
$destinationPath3 = public_path('/photos');
$request->photoUrl3->move($destinationPath3, $filenametostore3); // uploading file to given path
$thumbnailpath3 = public_path('/photos'.'/'.$filenametostore3);
$img = Image::make($thumbnailpath3)->resize(400, 500, function($constraint) {
		$constraint->aspectRatio();
});
$img->save($thumbnailpath3);
$photoUrl3 = $filenametostore3;
}
else { $photoUrl3 = null; }

if($request->hasFile('photoUrl4')) {
$request->validate([
		'photoUrl4' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
]);
$filenamewithextension4 = $request->file('photoUrl4')->getClientOriginalName();
$filename4 = pathinfo($filenamewithextension4, PATHINFO_FILENAME);
$extension4 = $request->file('photoUrl4')->getClientOriginalExtension();
$filenametostore4 = $filename4.'_'.time().'.'.$extension4;
$destinationPath4 = public_path('/photos');
$request->photoUrl4->move($destinationPath4, $filenametostore4); // uploading file to given path
$thumbnailpath4 = public_path('/photos'.'/'.$filenametostore4);
$img = Image::make($thumbnailpath4)->resize(400, 500, function($constraint) {
		$constraint->aspectRatio();
});
$img->save($thumbnailpath4);
$photoUrl4 = $filenametostore4;
}
else { $photoUrl4 = null; }

			$productName = $request->productName;
			$unit = $request->unit;
			$categoryId = $request->categoryId;
			$description = $request->description;

			$saveimg = Products::storeone($productName,$description,$unit,$categoryId,$photoUrl1,$photoUrl2,$photoUrl3,$photoUrl4);
			if($saveimg) {
				return redirect()->back()->with(['success' => "Product was added successfully"]);
			}
			else {
				return redirect()->back()->with(['error' => "Failed to add product"]);
			}

	// }
	// else {
	// 	return redirect()->back()->withInput($request->input())->with('error', 'Failed to upload photo');
	// }
}

public function editproduct(Request $request) {

$details = Products::where('id',$request->id)->first();

$countphotos = 1; $allphotos = Photos::getPhotos($request->id);

if(!empty($request->photoUrlId1)) {
if($request->hasFile('photoUrl1')) {
	$request->validate([
				'photoUrl1' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
		]);
		$filenamewithextension = $request->file('photoUrl1')->getClientOriginalName();
		$filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);
		$extension = $request->file('photoUrl1')->getClientOriginalExtension();
		$filenametostore = $filename.'_'.time().'.'.$extension;
		$destinationPath = public_path('/photos');
		$request->photoUrl1->move($destinationPath, $filenametostore); // uploading file to given path
		$thumbnailpath = public_path('/photos'.'/'.$filenametostore);
		$img = Image::make($thumbnailpath)->resize(400, 500, function($constraint) {
				$constraint->aspectRatio();
		});
		$img->save($thumbnailpath);
		$photoUrl1 = $filenametostore;
		Photos::where('id',$request->photoUrlId1)->update(['photoUrl'=>$photoUrl1]);
		}
		else { $photoUrl1 = null; }
	}

if(!empty($request->photoUrlId2)) {
	if($request->hasFile('photoUrl2')) {
$request->validate([
			'photoUrl2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
	]);
	$filenamewithextension2 = $request->file('photoUrl2')->getClientOriginalName();
	$filename2 = pathinfo($filenamewithextension2, PATHINFO_FILENAME);
	$extension2 = $request->file('photoUrl2')->getClientOriginalExtension();
	$filenametostore2 = $filename2.'_'.time().'.'.$extension2;
	$destinationPath2 = public_path('/photos');
	$request->photoUrl2->move($destinationPath2, $filenametostore2); // uploading file to given path
	$thumbnailpath2 = public_path('/photos'.'/'.$filenametostore2);
	$img = Image::make($thumbnailpath2)->resize(400, 500, function($constraint) {
			$constraint->aspectRatio();
	});
	$img->save($thumbnailpath2);
	$photoUrl2 = $filenametostore2;
	Photos::where('id',$request->photoUrlId2)->update(['photoUrl'=>$photoUrl2]);
}
else { $photoUrl2 = null; }
}

if(!empty($request->photoUrlId3)) {
if($request->hasFile('photoUrl3')) {
$request->validate([
	'photoUrl3' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
]);
$filenamewithextension3 = $request->file('photoUrl3')->getClientOriginalName();
$filename3 = pathinfo($filenamewithextension3, PATHINFO_FILENAME);
$extension3 = $request->file('photoUrl3')->getClientOriginalExtension();
$filenametostore3 = $filename3.'_'.time().'.'.$extension3;
$destinationPath3 = public_path('/photos');
$request->photoUrl3->move($destinationPath3, $filenametostore3); // uploading file to given path
$thumbnailpath3 = public_path('/photos'.'/'.$filenametostore3);
$img = Image::make($thumbnailpath3)->resize(400, 500, function($constraint) {
	$constraint->aspectRatio();
});
$img->save($thumbnailpath3);
$photoUrl3 = $filenametostore3;
Photos::where('id',$request->photoUrlId3)->update(['photoUrl'=>$photoUrl3]);
}
else { $photoUrl3 = null; }
}

if(!empty($request->photoUrlId4)) {
if($request->hasFile('photoUrl4')) {
$request->validate([
	'photoUrl4' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
]);
$filenamewithextension4 = $request->file('photoUrl4')->getClientOriginalName();
$filename4 = pathinfo($filenamewithextension4, PATHINFO_FILENAME);
$extension4 = $request->file('photoUrl4')->getClientOriginalExtension();
$filenametostore4 = $filename4.'_'.time().'.'.$extension4;
$destinationPath4 = public_path('/photos');
$request->photoUrl4->move($destinationPath4, $filenametostore4); // uploading file to given path
$thumbnailpath4 = public_path('/photos'.'/'.$filenametostore4);
$img = Image::make($thumbnailpath4)->resize(400, 500, function($constraint) {
	$constraint->aspectRatio();
});
$img->save($thumbnailpath4);
$photoUrl4 = $filenametostore4;
Photos::where('id',$request->photoUrlId4)->update(['photoUrl'=>$photoUrl4]);
}
else { $photoUrl4 = null; }
}

		$id = $request->id;
		$productName = $request->productName;
		$unit = $request->unit;
		$categoryId = $request->categoryId;

		$description = $request->description;

		$saveimg = Products::updateone($id,$productName,$description,$unit,$categoryId);
		if($saveimg) {
			return redirect()->back()->with(['success' => "Product was updated successfully"]);
		}
		else {
			return redirect()->back()->with(['error' => "Failed to update product"]);
		}
}

	public function deleteproduct(Request $request)
	{
			$id = $request->id;
			$delete = Products::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
