<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Accounts;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class AccountsController extends Controller {

	public function accounts()
	{
		$list = Accounts::getAll();
		return view('accounts',['list'=>$list]);
	}

	public function addaccount(Request $request)
	{
			$accountName = $request->accountName;

			$add = Accounts::storeone($accountName);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New account was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating account.']);
			}

	}


	public function editaccount(Request $request)
	{
			$id = $request->id;
			$accountName = $request->accountName;

			$update = Accounts::updateone($id, $accountName);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The account was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating account']);
			}

	}

	public function deleteaccount(Request $request)
	{
			$id = $request->id;
			$delete = Accounts::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
