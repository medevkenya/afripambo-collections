<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Customers;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class CustomersController extends Controller {

	public function customers()
	{
		$list = Customers::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
		return view('customers',['list'=>$list]);
	}

	public function addcustomer(Request $request)
	{
			$adminId	= Auth::user()->adminId;
			$customerName = $request->customerName;
			$email = $request->email;
			$contacts = $request->contacts;

			$check = Customers::where('customerName', $customerName)->where('adminId',$adminId)->where('isDeleted', 0)->first();
			if ($check)
			{
					return Redirect::back()->with(['status0'=>'Record already exists.']);
			}
			else
			{
					$add = Customers::storeone($customerName,$email,$contacts);
					if ($add)
					{
							return Redirect::back()->with(['status1'=>'New record was created successfully.']);
					}
					else
					{
							return Redirect::back()->with(['status0'=>'Error occurred while creating record.']);
					}
			}
	}

	public function editcustomer(Request $request)
	{
			$id = $request->id;
			$customerName = $request->customerName;
			$email = $request->email;
			$contacts = $request->contacts;

			$update = Customers::updateone($id, $customerName,$email,$contacts);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The record was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating record']);
			}

	}

	public function deletecustomer(Request $request)
	{
			$id = $request->id;
			$delete = Customers::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
