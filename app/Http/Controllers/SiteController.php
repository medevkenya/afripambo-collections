<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use App\User;
use App\EmailSubscriptions;
use App\Categories;
use App\Shopproducts;
use App\Brands;
use App\Productratings;
use App\Cart;
use App\Orders;
use App\Payments;
use App\Pricing;
use App\Settings;
use Mail;
use Hash;
use Auth;
use Illuminate\Support\Facades\Vallogidator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Intervention\Image\Facades\Image as Image;
use Log;
use Session;

class SiteController extends Controller
{

    public function home()
    {
        return view('site.home');
    }

    public function signin()
    {
        return view('site.signin');
    }

    public function dosignin(Request $request)
    {

        request()->validate([
            'email' => 'required|email',
            'password' => 'required|min:6',
        ], [
            'email.required' => 'Email is required',
        ]);

        $checkuser = User::where('email',request()->email)->first();
				if($checkuser) {

          if($checkuser->isVerified == 0)
          {
            return redirect()->back()->withInput($request->input())->with(['error'=>'Your account is not activated. Please check your email and click on the activation link.']);
          }
          else
          {

          $isDisabled = $checkuser->isDisabled;
          if($isDisabled == 0)
          {

            if (Auth::attempt(['email' => request()->email, 'password' => request()->password])) {
      				   User::where('email', request()->email)->update(['isOnline' => 1]);
                 return redirect()->intended('dashboard');
      			}
            else
            {
      				   return redirect()->back()->withInput($request->input())->with(['error'=>'Login failed. Wrong email or password']);
      			}

      }
      else {
          return redirect()->back()->withInput($request->input())->with(['error'=>'This account is blocked. Please contact the site administrator at admin@rentpoa.com']);
      }
}
      }
      else {
          return redirect()->back()->withInput($request->input())->with(['error'=>'Email address not found']);
      }

    }

    public function register()
    {
        return view('site.register');
    }

    public function doRegister(Request $request)
    {
      //https://stackoverflow.com/questions/51962486/datepicker-set-maximum-date-to-18-years-ago-from-current-date
        request()->validate([
            'firstName' => 'required|min:3|max:50',
            'lastName' => 'required|min:3|max:50',
            'mobileNo' => 'required|numeric|regex:/(07)[0-9]{8}/',// input starts with 01 and is followed by 9 numbers
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'cpassword' => 'required|min:6|max:20|same:password',
            'terms' => 'required',
        ], [
            'firstName.required' => 'First Name is required',
            'firstName.min' => 'First Name must be at least 3 characters.',
            'firstName.max' => 'First Name should not be greater than 20 characters.',
            'lastName.required' => 'Last Name is required',
            'lastName.min' => 'Last Name must be at least 3 characters.',
            'lastName.max' => 'Last Name should not be greater than 20 characters.',
            'mobileNo.required' => 'Mobile Number is required',
            'mobileNo.regex' => 'Mobile number should be 10 digits starting with 07',
            'password.required' => 'Please enter your password',
            'cpassword.required' => 'Please re-enter your password',
            'terms.required' => 'Please accept our terms of service',
        ]);

        // $input = request()->except('password','confirm_password');
        // $user=new User($input);
        // $user->password=bcrypt(request()->password);
        // $user->save();
        //request()->firstName,request()->lastName,request()->mobileNo,,request()->password
        $res = User::registerUser(request()->all());
        if($res) {
          return Redirect::to('login')->with('success', 'Successfully registration. Please check your email for an activation link & click it');
        }
        else {
          return redirect()->back()->withInput($request->input())->with('error', 'Registration failed. Please try again');
        }

    }

    public function orderreturns()
    {
        return view('site.orderreturns');
    }

    public function terms()
    {
        return view('site.terms');
    }

    public function privacy()
    {
        return view('site.privacy');
    }

    public function about()
    {
        return view('site.about');
    }

  public function logout(Request $request) {
    Auth::logout();
    return redirect('/login');
  }

public function activateAccount(){

    $resetCode=input::get('c');
    $id=input::get('k');

	Log::info('activateAccount activate id--'.$id);
        Log::info('activateAccount resetCode--'.$resetCode);

    $m=User::where('id',$id)->first();

    if($m){

        $mt=User::where('id',$id)->first();
        Log::info('activate id--'.$id);
        Log::info('resetCode--'.$mt->resetCode);

        if($mt->status == 1){
        return Redirect::to('login')->with('success', 'Your account is already activated.Please login');
        }

      if($mt->resetCode==$resetCode){
        //make active

         $m=User::where('id',$id)->update(['isVerified'=>1,'resetCode'=>0]);

        return Redirect::to('login')->with('success', 'You have successfully activated your account. Login.');

      }else{
         return Redirect::to('login')->with('error', 'Sorry, we could not verify your account. Contact admin for help.');
      }

    }else{
      return Redirect::to('login')->with('error', 'Invalid verification attempt');
    }
  }

  public function subscribe(Request $request){
  //  log::info($request->email);

    $check=EmailSubscriptions::where('email',$request->email)->where('isDeleted',0)->first();
    if(!$check) {
    if(EmailSubscriptions::saveNew($request->email)) {
      $response = array(
          'status' => '1',
          'message'=>'Your email was successfully saved. Thank you for trusting us.',
          'email' => $request->email,
      );
    }
    else {
      $response = array(
          'status' => '1',
          'message'=>'Failed to save your email. Please try again',
          'email' => $request->email,
      );
    }
  }
  else {
    $response = array(
        'status' => '1',
        'message'=>'Email address [ '.$request->email.' ] already exists',
        'email' => $request->email,
    );
  }
      return response()->json($response);
  }

  public function category($slug)
  {
    $categoryDetails = Categories::select('id','categoryName')->where('slug',$slug)->where('isDeleted',0)->first();
    if(!$categoryDetails)
    {
      return redirect()->back()->with('error', 'Invalid category');
    }
    $list=Shopproducts::getByCategoryId($categoryDetails->id);
    return view('site.category',['homeproducts'=>$list,'categoryDetails'=>$categoryDetails]);
  }

  public function nearest(Request $request)
  {
    if(empty($request->latitude) || empty($request->longitude))
    {
      return redirect()->back()->with('error', 'Invalid category');
    }
    $list=Shopproducts::getNearest($request->latitude,$request->longitude);
    return view('site.nearest',['homeproducts'=>$list]);
  }

  public function hotdeals()
  {
    $list=Shopproducts::getHotDeals();
    return view('site.hotdeals',['homeproducts'=>$list]);
  }

  public function brand($slug)
  {
    $brandDetails = Brands::select('id','brandName')->where('slug',$slug)->where('isDeleted',0)->first();
    if(!$brandDetails)
    {
      return redirect()->back()->with('error', 'Invalid brand');
    }
    $list=Shopproducts::getByBrandId($brandDetails->id);
    return view('site.brand',['homeproducts'=>$list,'brandDetails'=>$brandDetails]);
  }

  public function product($slug)
  {
    $sdetails = Shopproducts::where('slug',$slug)->where('isDeleted',0)->first();
    if(!$sdetails)
    {
      return redirect()->back()->with('error', 'Invalid brand');
    }
    $details=Shopproducts::getById($sdetails->id);
    return view('site.product',['details'=>$details]);
  }

  public function submitreview(Request $request)
    {

        request()->validate([
            'name' => 'required|min:3|max:50',
            'review' => 'required|min:3|max:300',
            'rate' => 'required|numeric',// input starts with 01 and is followed by 9 numbers
            'email' => 'required|email',
            'shopProductId' => 'required'
        ], [
            'name.required' => 'Your name is required',
            'name.min' => 'Your name must be at least 3 characters.',
            'name.max' => 'Your name cannot be more than 50 characters',
            'review.min' => 'Review must be at least 3 characters.',
            'review.max' => 'Review should not be greater than 300 characters.',
        ]);

        $res = Productratings::saveone(request()->all());
        if($res) {
          return redirect()->back()->with('success', 'Your review was saved successfully');
        }
        else {
          return redirect()->back()->withInput($request->all())->with('error', 'Request failed. Please try again');
        }

    }

    public function savecart(Request $request){
     //log::info($request->items);

      if(Cart::saveNew($request->items)) {
        $response = array(
            'status' => '1',
            'message'=>'Your cart was successfully saved'
        );
      }
      else {
        $response = array(
            'status' => '0',
            'message'=>'Failed to save cart. Please try again'
        );
      }

      return response()->json($response);

    }

    public function checkout(Request $request)
    {

        $settings = Settings::getDetails();
        $standardShippingCost = $settings->standardShippingCost;

        $sessionId = Session::getId();
        $check = Orders::where('sessionId',$sessionId)->orderBy('id','DESC')->limit(1)->first();
        if($check && $check->status == "pending")
        {
          Orders::where('id',$check->id)->update(['gotToCheckout'=>1]);
          $cartlist = Cart::where('orderId',$check->id)->get();
          $total = Cart::where('orderId',$check->id)->sum('totalCost') + $standardShippingCost;
          return view('site.checkout',['orderdetails'=>$check,'cartlist'=>$cartlist,'total'=>round($total),'standardShippingCost'=>$standardShippingCost]);
        }
        else
        {
          return Redirect::to('/')->with('error', 'Request failed. There is no pending order to checkout');
        }

    }

    public function cashondelivery(Request $request)
    {
     //log::info($request->items);

     if(!empty($request->password) && !empty($request->email)) {
       User::saveNew($request->mobileNo,$request->email,$request->password,$request->firstName,$request->lastName);
     }

     Orders::updateOrderByNo("cashondelivery",$request->orderNo,$request->address,$request->latitude,$request->longitude,$request->mobileNo,$request->email,$request->firstName,$request->lastName);

      //if(Payments::cashondelivery($request->orderId,$request->orderNo,$request->total,$request->mobileNo))
      if(Payments::initiatePayment($request->total,$request->orderNo))
      {
        Orders::where('orderNo',$request->orderNo)->update(['status'=>'packaging']);
        $response = array(
            'status' => '1',
            'message'=>'Order completed successfully. Your order will be processed and delivered to your location. Note payment instructions below:<br><p><strong>M-PESA Pay Bill No.:</strong> '.env("C2B_SHORTCODE").'</p><p><strong>Account No.:</strong> '.$request->orderNo.'</p><p><strong>Amount Ksh.:</strong> '.$request->total.'</p>',
            'email' => number_format($request->total,0)
        );
      }
      else
      {
        $response = array(
            'status' => '0',
            'message'=>'Failed to complete order. Please try again or use a different payment method',
            'email' => number_format($request->total,0)
        );
      }

      return response()->json($response);

    }

    public function sendPaymentInvoice(Request $request)
    {

      if(Notifications::sendPaymentInvoices($request->orderNo))
      {
        $response = array(
            'status' => '1',
            'message'=>'Notifications sent successfully'
        );
      }
      else
      {
        $response = array(
            'status' => '0',
            'message'=>'Failed to complete order. Please try again or use a different payment method'
        );
      }

      return response()->json($response);

    }

    public function calculateDeliveryFee(Request $request)
    {

      $deliveryModeId = env("DEFAULT_DELIVERY_METHOD");

      $fee = Pricing::calculateDeliveryFee($deliveryModeId,$request->latitude,$request->longitude,$request->orderNo);

      if($fee)
      {
        $response = array(
            'status' => '1',
            'message'=>'Successfully',
            'fee'=>round($fee)
        );
      }
      else
      {
        $response = array(
            'status' => '0',
            'message'=>'Failed',
            'fee'=>round($fee)
        );
      }

      return response()->json($response);

    }

}
