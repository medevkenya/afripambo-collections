<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\User;
use App\Sales;
use App\Categories;
use App\Reportranges;
use App\Exports\FilterSaleExport;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class SaleController extends Controller {

	public function sales()
	{
		$list = Sales::select('sales.*','products.productName','products.unit','categories.categoryName','customers.customerName')
		->leftJoin('products','sales.productId','=','products.id')
		->leftJoin('categories','sales.categoryId','=','categories.id')
		->leftJoin('customers','sales.customerId','=','customers.id')
		->where('sales.adminId',Auth::user()->adminId)
		->where('sales.isDeleted',0)
		->orderBy('sales.id','DESC')
		->get();

		return view('sale',['list'=>$list]);
	}

	public function postfiltersalereport(Request $request)
	{
			$fromdate = $request->fromdate;
			$fromdate = date('Y-m-d', strtotime("-1 day", strtotime($fromdate)));
			$fromdate = date("Y-m-d", strtotime($fromdate))." 23:59:59";
			$todate = $request->todate;
			$todate = date('Y-m-d', strtotime("+1 day", strtotime($todate)));
			$todate = date("Y-m-d", strtotime($todate))." 23:59:59";

			$list = Sales::select('sales.*','products.productName','products.unit','categories.categoryName','customers.customerName')
			->leftJoin('products','sales.productId','=','products.id')
			->leftJoin('categories','sales.categoryId','=','categories.id')
			->leftJoin('customers','sales.customerId','=','customers.id')
			->where('sales.adminId',Auth::user()->adminId)
			->whereBetween('sales.created_at', [$fromdate, $todate])
			->where('sales.isDeleted',0)
			->orderBy('sales.id','DESC')->get();

			Reportranges::saveone("sale",$fromdate,$todate);

			return view('sale',['list'=>$list,'fromdate'=>$fromdate,'todate'=>$todate]);
	}

	public function editsale(Request $request)
	{

			$id = $request->id;
			$productId = $request->productId;
			$customerId = $request->customerId;
			$quantity = $request->quantity;
			$type = $request->type;
			$discount = $request->discount;

			$update = Sales::updateone($id,$productId,$customerId,$quantity,$discount,$type);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The sale was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating sale']);
			}

	}

	public function deletesale(Request $request)
	{
			$id = $request->id;
			$delete = Sales::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

	public function completesale(Request $request)
	{

		log::info("post subTotal--".$request->discount);
		// $data = Cart::getCart();
		// foreach ($data as $key) {
		// 	$check = Batches::where('productId',$key->productId)->where('status','In Stock')->where('isDeleted',0)->orderBy('id','ASC')->first();
		// 	if($check)
		// 	{
		//
		// 	}
		// }

		$res = Sales::completesale($request->total,$request->type,$request->productId,$request->discount,$request->categoryId,$request->quantity,$request->customerId);
		if($res) {
			return response()->json(['status'=>1,'success'=>'Successfully completed sale']);
		}
		else {
			return response()->json(['status'=>0,'error'=>'Failed to complete sale, please try again']);
		}

	}

	/**
	* @return \Illuminate\Support\Collection
	*/
	public function exportfiltersaleexcel($fromdate,$todate)
	{
			return Excel::download(new FilterSaleExport, 'sales.xlsx');
	}

}
