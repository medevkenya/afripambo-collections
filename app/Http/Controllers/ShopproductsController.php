<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Shopproducts;
use App\Batches;
use App\Photos;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;
use Intervention\Image\Facades\Image as Image;

class ShopproductsController extends Controller {

	public function shopproducts()
	{

		$list = Shopproducts::getAll(0);
		return view('shopproducts',['list'=>$list]);

	}

	public function addshopproduct(Request $request)
	{
			$shopId = $request->shopId;
			$productId = $request->productId;
			$categoryId = $request->categoryId;
			$productCondition = $request->productCondition;
			$quantity = $request->quantity;
			$discount = $request->discount;
			$inPrice = $request->inPrice;
			$outPrice = $request->outPrice;
			$alertLevel = $request->alertLevel;

			$saveimg = Shopproducts::storeone($shopId,$productId,$productCondition,$categoryId,$discount,$quantity,$inPrice,$outPrice,$alertLevel);
			if($saveimg)
			{
				return redirect()->back()->with(['success' => "Shop product was added successfully"]);
			}
			else
			{
				return redirect()->back()->with(['error' => "Failed to add shop product"]);
			}
	}

	public function editshopproduct(Request $request)
	{
			$id = $request->id;
			$shopId = $request->shopId;
			$productCondition = $request->productCondition;
			$categoryId = $request->categoryId;
			$discount = $request->discount;
			$productId = $request->productId;
			$inPrice = $request->inPrice;
			$outPrice = $request->outPrice;
			//$quantity = $request->quantity;
			$alertLevel = $request->alertLevel;

			$saveimg = Shopproducts::updateone($id,$shopId,$productId,$productCondition,$categoryId,$discount,$inPrice,$outPrice,$alertLevel);
			if($saveimg) {
				return redirect()->back()->with(['success' => "Shop product was updated successfully"]);
			}
			else {
				return redirect()->back()->with(['error' => "Failed to update shop product"]);
			}
	}

	public function deleteshopproduct(Request $request)
	{
			$id = $request->id;
			$delete = Shopproducts::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
