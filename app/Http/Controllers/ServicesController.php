<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Services;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class ServicesController extends Controller {

	public function services()
	{
		$list = Services::getAll();
		return view('services',['list'=>$list]);
	}

	public function addservice(Request $request)
	{
			$name = $request->name;
			$description = $request->description;
			$appointmentFee = $request->appointmentFee;

			$check = Services::where('name', $name)->where('adminId',$adminId)->where('isDeleted', 0)->first();
			if ($check) {
					return Redirect::back()->with(['status0'=>'Record already exists.']);
			} else {

			$add = Services::storeone($name,$description,$appointmentFee);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New service was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating service.']);
			}

		}

	}


	public function editservice(Request $request)
	{
			$id = $request->id;
			$name = $request->name;
			$description = $request->description;
			$appointmentFee = $request->appointmentFee;

			$update = Services::updateone($id, $name,$description,$appointmentFee);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The service was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating service']);
			}

	}

	public function deleteservice(Request $request)
	{
			$id = $request->id;
			$delete = Services::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

}
