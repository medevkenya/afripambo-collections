<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Batches;
use App\Shopproducts;
use App\Exports\FilterStockExport;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;
use Intervention\Image\Facades\Image as Image;
use Carbon\Carbon;

class BatchesController extends Controller {

	public function batches()
	{
		$list = Batches::getAll();
		return view('batches',['list'=>$list]);
	}

	public function addbatch(Request $request)
	{
			$supplierId = $request->supplierId;
			$quantity = $request->quantity;
			$shopId = $request->shopId;
			$shopProductId = $request->shopProductId;
			$type = $request->type;
			$batchNo = $request->batchNo;
			$expiryDate = $request->expiryDate;

			$newDate = date("Y-m-d", strtotime($expiryDate));

			if($newDate < date('Y-m-d')) {
				return Redirect::back()->with(['status0'=>'Expiry date must be equal or greater than today']);
			}
			else {

			$add = Batches::storeone($supplierId,$shopProductId,$quantity,$shopId,$type,$batchNo,$expiryDate);
			if ($add) {
					return Redirect::back()->with(['status1'=>'New batch was created successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while creating batch.']);
			}

		}
	}

	public function updatequantity(Request $request)
	{
			$supplierId = $request->supplierId;
			$quantity = $request->quantity;
			$shopProductId = $request->shopProductId;
			$type = $request->type;

			$add = Batches::updatequantity($supplierId,$shopProductId,$quantity,$type);
			if ($add) {
					return Redirect::back()->with(['status1'=>'Product batch was update successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating batch.']);
			}
	}

	public function ejectbatch(Request $request)
	{
			$id = $request->id;
			$quantity = $request->quantity;
			$shopProductId = $request->shopProductId;

			$shopproductdetails = Shopproducts::where('id',$id)->first();
			if($shopproductdetails->quantity < $quantity) {
				return Redirect::back()->with(['status0'=>'Insufficient shop product balance for ejecting batch.']);
			}
			else
			{

			$add = Batches::ejectbatch($id,$shopProductId,$quantity);
			if ($add) {
					return Redirect::back()->with(['status1'=>'Product batch was ejected successfully.']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while ejecting batch.']);
			}

		}
	}

	public function editbatch(Request $request)
	{
			$id = $request->id;
			$supplierId = $request->supplierId;
			$shopId = $request->shopId;
			$shopProductId = $request->shopProductId;
			$type = $request->type;
			$batchNo = $request->batchNo;
			$expiryDate = $request->expiryDate;

			$newDate = date("Y-m-d", strtotime($expiryDate));

			if($newDate < date('Y-m-d')) {
				return Redirect::back()->with(['status0'=>'Expiry date must be equal or greater than today']);
			}
			else {

			$update = Batches::updateone($id,$supplierId,$shopProductId,$shopId,$type,$batchNo,$expiryDate);
			if ($update) {
					return Redirect::back()->with(['status1'=>'The batch was updated successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while updating batch']);
			}

		}
	}

	public function deletebatch(Request $request)
	{
			$id = $request->id;
			$delete = Batches::deleteone($id);
			if ($delete) {
					return Redirect::back()->with(['status1'=>'Record was deleted successfully']);
			} else {
					return Redirect::back()->with(['status0'=>'Error occurred while deleting record']);
			}
	}

	public function stockreport()
	{

  $list = Batches::getAllProducts();

  $now = date('d-m-Y h:i A');

    $data = '<style>
  	.titles { text-align:left; font-size:16px;}
    table.imagetable {
     font-family: verdana,arial,sans-serif;
     font-size:14px;
     color:#333333;
     border-width: 1px;
     border-color: #999999;
     border-collapse: collapse;
     width:100%;
    }
    table.imagetable th {
     background:#3E4095;
     border-width: 1px;
     padding: 8px;
     border-style: solid;
     border-color: #999999;
     text-align:left;
     color:#ffffff;
    }
    table.imagetable td {
     background:#fff;
     border-width: 1px;
     padding: 8px;
     border-style: solid;
     border-color: #999999;
    }
  		</style>';

  		$pathimg = public_path('images/logo.png');
  		$data .= '<center><img src="'.$pathimg.'" style="width:15%"></center>';

  		$data .= '<h2 class=titles>Stock Summary Report as at '.$now.'</h2>';

      $count = 0;

  			$data .= '<table class="imagetable"><tr><th>Product</th><th>Quantity</th><th>Value (Ksh.)</th></tr>';
  			foreach ($list as $pay)
        {
  				$count++;
					//$quantity = $pay->quantity - $pay->sold;
					$details = Batches::getProductStockValue($pay->productId);
					$value = $details['total'];
					$quantity = $details['quantity'];
  				$data .= '<tr><td>'.$pay->productName.'</td><td>'.$quantity.'</td><td>'.number_format($value,0).'</td></tr>';
  			}

  		$data .= '</table>';

  		$pdf = PDF::loadHTML(
          $data,
          [
            'title' => 'Stock Summary Report',
            'format' => 'A4-L',
            'orientation' => 'L'
          ]);

  	return $pdf->download('Stock_Summary_Report_'.$now.''.'.pdf');

}

public function filterstockreport()
	{
		return view('filterstockreport');
	}

	public function postfilterstockreport(Request $request)
	{
			$fromdate = $request->fromdate;
			$fromdate = date('Y-m-d', strtotime("-1 day", strtotime($fromdate)));
			$fromdate = date("Y-m-d", strtotime($fromdate))." 23:59:59";
			$todate = $request->todate;
			$todate = date('Y-m-d', strtotime("+1 day", strtotime($todate)));
			$todate = date("Y-m-d", strtotime($todate))." 23:59:59";

				$list = Batches::select('batches.*','products.productName','products.unit','categories.categoryName','suppliers.supplierName')
	      ->leftJoin('suppliers','batches.supplierId','=','suppliers.id')
	      ->leftJoin('products','batches.productId','=','products.id')
	      ->leftJoin('categories','products.categoryId','=','categories.id')
	      ->where('batches.adminId',Auth::user()->adminId)
	      ->where('batches.status','In Stock')
				->whereBetween('batches.created_at', [new Carbon($fromdate), new Carbon($todate)])
				->where('batches.isDeleted',0)
				->orderBy('batches.id','DESC')
				->get();

			return view('filterstockreport',['list'=>$list,'fromdate'=>$fromdate,'todate'=>$todate]);
	}

		/**
    * @return \Illuminate\Support\Collection
    */
    public function exportfilterstockexcel($fromdate,$todate)
    {
        return Excel::download(new FilterStockExport, 'users.xlsx');
    }

		public function exportfilterstockpdf($fromdate,$todate)
    {
			$list = Batches::select('batches.*','products.productName','products.unit','categories.categoryName','suppliers.supplierName')
			->leftJoin('suppliers','batches.supplierId','=','suppliers.id')
			->leftJoin('products','batches.productId','=','products.id')
			->leftJoin('categories','products.categoryId','=','categories.id')
			->where('batches.adminId',Auth::user()->adminId)
			->where('batches.status','In Stock')
			->whereBetween('batches.created_at', [new Carbon($fromdate), new Carbon($todate)])
			->where('batches.isDeleted',0)
			->orderBy('batches.id','DESC')
			->get();

			$fromdate = date("Y-m-d", strtotime($fromdate));

			$todate = date("Y-m-d", strtotime($todate));

				$data = '<style>
				.titles { text-align:left; font-size:16px;}
				table.imagetable {
				 font-family: verdana,arial,sans-serif;
				 font-size:14px;
				 color:#333333;
				 border-width: 1px;
				 border-color: #999999;
				 border-collapse: collapse;
				 width:100%;
				}
				table.imagetable th {
				 background:#3E4095;
				 border-width: 1px;
				 padding: 8px;
				 border-style: solid;
				 border-color: #999999;
				 text-align:left;
				 color:#ffffff;
				}
				table.imagetable td {
				 background:#fff;
				 border-width: 1px;
				 padding: 8px;
				 border-style: solid;
				 border-color: #999999;
				}
					</style>';

					$pathimg = public_path('images/logo.png');
					$data .= '<center><img src="'.$pathimg.'" style="width:15%"></center>';

					$data .= '<h2 class=titles>Stock Summary Report Between '.$fromdate.' & '.$todate.'</h2>';

					$count = 0;

						$data .= '<table class="imagetable"><tr><th>Supplier Name</th><th>Product</th><th>Quantity</th><th>Value (Ksh.)</th><th>Date</th></tr>';
						foreach ($list as $pay)
						{
							$count++;
							$quantity = $pay->quantity;
							$value = $quantity * $pay->sellingPrice;
							$date = date("d-m-Y h:i A", strtotime($pay->created_at));
							$data .= '<tr><td>'.$pay->supplierName.'</td><td>'.$pay->productName.'</td><td>'.$quantity.'</td><td>'.number_format($value,0).'</td><td>'.$date.'</td></tr>';
						}

					$data .= '</table>';

					$pdf = PDF::loadHTML(
							$data,
							[
								'title' => 'Stock Summary Report',
								'format' => 'A4-L',
								'orientation' => 'L'
							]);

				return $pdf->download('Stock_Summary_Report_Between '.$fromdate.' & '.$todate.''.'.pdf');
    }

}
