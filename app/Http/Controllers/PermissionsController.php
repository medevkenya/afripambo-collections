<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Users;
use App\Permissions;
use App\Userpermissions;
use App\Roles;
use Hash;
use Session;
use PDF;
use Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Maatwebsite\Excel\Facades\Excel;

class PermissionsController extends Controller {

	public function permissions()
	{
		$roles = Roles::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
		return view('permissions',['roles'=>$roles]);
	}

	public function updatepermission(Request $request)
	{
			if($request->ajax())
			{
				$roleId = $request->roleId;
				$status = $request->status;
				$permissionId = $request->user_permission_id;

				log::info("updatepermission--roleId-".$roleId."--status-".$status."---permissionId--".$permissionId."------");

				$check = Userpermissions::where('permissionId',$permissionId)->where('roleId',$roleId)->first();
				if(!$check) {
					$model = new Userpermissions;
	        $model->permissionId = $permissionId;
					$model->roleId = $roleId;
					$model->status = $status;
					$model->adminId = Auth::user()->id;
					$model->created_by = Auth::user()->id;
	        $model->save();
					return 200;
				}
				else {

				$res = Userpermissions::where('permissionId',$permissionId)->where('roleId',$roleId)->update(['status'=>$status]);
				if($res) {
					return 200;
				}
				else {
					return 500;
				}
			}
			}

	}

	// public function updatepermission(Request $request)
	// {
	// 		if($request->ajax())
	// 		{
	// 			$status = $request->status;
	// 			$id = $request->user_permission_id;
	// 			$res = Userpermissions::where('id',$id)->update(['status'=>$status]);
	// 			if($res) {
	// 				return 200;
	// 			}
	// 			else {
	// 				return 500;
	// 			}
	// 		}
	// }


}
