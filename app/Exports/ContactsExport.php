<?php
namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\FromCollection;
use App\Contacts;

class ContactsExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function export($groupId,$adminId)
    {
      if($groupId=="All") {
        return Contacts::getAll();
      }
      else if($groupId=="CWG") {
        return Contacts::select('contacts.*','groups.groupName')
        ->leftJoin('groups','contacts.groupId','=','groups.id')->where('contacts.adminId',$adminId)
        ->where('contacts.groupId',null)->where('contacts.isDeleted', 0)->orderBy('contacts.id','DESC')->get();
      }
      else {
        return Contacts::select('contacts.*','groups.groupName')
        ->leftJoin('groups','contacts.groupId','=','groups.id')->where('contacts.adminId',$adminId)
        ->where('contacts.groupId',$groupId)->where('contacts.isDeleted', 0)->orderBy('contacts.id','DESC')->get();
      }

    }
}
