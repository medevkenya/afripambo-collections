<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Mail;
use PDF;
use App\Orders;
use App\Cart;
use App\User;
use App\Settings;

class Notifications extends Model
{
    protected $table = 'notifications';

    public static function countToday() {
      return Notifications::where('adminId',Auth::user()->adminId)->where('isDeleted', 0)->whereRaw('Date(created_at) = CURDATE()')->count();
    }

    public static function getLatest() {
      return Notifications::select('users.firstName','users.lastName','notifications.message','notifications.created_at','users.profilePic')
      ->leftJoin('users','users.id','=','notifications.userId')
      ->where('notifications.adminId',Auth::user()->adminId)
      ->where('notifications.isDeleted',0)
      ->orderBy('notifications.id','DESC')
      ->take(10)
      ->get();
    }

    public static function getAll()
    {
        return Notifications::select('users.firstName','users.lastName','notifications.message','notifications.created_at','users.profilePic')
        ->leftJoin('users','users.id','=','notifications.userId')
        ->where('notifications.adminId',Auth::user()->adminId)
        ->where('notifications.isDeleted',0)
        ->orderBy('notifications.id','DESC')
        ->get();
    }

    public static function storeone($message)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Notifications;
        $model->message = $message;
        $model->adminId = $adminId;
        $model->userId = $created_by;
        $model->save();
        if ($model)
        {
          return true;

        }

        return false;

    }

    public static function sendPaymentInvoices($orderNo)
    {

      $orderdetails = Orders::where('orderNo',$orderNo)->first();
      $email = $orderdetails->email;

      $settings = Settings::getDetails();

      $pass = time();

      $name = $orderdetails->firstName." ".$orderdetails->lastName;

      $data = array('orderNo'=>$orderNo,'name'=>$name,'cartlist'=>$cartlist,'orderdetails'=>$orderdetails,'settings'=>$settings);

      $total = Cart::where('orderId',$orderdetails->id)->sum('totalCost');

      //////////////////////////////////////////////////////////////////////////
      $data['orderNo']=$orderNo;
      $data['name']=$name;
      $data['cartlist']=$cartlist;
      $data['orderdetails']=$orderdetails;
      $data['total']=$total;
      $data['settings'] = $settings;

      $pdf = PDF::loadView('emails.invoicetemplatepdf', $data,[],['format'=>'A4-P',
      'title' => 'Invoice',
      'orientation' => 'P',
      'margin_left' => '10',
      'margin_right' => '10',
      'margin_top' => '10',
      'margin_bottom' => '10']);

      //start send mail
      $pass=str_random(32);
      //$dataR=array('pass'=>$pass,'orderNo'=>$orderNo,'name'=>$name,'cartlist'=>$cartlist,'orderdetails'=>$orderdetails);

       $subject = strtoupper($name)." - INVOICE NO. ".$orderNo;

       Mail::send('emails.invoicetemplate', $data, function($message) use ($pass,$email,$orderNo,$subject) {
         $message->to($email)->subject($subject);
         $message->from(env("MAIL_FROM_ADDRESS"),env("MAIL_FROM_NAME"));
         $message->attachData($pdf->output(), 'invoice.pdf');
       });

      //  $pdfsaved = ''.$orderNo.'filename.pdf';
      //
      //   $outputt = $pdf->output();
      // file_put_contents('public/pdfs/'.$pdfsaved, $outputt);
      //
      //       $files = [
      //
      //           public_path('pdfs/'.$pdfsaved),
      //
      //       ];
      //
      //  $paydata = Mail::send('member.invoicetemplatepdf', $dataR, function($message) use ($pass,$subject,$pdf,$files) {
      //   $message->to('admin@achrp.org')->subject($subject);
      //   $message->from('mails@achrp.org','ACHRP');
      //
      //   //$message->attach($file, array('as' => 'invoice', 'mime' => 'application/pdf'));
      //   foreach ($files as $file){
      //
      //              //$message->attach($file);
      //               $message->attach($file, array('as' => 'invoice', 'mime' => 'application/pdf'));
      //
      //           }
      //   //$message->attachData('public/pdfs/'.$pdfsaved, $subject.'.pdf');
      //
      // //$message->attachData($pdf->output(), 'invoice.pdf', ['mime' => $mime]);
      //   //$message->attachData($allpdfdata, 'invoice.pdf');
      //   });
      //   //end send mail
      //
      //   unlink(public_path('pdfs/'.$pdfsaved));
      //
      // //$pdf->download('Invoice_'.$data['totalAmount'].'_'.$data['date'].'.pdf');
      //////////////////////////////////////////////////////////////////////////

    }

    public static function sendPaymentReceipt($orderNo)
    {

      $orderdetails = Orders::where('orderNo',$orderNo)->first();
      $email = $orderdetails->email;

      $settings = Settings::getDetails();

      $pass = time();

      $name = $orderdetails->firstName." ".$orderdetails->lastName;

      $data = array('orderNo'=>$orderNo,'name'=>$name,'cartlist'=>$cartlist,'orderdetails'=>$orderdetails,'settings'=>$settings);

      $total = Cart::where('orderId',$orderdetails->id)->sum('totalCost');

      //////////////////////////////////////////////////////////////////////////
      $data['orderNo']=$orderNo;
      $data['name']=$name;
      $data['cartlist']=$cartlist;
      $data['orderdetails']=$orderdetails;
      $data['total']=$total;
      $data['settings']=$settings;

      $pdf = PDF::loadView('emails.receipttemplatepdf', $data,[],['format'=>'A4-P',
      'title' => 'Receipt',
      'orientation' => 'P',
      'margin_left' => '10',
      'margin_right' => '10',
      'margin_top' => '10',
      'margin_bottom' => '10']);

      //start send mail
      $pass=str_random(32);
      //$dataR=array('pass'=>$pass,'orderNo'=>$orderNo,'name'=>$name,'cartlist'=>$cartlist,'orderdetails'=>$orderdetails);

       $subject = strtoupper($name)." - INVOICE NO. ".$orderNo;

       Mail::send('emails.receipttemplate', $data, function($message) use ($pass,$email,$orderNo,$subject) {
         $message->to($email)->subject($subject);
         $message->from(env("MAIL_FROM_ADDRESS"),env("MAIL_FROM_NAME"));
         $message->attachData($pdf->output(), 'receipt.pdf');
       });

    }

}
