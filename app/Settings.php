<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Settings extends Model
{
    protected $table = 'settings';

    public static function getDetails() {
      return Settings::where('id',1)->first();
    }

    public static function editMyProfile($all) {
      //$response = new Responseobject;
      $id	= Auth::user()->id;
      $model = Settings::find($id);
      $model->companyName = $all['companyName'];
      $model->companyLocation = $all['companyLocation'];
      $model->companyEmail = $all['companyEmail'];
      $model->companyTelephone = $all['companyTelephone'];
      $model->save();
      if($model) {
        return true;
      }
      else {
        return false;
      }
    }

}
