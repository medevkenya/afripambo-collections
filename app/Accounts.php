<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Accounts extends Model
{
    protected $table = 'accounts';

    public static function getAll() {
      return $list = Accounts::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function storeone($accountName)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Accounts;
        $model->accountName = $accountName;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new account [".$accountName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $accountName)
    {
        $model = Accounts::find($id);
        $model->accountName = $accountName;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited account [".$accountName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Accounts::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted account [".$id."]");
            return true;
        }
        return false;
    }

}
