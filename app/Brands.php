<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Brands extends Model
{
    protected $table = 'brands';

    public static function getAll($limit)
    {
        if($limit==0) {
          return Brands::where('isDeleted',0)->orderBy('id','DESC')->get();
        }
        else {
          return Brands::where('isDeleted',0)->orderBy('id','DESC')->limit($limit)->get();
        }
    }

}
