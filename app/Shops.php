<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Shops extends Model
{
    protected $table = 'shops';

    public static function getAll($limit)
    {
        if($limit==0) {
          return Shops::where('isDeleted',0)->orderBy('id','DESC')->get();
        }
        else {
          return Shops::where('isDeleted',0)->inRandomOrder()->limit($limit)->get();
        }
    }

}
