<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;
use App\Cart;
use App\Notifications;
use App\Products;
use App\Shopproducts;
use Log;

class Batches extends Model
{
    protected $table = 'batches';

    public static function getAll() {
      return $list = Batches::select('batches.*','products.productName','products.unit','categories.categoryName','suppliers.supplierName','shopproducts.shopId','shops.shopName')
      ->leftJoin('suppliers','batches.supplierId','=','suppliers.id')
      ->leftJoin('products','batches.productId','=','products.id')
      ->leftJoin('categories','products.categoryId','=','categories.id')
      ->leftJoin('shopproducts','batches.shopProductId','=','shopproducts.id')
      ->leftJoin('shops','shopproducts.shopId','=','shops.id')
      ->where('batches.adminId',Auth::user()->adminId)
      ->where('batches.status','In Stock')
      ->where('batches.isDeleted',0)
      ->orderBy('batches.id','DESC')->get();
    }

    public static function getAllProducts() {
      return $list = Batches::select('batches.*','products.productName','products.unit','categories.categoryName','suppliers.supplierName','shopproducts.shopId','shops.shopName')
      ->leftJoin('suppliers','batches.supplierId','=','suppliers.id')
      ->leftJoin('products','batches.productId','=','products.id')
      ->leftJoin('categories','products.categoryId','=','categories.id')
      ->leftJoin('shopproducts','batches.shopProductId','=','shopproducts.id')
      ->leftJoin('shops','shopproducts.shopId','=','shops.id')
      ->where('batches.adminId',Auth::user()->adminId)
      ->where('batches.status','In Stock')
      ->where('batches.isDeleted',0)
      ->groupBy('batches.productId')
      ->orderBy('batches.id','DESC')->get();
    }

    public static function getProductStockValue($productId) {
      $total = 0;
      $quantity = 0;
      $list = Batches::where('productId',$productId)->where('status','In Stock')->where('isDeleted',0)->get();
      foreach ($list as $key) {
        $quantity += $key->quantity - $key->sold;
        $total += $key->sellingPrice * $quantity;
      }
      return array('total'=>$total,'quantity'=>$quantity);
    }

    public static function getProductQuantity($shopProductId) {
      $quantity = Batches::where('productId',$shopProductId)->where('status','In Stock')->where('isDeleted',0)->sum('quantity');
      $sold = Batches::where('productId',$shopProductId)->where('status','In Stock')->where('isDeleted',0)->sum('sold');
      $bal = $quantity - $sold;
      if($bal > 0) {
        return $bal;
      }
      else {
        return 0;
      }
    }

    public static function storeone($supplierId,$shopProductId,$quantity,$shopId,$type,$batchNo,$expiryDate)
    {

      $shopproductdetails = Shopproducts::where('id',$shopProductId)->first();
      $details = Products::where('id',$shopproductdetails->productId)->first();

        $isPaid = "Pending";

        if($type=="Cash")
        {
          $isPaid = "Paid";
        }

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Batches;
        $model->supplierId = $supplierId;
        $model->productId = $details->id;
        $model->shopProductId = $shopProductId;
        $model->categoryId = $details->categoryId;
        $model->type = $type;
        $model->batchNo = $batchNo;
        $model->expiryDate = $expiryDate;
        $model->shopId = $shopId;
        $model->quantity = $quantity;
        $model->isPaid = $isPaid;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Shopproducts::updateShopProductQuantity($shopProductId,$quantity);
          Activities::saveLog("Added new batch [".$supplierId."]");
          return true;
        }

        return false;

    }

    public static function updatequantity($supplierId,$shopProductId,$quantity,$type)
    {
        $shopproductdetails = Shopproducts::where('id',$shopProductId)->first();
        $details = Products::where('id',$shopproductdetails->productId)->first();

        $isPaid = "Pending";

        if($type=="Cash")
        {
          $isPaid = "Paid";
        }

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Batches;
        $model->supplierId = $supplierId;
        $model->productId = $details->id;
        $model->shopProductId = $shopProductId;
        $model->categoryId = $details->categoryId;
        $model->quantity = $quantity;
        $model->type = $type;
        $model->isPaid = $isPaid;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Shopproducts::updateShopProductQuantity($shopProductId,$quantity);
          Activities::saveLog("Added new batch [".$supplierId."]");
          return true;
        }

        return false;

    }

    public static function ejectbatch($id,$shopProductId,$quantity) {
      $shopproductdetails = Shopproducts::where('id',$shopProductId)->first();
      $batchdetails = Batches::where('id',$id)->first();
      $newBatchQuantity = $batchdetails->quantity - $quantity;
      $newShopProductQuantity = $shopproductdetails->quantity - $quantity;

      $model = Batches::find($id);
      $model->quantity = $newBatchQuantity;
      $model->save();
      if ($model) {
          Activities::saveLog("Ejected batch [".$id."], quantity [".$quantity."] ");
      }
      else {
        Activities::saveLog("Failed to Eject batch [".$id."], quantity [".$quantity."] ");
      }

      $model = Shopproducts::find($shopProductId);
      $model->quantity = $newShopProductQuantity;
      $model->save();
      if ($model) {
          Activities::saveLog("Ejected shop product [".$shopProductId."], quantity [".$quantity."] ");
      }
      else {
        Activities::saveLog("Failed to Eject shop product [".$shopProductId."], quantity [".$quantity."] ");
      }

      return true;

    }

    public static function deductQuantity($shopProductId,$quantity)
    {
      $batchdetails = Batches::select('id','quantity')->where('shopProductId',$shopProductId)->where('status','In Stock')->where('isDeleted',0)->orderBy('id','DESC')->first();
      $newBatchQuantity = $batchdetails->quantity - $quantity;
      $model = Batches::find($batchdetails->id);
      $model->quantity = $newBatchQuantity;
      $model->save();
      if ($model)
      {
          Activities::saveLog("Sold batch [".$batchdetails->id."], quantity [".$quantity."] ");
      }
      else
      {
        Activities::saveLog("Failed to sale batch [".$batchdetails->id."], quantity [".$quantity."] ");
      }
    }

    public static function updateone($id,$supplierId,$shopProductId,$shopId,$type,$batchNo,$expiryDate)
    {
      $shopproductdetails = Shopproducts::where('id',$shopProductId)->first();
      $details = Products::where('id',$shopproductdetails->productId)->first();
        $model = Batches::find($id);
        $model->supplierId = $supplierId;
        $model->productId = $details->id;
        $model->shopProductId = $shopProductId;
        $model->categoryId = $details->categoryId;
        $model->type = $type;
        $model->batchNo = $batchNo;
        $model->expiryDate = $expiryDate;
        $model->shopId = $shopId;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited batch [".$supplierId."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Batches::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted batch [".$id."]");
            return true;
        }
        return false;
    }

    public static function updateProduct($orderId)
    {

      $list = Cart::where('orderId',$orderId)->where('isDeleted',0)->get();
      foreach ($list as $key) {

        $check = Batches::where('productId',$key->productId)->where('status','In Stock')->where('isDeleted',0)->orderBy('id','ASC')->first();
        if($check)
        {
          log::info("batch id--".$check->id);
          $checkdetails = Batches::where('id',$check->id)->first();
            $sold = $checkdetails->sold + $key->quantity;
            log::info("cart quantity--".$key->quantity);
            log::info("batch new sold--".$sold);
            Batches::where('id',$checkdetails->id)->update(['sold'=>$sold]);
            if($sold >= $checkdetails->quantity)
            {
                Batches::where('id',$checkdetails->id)->update(['status'=>'Out of Stock']);
                $message = $key->itemName." has run out of stock";
                Notifications::storeone($message);
            }
        }

      }

    }

}
