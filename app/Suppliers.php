<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Userpermissions;
use App\Activities;

class Suppliers extends Model
{
    protected $table = 'suppliers';

    public static function getAll()
    {
        return Suppliers::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();
    }

    public static function storeone($supplierName,$email,$contacts,$amountOwed)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Suppliers;
        $model->supplierName = $supplierName;
        $model->email = $email;
        $model->contacts = $contacts;
        $model->amountOwed = $amountOwed;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
        Activities::saveLog("Added new supplier [".$supplierName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $supplierName,$email,$contacts,$amountOwed)
    {
        $model = Suppliers::find($id);
        $model->supplierName = $supplierName;
        $model->email = $email;
        $model->contacts = $contacts;
        $model->amountOwed = $amountOwed;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited supplier [".$supplierName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Suppliers::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted supplier [".$id."]");
        }
        return false;
    }

}
