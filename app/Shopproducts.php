<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;
use Str;
use App\Activities;
use App\Products;
use App\Batches;

class Shopproducts extends Model
{
    protected $table = 'shopproducts';

    public static function getAll($limit)
    {
        if($limit==0) {
          return Shopproducts::select('shopproducts.*','shops.shopName','shops.location','shops.latitude','shops.longitude','products.productName','products.description','products.categoryId','products.sizeId','products.colorId','products.brandId','categories.categoryName','colors.colorName','brands.brandName','sizes.sizeName')
          ->leftJoin('products','shopproducts.productId','=','products.id')
          ->leftJoin('shops','shopproducts.shopId','=','shops.id')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->leftJoin('colors','products.colorId','=','colors.id')
          ->leftJoin('brands','products.brandId','=','brands.id')
          ->leftJoin('sizes','products.sizeId','=','sizes.id')
          ->where('shopproducts.isDeleted',0)->orderBy('shopproducts.id','DESC')->get();
        }
        else {
          return Shopproducts::select('shopproducts.*','shops.shopName','shops.location','shops.latitude','shops.longitude','products.productName','products.description','products.categoryId','products.sizeId','products.colorId','products.brandId','categories.categoryName','colors.colorName','brands.brandName','sizes.sizeName')
          ->leftJoin('products','shopproducts.productId','=','products.id')
          ->leftJoin('shops','shopproducts.shopId','=','shops.id')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->leftJoin('colors','products.colorId','=','colors.id')
          ->leftJoin('brands','products.brandId','=','brands.id')
          ->leftJoin('sizes','products.sizeId','=','sizes.id')
          ->where('shopproducts.isDeleted',0)->orderBy('shopproducts.id','DESC')->limit($limit)->get();
        }
    }

    public static function getLowStockAll($limit)
    {
        if($limit==0) {
          return Shopproducts::select('shopproducts.*','shops.shopName','shops.location','shops.latitude','shops.longitude','products.productName','products.description','products.categoryId','products.sizeId','products.colorId','products.brandId','categories.categoryName','colors.colorName','brands.brandName','sizes.sizeName')
          ->leftJoin('products','shopproducts.productId','=','products.id')
          ->leftJoin('shops','shopproducts.shopId','=','shops.id')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->leftJoin('colors','products.colorId','=','colors.id')
          ->leftJoin('brands','products.brandId','=','brands.id')
          ->leftJoin('sizes','products.sizeId','=','sizes.id')
          ->where('shopproducts.isDeleted',0)->orderBy('shopproducts.id','DESC')->get();
        }
        else {
          return Shopproducts::select('shopproducts.*','shops.shopName','shops.location','shops.latitude','shops.longitude','products.productName','products.description','products.categoryId','products.sizeId','products.colorId','products.brandId','categories.categoryName','colors.colorName','brands.brandName','sizes.sizeName')
          ->leftJoin('products','shopproducts.productId','=','products.id')
          ->leftJoin('shops','shopproducts.shopId','=','shops.id')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->leftJoin('colors','products.colorId','=','colors.id')
          ->leftJoin('brands','products.brandId','=','brands.id')
          ->leftJoin('sizes','products.sizeId','=','sizes.id')
          ->where('shopproducts.isDeleted',0)->orderBy('shopproducts.id','DESC')->limit($limit)->get();
        }
    }

    public static function getNearest($latitude,$longitude)
    {

      $nearestRadius = env("nearestRadius");

      $all = array();

       $nearest = Shopproducts::select('shopproducts.*','shops.shopName','shops.location','shops.latitude','shops.longitude','products.productName','products.description','products.categoryId','products.sizeId','products.colorId','products.brandId','categories.categoryName', DB::raw(sprintf(
            '(6371 * acos(cos(radians(%1$.7f)) * cos(radians(shops.latitude)) * cos(radians(shops.longitude) - radians(%2$.7f)) + sin(radians(%1$.7f)) * sin(radians(shops.latitude)))) AS distance',
            $latitude,
            $longitude
        )))
        ->leftJoin('products','shopproducts.productId','=','products.id')
        ->leftJoin('shops','shopproducts.shopId','=','shops.id')
        ->leftJoin('categories','products.categoryId','=','categories.id')
        ->where('shopproducts.isDeleted',0)
        ->having('distance', '<', $nearestRadius)
        ->orderBy('distance', 'asc')
        ->paginate(18);

      return $nearest;

    }

    public static function getHotDeals()
    {

          return Shopproducts::select('shopproducts.*','shops.shopName','shops.location','shops.latitude','shops.longitude','products.productName','products.description','products.categoryId','products.sizeId','products.colorId','products.brandId','categories.categoryName','colors.colorName','brands.brandName','sizes.sizeName')
          ->leftJoin('products','shopproducts.productId','=','products.id')
          ->leftJoin('shops','shopproducts.shopId','=','shops.id')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->leftJoin('colors','products.colorId','=','colors.id')
          ->leftJoin('brands','products.brandId','=','brands.id')
          ->leftJoin('sizes','products.sizeId','=','sizes.id')
          ->where('shopproducts.discount','>',0)
          ->where('shopproducts.isDeleted',0)->orderBy('shopproducts.discount','DESC')->paginate(18);

    }

    public static function countHotDeals() {
      return Shopproducts::where('discount','>',0)->where('isDeleted',0)->count();
    }

    public static function getByCategoryId($categoryId)
    {

          return Shopproducts::select('shopproducts.*','shops.shopName','shops.location','shops.latitude','shops.longitude','products.productName','products.description','products.categoryId','products.sizeId','products.colorId','products.brandId','categories.categoryName','colors.colorName','brands.brandName','sizes.sizeName')
          ->leftJoin('products','shopproducts.productId','=','products.id')
          ->leftJoin('shops','shopproducts.shopId','=','shops.id')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->leftJoin('colors','products.colorId','=','colors.id')
          ->leftJoin('brands','products.brandId','=','brands.id')
          ->leftJoin('sizes','products.sizeId','=','sizes.id')
          ->where('products.categoryId',$categoryId)
          ->where('shopproducts.isDeleted',0)->orderBy('shopproducts.id','DESC')->paginate(18);

    }

    public static function getRelatedProducts($categoryId)
    {

          return Shopproducts::select('shopproducts.*','shops.shopName','shops.location','shops.latitude','shops.longitude','products.productName','products.description','products.categoryId','products.sizeId','products.colorId','products.brandId','categories.categoryName','colors.colorName','brands.brandName','sizes.sizeName')
          ->leftJoin('products','shopproducts.productId','=','products.id')
          ->leftJoin('shops','shopproducts.shopId','=','shops.id')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->leftJoin('colors','products.colorId','=','colors.id')
          ->leftJoin('brands','products.brandId','=','brands.id')
          ->leftJoin('sizes','products.sizeId','=','sizes.id')
          ->where('products.categoryId',$categoryId)
          ->where('shopproducts.isDeleted',0)->orderBy('shopproducts.id','DESC')->take(8)->get();

    }

    public static function getByBrandId($brandId)
    {

          return Shopproducts::select('shopproducts.*','shops.shopName','shops.location','shops.latitude','shops.longitude','products.productName','products.description','products.categoryId','products.sizeId','products.colorId','products.brandId','categories.categoryName','colors.colorName','brands.brandName','sizes.sizeName')
          ->leftJoin('products','shopproducts.productId','=','products.id')
          ->leftJoin('shops','shopproducts.shopId','=','shops.id')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->leftJoin('colors','products.colorId','=','colors.id')
          ->leftJoin('brands','products.brandId','=','brands.id')
          ->leftJoin('sizes','products.sizeId','=','sizes.id')
          ->where('products.brandId',$brandId)
          ->where('shopproducts.isDeleted',0)->orderBy('shopproducts.id','DESC')->paginate(18);

    }

    public static function getById($id)
    {

          return Shopproducts::select('shopproducts.*','shops.shopName','shops.location','shops.latitude','shops.longitude','products.productName','products.description','products.categoryId','products.sizeId','products.colorId','products.brandId','categories.categoryName','colors.colorName','brands.brandName','sizes.sizeName')
          ->leftJoin('products','shopproducts.productId','=','products.id')
          ->leftJoin('shops','shopproducts.shopId','=','shops.id')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->leftJoin('colors','products.colorId','=','colors.id')
          ->leftJoin('brands','products.brandId','=','brands.id')
          ->leftJoin('sizes','products.sizeId','=','sizes.id')
          ->where('shopproducts.id',$id)
          ->where('shopproducts.isDeleted',0)->first();

    }

    public static function countByCategoryId($categoryId) {
      return Shopproducts::select('shopproducts.*','products.productName','products.description','products.categoryId','categories.categoryName')
      ->leftJoin('products','shopproducts.productId','=','products.id')
      ->leftJoin('categories','products.categoryId','=','categories.id')
      ->where('products.categoryId',$categoryId)
      ->where('shopproducts.isDeleted',0)->count();
    }

    public static function countByBrandId($brandId) {
      return Shopproducts::select('shopproducts.*','products.productName','products.description','products.categoryId','categories.categoryName')
      ->leftJoin('products','shopproducts.productId','=','products.id')
      ->leftJoin('categories','products.categoryId','=','categories.id')
      ->where('products.brandId',$brandId)
      ->where('shopproducts.isDeleted',0)->count();
    }

    public static function getFeatured()
    {

          return Shopproducts::select('shopproducts.*','shops.shopName','shops.location','shops.latitude','shops.longitude','products.productName','products.description','products.categoryId','products.sizeId','products.colorId','products.brandId','categories.categoryName','colors.colorName','brands.brandName','sizes.sizeName')
          ->leftJoin('products','shopproducts.productId','=','products.id')
          ->leftJoin('shops','shopproducts.shopId','=','shops.id')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->leftJoin('colors','products.colorId','=','colors.id')
          ->leftJoin('brands','products.brandId','=','brands.id')
          ->leftJoin('sizes','products.sizeId','=','sizes.id')
          ->where('shopproducts.isDeleted',0)->orderBy('shopproducts.id','DESC')->limit(8)->get();

    }

    public static function topProducts1()
    {

          return Shopproducts::select('shopproducts.*','products.productName','products.description','products.categoryId','categories.categoryName')
          ->leftJoin('products','shopproducts.productId','=','products.id')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->where('shopproducts.isDeleted',0)->orderBy('shopproducts.id','DESC')->limit(4)->get();

    }

    public static function topProducts2()
    {

          return Shopproducts::select('shopproducts.*','products.productName','products.description','products.categoryId','categories.categoryName')
          ->leftJoin('products','shopproducts.productId','=','products.id')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->where('shopproducts.isDeleted',0)->orderBy('shopproducts.id','DESC')->limit(4)->get();

    }

    public static function trendingProducts1()
    {

          return Shopproducts::select('shopproducts.*','products.productName','products.description','products.categoryId','categories.categoryName')
          ->leftJoin('products','shopproducts.productId','=','products.id')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->where('shopproducts.isDeleted',0)->orderBy('shopproducts.id','DESC')->limit(4)->get();

    }

    public static function trendingProducts2()
    {

          return Shopproducts::select('shopproducts.*','products.productName','products.description','products.categoryId','categories.categoryName')
          ->leftJoin('products','shopproducts.productId','=','products.id')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->where('shopproducts.isDeleted',0)->orderBy('shopproducts.id','DESC')->limit(4)->get();

    }

    public static function popularProducts1()
    {

          return Shopproducts::select('shopproducts.*','products.productName','products.description','products.categoryId','categories.categoryName')
          ->leftJoin('products','shopproducts.productId','=','products.id')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->where('shopproducts.isDeleted',0)->orderBy('shopproducts.id','DESC')->limit(4)->get();

    }

    public static function popularProducts2()
    {

          return Shopproducts::select('shopproducts.*','products.productName','products.description','products.categoryId','categories.categoryName')
          ->leftJoin('products','shopproducts.productId','=','products.id')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->where('shopproducts.isDeleted',0)->orderBy('shopproducts.id','DESC')->limit(4)->get();

    }

    public static function storeone($shopId,$productId,$productCondition,$categoryId,$discount,$quantity,$inPrice,$outPrice,$alertLevel)
    {

        $details = Products::where('id',$productId)->first();
        $title = $details->productName."-".$shopId."-".$productId.time();

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Shopproducts;
        $model->shopId = $shopId;
        $model->productId = $productId;
        $model->productCondition = $productCondition;
        $model->discount = $discount;
        $model->quantity = $quantity;
        $model->inPrice = $inPrice;
        $model->outPrice = $outPrice;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->categoryId = $categoryId;
        $model->alertLevel = $alertLevel;
        $model->slug = Str::slug($title);
        $model->save();
        if ($model)
        {
          Batches::updatequantity(0,$model->id,$quantity,"Cash");
          Activities::saveLog("Added new shop product [".$productId."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $shopId,$productId,$productCondition,$categoryId,$discount,$inPrice,$outPrice,$alertLevel)
    {
        $model = Shopproducts::find($id);
        $model->shopId = $shopId;
        $model->productId = $productId;
        $model->productCondition = $productCondition;
        $model->discount = $discount;
        //$model->quantity = $quantity;
        $model->inPrice = $inPrice;
        $model->outPrice = $outPrice;
        $model->categoryId = $categoryId;
        $model->alertLevel = $alertLevel;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited shop product [".$id."]");
            return true;
        }
        return false;
    }

    public static function updateShopProductQuantity($shopProductId,$quantity) {
      $shopproductdetails = Shopproducts::where('id',$shopProductId)->first();
      $newShopProductQuantity = $shopproductdetails->quantity + $quantity;

      $model = Shopproducts::find($shopProductId);
      $model->quantity = $newShopProductQuantity;
      $model->save();
      if ($model) {
          Activities::saveLog("Added shop product [".$shopProductId."], quantity [".$quantity."] ");
      }
      else {
        Activities::saveLog("Failed to add shop product [".$shopProductId."], quantity [".$quantity."] ");
      }

      return true;

    }

    public static function deductShopProductQuantity($shopProductId,$quantity)
    {

      $shopproductdetails = Shopproducts::where('id',$shopProductId)->first();
      $newShopProductQuantity = $shopproductdetails->quantity - $quantity;

      $model = Shopproducts::find($shopProductId);
      $model->quantity = $newShopProductQuantity;
      $model->save();
      if ($model)
      {
          Activities::saveLog("Sold shop product [".$shopProductId."], quantity [".$quantity."] ");
      }
      else
      {
        Activities::saveLog("Failed to sale shop product [".$shopProductId."], quantity [".$quantity."] ");
      }

      return true;

    }

    public static function deleteone($id)
    {
        $model = Shopproducts::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted shop product [".$id."]");
            return true;
        }
        return false;
    }

}
