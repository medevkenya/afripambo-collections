<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Categories extends Model
{
    protected $table = 'categories';

    public static function getAll($limit)
    {
        if($limit==0) {
          return Categories::where('isDeleted',0)->orderBy('id','DESC')->get();
        }
        else {
          return Categories::where('isDeleted',0)->inRandomOrder()->limit($limit)->get();
        }
    }

    public static function storeone($categoryName)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Categories;
        $model->categoryName = $categoryName;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new category [".$categoryName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $categoryName)
    {
        $model = Categories::find($id);
        $model->categoryName = $categoryName;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited category [".$categoryName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Categories::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted category [".$id."]");
            return true;
        }
        return false;
    }


}
