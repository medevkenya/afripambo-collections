<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Responseobject
{
    const status_ok = "Ok";
    const status_fail = "Fail";
    const code_ok = 1;
    const code_fail = 400;
    const status_unauthorized = "Unauthorized";
    const code_unauthorized = 403;
    const status_not_found = "Not found";
    const code_not_found = 404;
    const status_error = "Error";
    const code_error = 500;

    public $status;
    public $code;
    public $message;
    public $messages = array();
    public $result = array();
}
