<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Costs;
use App\Activities;
use App\Shopproducts;
use App\Products;
use App\Batches;

class Sales extends Model
{

    protected $table = 'sales';

    public static function completesale($total,$type,$shopProductId,$discount,$categoryId,$quantity,$customerId)
    {

        $shopproductdetails = Shopproducts::where('id',$shopProductId)->first();
        //$details = Products::where('id',$shopproductdetails->productId)->first();

        $batchDetails = Batches::select('id','quantity')->where('shopProductId',$shopProductId)->where('status','In Stock')->where('isDeleted',0)->orderBy('id','DESC')->first();

        $unitCost = $shopproductdetails->outPrice;

        $isPaid = "Pending";

        if($type=="Cash")
        {
          $isPaid = "Paid";
        }

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Sales;
        $model->categoryId = $categoryId;
        $model->productId = $shopproductdetails->productId;
        $model->customerId = $customerId;
        $model->total = $total;
        $model->discount = $discount;
        $model->unitCost = $unitCost;
        $model->quantity = $quantity;
        $model->type = $type;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->isPaid = $isPaid;
        $model->save();
        if ($model)
        {
          Shopproducts::deductShopProductQuantity($shopProductId,$quantity);
          if($batchDetails)
          {
            Batches::deductQuantity($shopProductId,$quantity);
          }
          Activities::saveLog("Added new sale [".$shopProductId."]");
          return true;
        }

        return false;

    }

    public static function updateone($id,$productId,$customerId,$quantity,$discount,$type)
    {

        $batchDetails = Sales::select('id','unitCost')->where('id',$is)->where('isDeleted',0)->orderBy('id','DESC')->first();
        $unitCost = $batchDetails->unitCost;
        $total = ($unitCost * $quantity) - $discount;

        $isPaid = "Pending";

        if($type=="Cash")
        {
          $isPaid = "Paid";
        }

        $model = Sales::find($id);
        $model->total = $total;
        $model->productId = $productId;
        $model->customerId = $customerId;
        $model->discount = $discount;
        $model->quantity = $quantity;
        $model->type = $type;
        $model->isPaid = $isPaid;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited sale [".$productId."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Sales::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted sale [".$id."]");
            return true;
        }
        return false;
    }

}
