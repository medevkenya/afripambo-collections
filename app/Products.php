<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;
use App\Photos;

class Products extends Model
{
    protected $table = 'products';

    public static function getAll($limit)
    {
        if($limit==0) {
          return Products::select('products.*','categories.categoryName')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->where('products.adminId',Auth::user()->adminId)->where('products.isDeleted',0)->orderBy('products.id','DESC')->get();
        }
        else {
          return Products::select('products.*','categories.categoryName')
          ->leftJoin('categories','products.categoryId','=','categories.id')
          ->where('products.adminId',Auth::user()->adminId)->where('products.isDeleted',0)->orderBy('products.id','DESC')->limit($limit)->get();
        }
    }

//     public static function getLowStockAll() {
//   return Products::select('products.*','categories.categoryName')
//   ->leftJoin('categories','products.categoryId','=','categories.id')
//   ->where('products.adminId',Auth::user()->adminId)->where('products.isDeleted',0)->orderBy('products.id','DESC')->get();
// }


    public static function storeone($productName,$description,$unit,$categoryId,$photoUrl1,$photoUrl2,$photoUrl3,$photoUrl4)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Products;
        $model->productName = $productName;
        $model->categoryId = $categoryId;
        $model->unit = $unit;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->description = $description;
        $model->save();
        if ($model)
        {
          if($photoUrl1 != null) {
            Photos::savePhoto($model->id,$photoUrl1);
          }
          if($photoUrl2 != null) {
            Photos::savePhoto($model->id,$photoUrl2);
          }
          if($photoUrl3 != null) {
            Photos::savePhoto($model->id,$photoUrl3);
          }
          if($photoUrl4 != null) {
            Photos::savePhoto($model->id,$photoUrl4);
          }
          Activities::saveLog("Added new product [".$productName."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $productName,$description,$unit,$categoryId)
    {
        $model = Products::find($id);
        $model->productName = $productName;
        $model->description = $description;
        $model->categoryId = $categoryId;
        $model->unit = $unit;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited product [".$productName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Products::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted product [".$id."]");
            return true;
        }
        return false;
    }


}
