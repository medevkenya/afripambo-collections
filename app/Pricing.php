<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\Orders;
use App\Cart;
use App\Shops;
use App\Shopproducts;
use Log;

class Pricing extends Model
{

  protected $table = 'pricings';

  public static function calculateDeliveryFee($deliveryModeId,$fromLatitude,$fromLongitude,$orderNo) {
    log::info("calculateDeliveryFee--orderNo-".$orderNo);
        //get distance matric, time, - driver
        $orderdetails = Orders::where('orderNo',$orderNo)->first();
        $cartdetails = Cart::where('orderId',$orderdetails->id)->first();
        $shopproductdetails = Shopproducts::where('id',$cartdetails->shopProductId)->first();
        $shopdetails = Shops::where('id',$shopproductdetails->shopId)->first();

        log::info("calculateDeliveryFee--orderdetails-".json_encode($orderdetails));
        log::info("calculateDeliveryFee--cartdetails-".json_encode($cartdetails));
        log::info("calculateDeliveryFee--shopproductdetails-".json_encode($shopproductdetails));
        log::info("calculateDeliveryFee--shopdetails-".json_encode($shopdetails));

        $matrixDriver = self::GetDrivingDistance($fromLatitude, $shopdetails->latitude, $fromLongitude, $shopdetails->longitude);//GetDistanceAndTime($fromAddress,$keyd->lastAddress);
        $time = $matrixDriver['time'];
        $km = number_format($matrixDriver['km'],2);

        return Pricing::getPrice($deliveryModeId,$km,$time);
  }

  public static function getPrice($deliveryModeId,$distance,$time) {

    $modelDetails = Pricing::where('deliveryModeId',$deliveryModeId)->where('isDeleted',0)->first();

    $minutes = $time / 60;
    $costPerMinute = $minutes * $modelDetails->pricePerMinute;

    if(self::CheckTime() == "day")
    {
      $costByDistance = $distance * $modelDetails->dayTime;
    }
    else
    {
      $costByDistance = $distance * $modelDetails->nightTime;
    }

    $costFacility = ($costPerMinute + $costByDistance) * ($modelDetails->facilityCost / 100);

    $totalCost = $costFacility + $costPerMinute + $costByDistance;

    if($totalCost > $modelDetails->baseRate) {
      return $totalCost;
    }
    else {
      return $modelDetails->baseRate;
    }

  }

  public static function CheckTime() {

    date_default_timezone_set('Africa/Nairobi');

    $hour = date('H', time());

    if( $hour > 6 && $hour <= 19) {
      return "day";
    }
    else if($hour > 19 && $hour < 6) {
      return "night";
    }

  }

  public static function editPricing($userId,$id,$deliveryModeId,$pricePerMinute,$dayTime,$nightTime,$baseRate,$facilityFee) {
    $res = Pricing::where('id', $id)->update(['isDeleted' => 1]);
    if($res) {
      $model = new Pricing;
      $model->deliveryModeId = $deliveryModeId;
      $model->pricePerMinute = $pricePerMinute;
      $model->dayTime = $dayTime;
      $model->nightTime = $nightTime;
      $model->baseRate = $baseRate;
      $model->facilityFee = $facilityFee;
      $model->createdBy = $userId;
      $model->save();
      if($model) {
        return true;
      }
      else {
        return false;
      }
    }
  }

  public static function GetDrivingDistance($lat1, $lat2, $long1, $long2)
  {

      log::info("GetDrivingDistance GetDrivingDistance GetDrivingDistance GetDrivingDistance GetDrivingDistance GetDrivingDistance GetDrivingDistance GetDrivingDistance");

      $key = env("GoogleMapKey");

      $url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=".$key."&origins=".$lat1.",".$long1."&destinations=".$lat2.",".$long2."&mode=driving&language=pl-PL";
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      $response = curl_exec($ch);
      curl_close($ch); log::info($response);
      $response_a = json_decode($response, true);
      $dist = $response_a['rows'][0]['elements'][0]['distance']['value'];
      $time = $response_a['rows'][0]['elements'][0]['duration']['text'];

      log::info("LAST MILE TIME 8888---".$time);
      log::info("LAST MILE DISTANCE 8888 ---".$dist);

      //if($time > 0) {
      $time = preg_replace("/[^0-9]/", "",$time);
      //}
      //else {
      //$time = 0;
      //}

      if($dist > 0)
      {
        $dist = round($dist / 1000);//preg_replace("/[^0-9]/", "",$dist);
      }
      else
      {
        $dist = 0;
      }

      log::info("LAST MILE TIME 7777---".$time);
      log::info("LAST MILE DISTANCE 7777---".$dist);

      return array('km' => $dist, 'time' => $time);

}

public static function getDistance($latitude1, $longitude1, $latitude2, $longitude2) {
$earth_radius = 6371;

$dLat = deg2rad($latitude2 - $latitude1);
$dLon = deg2rad($longitude2 - $longitude1);

$a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
$c = 2 * asin(sqrt($a));
$d = $earth_radius * $c;

return $d;
}

public static function GetDistanceAndTime($from,$to) {
//$from = "Off Mombasa Road, Nairobi, Kenya";
//$to = "Thika Town, Thika, Kenya";

//$from = self::GetAddressFromCoords($lat,$lng);
//$to = self::GetAddressFromCoords($lat,$lng);

$from = urlencode($from);
$to = urlencode($to);

$key = env("GoogleMapKey");

$data = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?key=$key&origins=$from&destinations=$to&language=en-EN&sensor=false");
$data = json_decode($data);
log::info("GetDistanceAndTime--".json_encode($data));
$time = 0;
$distance = 0;

foreach($data->rows[0]->elements as $road) {
    $time += $road->duration->value;
    $distance += $road->distance->value;
}
$km=$distance/1000;
return array('km'=>$km,'destination'=>$data->destination_addresses[0],'origin'=>$data->origin_addresses[0],'time'=>$time);

// {
//   "km": 60.06,
//   "destination": "Thika, Kenya",
//   "origin": "Mombasa Road, Nairobi, Kenya",
//   "time": 4119
// }

// echo "To: ".$data->destination_addresses[0];
// echo "<br/>";
// echo "From: ".$data->origin_addresses[0];
// echo "<br/>";
// echo "Time: ".$time." seconds";
// echo "<br/>";
// echo "Distance: ".$distance." meters";
// echo "<br/>";
// echo $km;
}

public static function GetAddressFromCoords($lat,$lng) {
$key = env("GoogleMapKey");
  //Now build the actual lookup
  $address_url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . $lat . ',' . $lng . '&key='.$key.'&sensor=false';
  $address_json = json_decode(file_get_contents($address_url));
log::info("GetAddressFromCoords--".json_encode($address_json));
  $address_data = $address_json->results[0]->address_components;
  //return $address_data = $address_json->results[0]->formatted_address;

  $street = str_replace('Dr', 'Drive', $address_data[1]->long_name);
  $town = $address_data[2]->long_name;
  $county = $address_data[3]->long_name;

  return $street.', '. $town. ', '.$county;
}

public static function GetCoordsFromAddress() {
$key = env("GoogleMapKey");
$search_code = urlencode($postcode);
      $url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . $search_code . '&sensor=false';
      $json = json_decode(file_get_contents($url));
      if($json->results == []){
          return '';
      }
      $lat = $json->results[0]->geometry->location->lat;
      $lng = $json->results[0]->geometry->location->lng;
}

}
