<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Activities;

class Companyassets extends Model
{
    protected $table = 'companyassets';

    public static function getAll() {
      return $list = Companyassets::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->orderBy('id','DESC')->get();
    }

    public static function storeone($name,$quantity,$amount)
    {
        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Companyassets;
        $model->name = $name;
        $model->quantity = $quantity;
        $model->amount = $amount;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new company asset [".$name."]");
          return true;
        }
        return false;
    }

    public static function updateone($id,$name,$quantity,$amount)
    {
        $model = Companyassets::find($id);
        $model->name = $name;
        $model->quantity = $quantity;
        $model->amount = $amount;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited company asset [".$name."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Companyassets::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted company asset [".$id."]");
            return true;
        }
        return false;
    }

}
