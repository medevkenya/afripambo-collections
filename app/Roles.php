<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Userpermissions;
use App\Activities;

class Roles extends Model
{
    protected $table = 'roles';

    public static function getAll()
    {
        return Roles::where('adminId',Auth::user()->adminId)->where('isDeleted',0)->get();
    }

    public static function storeone($roleName)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Roles;
        $model->roleName = $roleName;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
        Activities::saveLog("Added new role [".$roleName."]");
          Userpermissions::seedPermissions($model->id);
          sleep(3);
          return true;

        }

        return false;

    }

    public static function updateone($id, $roleName)
    {
        $model = Roles::find($id);
        $model->roleName = $roleName;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited role [".$roleName."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Roles::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted role [".$id."]");
            return  Userpermissions::deletePermissions($id);
        }
        return false;
    }

}
