<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Costs;
use App\Activities;

class Expenses extends Model
{
    protected $table = 'expenses';

    public static function storeone($expenseTypeId,$description,$amount)
    {

        $adminId	= Auth::user()->adminId;
        $created_by	= Auth::user()->id;
        $model = new Expenses;
        $model->expenseTypeId = $expenseTypeId;
        $model->description = $description;
        $model->amount = $amount;
        $model->adminId = $adminId;
        $model->created_by = $created_by;
        $model->save();
        if ($model)
        {
          Activities::saveLog("Added new expense [".$expenseTypeId."]");
          return true;
        }

        return false;

    }

    public static function updateone($id, $expenseTypeId,$description,$amount)
    {
        $model = Expenses::find($id);
        $model->expenseTypeId = $expenseTypeId;
        $model->description = $description;
        $model->amount = $amount;
        $model->save();
        if ($model) {
            Activities::saveLog("Edited expense [".$expenseTypeId."]");
            return true;
        }
        return false;
    }

    public static function deleteone($id)
    {
        $model = Expenses::find($id);
        $model->isDeleted = 1;
        $model->save();
        if ($model) {
            Activities::saveLog("Deleted expense [".$id."]");
            return true;
        }
        return false;
    }

}
