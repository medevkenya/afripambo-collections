<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;
use App\Orders;

class Cart extends Model
{
    protected $table = 'cart';

    public static function saveNew($items)
    {

      if(Auth::user()) {
        $userId = Auth::user()->id;
      }
      else {
        $userId = 0;
      }

      if(!empty($items)) {

        $orderId = Orders::saveNew();
        if($orderId) {

        $items = json_decode($items);
        foreach ($items as $key) {
          $model = new Cart;
          $model->userId = $userId;
          $model->shopProductId = $key->id;
          $model->quantity = $key->quantity;
          $model->cost = $key->price;
          $model->totalCost = $key->price * $key->quantity;
          $model->color = $key->color;
          $model->size = $key->size;
          $model->itemName = $key->name;
          $model->sessionId = Session::getId();
          $model->orderId = $orderId;
          $model->save();
        }
        return true;
      }
      return false;
      }
      return false;
    }

}
