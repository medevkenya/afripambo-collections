<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Session;

class Productratings extends Model
{
    protected $table = 'productratings';

    public static function saveone($all) {
          $model = new Productratings;
          if(Auth::user()) {
          $model->userId = Auth::user()->id;
          }
          $model->review = $all['review'];
          $model->rate = $all['rate'];
          $model->shopProductId = $all['shopProductId'];
          $model->name = $all['name'];
          $model->email = $all['email'];
          $model->sessionId = Session::getId();
          $model->save();
          if($model) {
            return true;
          }
          else {
            return false;
          }
        }

        public static function deleteone($id)
        {
          $model = Productratings::find($id);
          $model->isDeleted = 1;
          $model->save();
          if($model) {
            return true;
          }
          else {
            return false;
          }
        }

        public static function getRating($shopProductId)
        {
          $count = Productratings::where('shopProductId',$shopProductId)->where('isDeleted',0)->count();
          if($count==0) {
            $rating = 0;
          }
          else {
          $sum = Productratings::where('shopProductId',$shopProductId)->where('isDeleted',0)->sum('rate');
          $rating = ($sum / $count);
        }
        return round($rating);
        }

        public static function getProductratings($shopProductId)
        {
          return Productratings::where('shopProductId',$shopProductId)->where('isDeleted',0)->orderBy('id','DESC')->paginate(10);
        }


        public static function getopenProductratings($shopProductId)
        {
          return Productratings::where('shopProductId',$shopProductId)->where('isDeleted',0)->orderBy('id','DESC')->get();
        }

        public static function getLimitedProductratings($shopProductId)
        {
          return Productratings::where('shopProductId',$shopProductId)->where('isDeleted',0)->orderBy('id','DESC')->limit(10)->get();
        }
}
