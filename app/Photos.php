<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use App\Responseobject;
use Response;
use Log;

class Photos extends Model
{
    protected $table = 'photos';

    public static function uploadPhoto($userId,$photo) {
      $model = new Photos;
      $model->userId = $userId;
      $model->photo = $photo;
      $model->save();
      if($model) {
        return User::getMainURL()."photos/".$photo;
      }
      else {
        return false;
      }
    }

    public static function savePhoto($productId,$photoUrl) {
      $model = new Photos;
      $model->productId = $productId;
      $model->photoUrl = $photoUrl;
      $model->save();
      if($model) {
        return true;
      }
      else {
        return false;
      }
    }

    public static function deletePhoto($photoId)
    {
      $model = Photos::find($photoId);
      $model->isDeleted = 1;
      $model->save();
      if($model) {
        return true;
      }
      else {
        return false;
      }
    }

    public static function getRandom($productId)
    {
      $res =  Photos::where('productId',$productId)->where('isDeleted',0)->inRandomOrder()->limit(1)->first();
      return $res->photoUrl;
    }

    public static function getPhotos($productId)
    {
      return Photos::where('productId',$productId)->where('isDeleted',0)->inRandomOrder()->limit(4)->get();
    }

}
