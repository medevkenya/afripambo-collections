<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\User;
use App\Responseobject;
use Response;
use Log;

class Promotions extends Model
{
    protected $table = 'promotions';

    public static function getPromotion()
    {
      return Promotions::where('status',1)->where('isDeleted',0)->inRandomOrder()->limit(1)->first();
    }

}
