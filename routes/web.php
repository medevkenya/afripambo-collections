<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@home');
Route::get('/category/{slug}', 'SiteController@category');
Route::get('/brand/{slug}', 'SiteController@brand')->name('brand');
Route::get('/product/{slug}', 'SiteController@product');
Route::get('/hotdeals', 'SiteController@hotdeals');
Route::post('/savecart', 'SiteController@savecart')->name('savecart');
Route::get('subscribe', 'SiteController@subscribe')->name('subscribe');
Route::get('checkout', 'SiteController@checkout')->name('checkout');
Route::post('/submitreview', 'SiteController@submitreview');

Route::get('/nearest', 'SiteController@nearest');

Route::get('requestMpesaStk', 'C2BController@requestMpesaStk')->name('requestMpesaStk');
Route::get('cashondelivery', 'SiteController@cashondelivery')->name('cashondelivery');
Route::post('sendPaymentInvoice', 'SiteController@sendPaymentInvoice')->name('sendPaymentInvoice');
Route::post('calculateDeliveryFee', 'SiteController@calculateDeliveryFee')->name('calculateDeliveryFee');

Route::get('/admin', 'HomeController@signin');
Route::post('/adminsignin', 'HomeController@dosignin');
Route::get('/signin', 'SiteController@signin');
Route::post('dosignin', 'SiteController@dosignin');
Route::get('login', [ 'as' => 'login', 'uses' => 'SiteController@signin']);
Route::get('/register', 'SiteController@register');
Route::post('doRegister', 'SiteController@doRegister');
Route::get('/logout', 'SiteController@logout');
Route::get('/activateAccount', 'SiteController@activateAccount');

Route::get('ResetPasswordLink/{PasswordConfirmation}/{email}', 'ForgotpasswordController@resetPasswordLink');
Route::get('forgotPassword', 'ForgotpasswordController@forgotPassword');
Route::post('resetPassword', 'ForgotpasswordController@resetPassword');
Route::post('DoResetMYPassword', 'ForgotpasswordController@doResetPassword');

Route::get('resendRecoveryEmail/{email}', 'ForgotpasswordController@resendRecoveryEmail');

Route::get('/terms', 'SiteController@terms');
Route::get('/privacy', 'SiteController@privacy');
Route::get('/orderreturns', 'SiteController@orderreturns');
Route::get('/about', 'SiteController@about');

Route::group(['middleware' => 'auth'], function () {

Route::get('dashboard', 'AccountController@dashboard');

Route::get('profile', 'AccountController@profile');
Route::post('updateProfile', 'AccountController@updateProfile');
Route::get('changepassword', 'AccountController@changepassword');
Route::post('doChangePass', 'AccountController@doChangePass');
Route::get('changePic', 'AccountController@changePic');
Route::post('UpdatePic', 'AccountController@UpdatePic')->name('UpdatePic');

Route::post('updateCompanyPhoto', 'SettingsController@updateCompanyPhoto')->name('updateCompanyPhoto');
Route::post('updateCompanyDetails', 'SettingsController@updateCompanyDetails');

Route::get('batches', 'BatchesController@batches');
Route::post('addbatch', 'BatchesController@addbatch');
Route::post('deletebatch', 'BatchesController@deletebatch');
Route::post('editbatch', 'BatchesController@editbatch');
Route::get('stockreport', 'BatchesController@stockreport');
Route::get('filterstockreport', 'BatchesController@filterstockreport');
Route::post('postfilterstockreport', 'BatchesController@postfilterstockreport');
Route::get('postfilterstockreport', 'BatchesController@filterstockreport');

Route::post('ejectbatch', 'BatchesController@ejectbatch');

Route::post('updatequantity', 'BatchesController@updatequantity');

Route::get('exportfilterstockexcel/{fromdate}/{todate}', 'BatchesController@exportfilterstockexcel')->name('exportfilterstockexcel');
Route::get('exportfilterstockpdf/{fromdate}/{todate}', 'BatchesController@exportfilterstockpdf')->name('exportfilterstockpdf');

Route::get('activities', 'AccountController@activities');

Route::get('salesReport', 'OrdersController@salesReport');
Route::post('postSalesReport', 'OrdersController@postSalesReport');
Route::get('postSalesReport', 'OrdersController@salesReport');
Route::post('deleteorder', 'OrdersController@deleteorder');

Route::get('roles', 'RolesController@roles');
Route::post('addrole', 'RolesController@addrole');
Route::post('deleterole', 'RolesController@deleterole');
Route::post('editrole', 'RolesController@editrole');

Route::get('categories', 'CategoriesController@categories');
Route::post('addcategory', 'CategoriesController@addcategory');
Route::post('deletecategory', 'CategoriesController@deletecategory');
Route::post('editcategory', 'CategoriesController@editcategory');

Route::get('accounttransfers', 'AccounttransfersController@accounttransfers');
Route::post('addaccounttransfer', 'AccounttransfersController@addaccounttransfer');
Route::post('deleteaccounttransfer', 'AccounttransfersController@deleteaccounttransfer');
Route::post('editaccounttransfer', 'AccounttransfersController@editaccounttransfer');

Route::get('accounts', 'AccountsController@accounts');
Route::post('addaccount', 'AccountsController@addaccount');
Route::post('deleteaccount', 'AccountsController@deleteaccount');
Route::post('editaccount', 'AccountsController@editaccount');

Route::get('companyassets', 'CompanyassetsController@companyassets');
Route::post('addcompanyasset', 'CompanyassetsController@addcompanyasset');
Route::post('deletecompanyasset', 'CompanyassetsController@deletecompanyasset');
Route::post('editcompanyasset', 'CompanyassetsController@editcompanyasset');

Route::get('expenses', 'ExpensesController@expenses');
Route::post('addexpense', 'ExpensesController@addexpense');
Route::post('deleteexpense', 'ExpensesController@deleteexpense');
Route::post('editexpense', 'ExpensesController@editexpense');
Route::post('postexpensesReport', 'ExpensesController@postexpensesReport');
Route::get('postexpensesReport', 'ExpensesController@expenses');

Route::get('expensetypes', 'ExpensetypesController@expensetypes');
Route::post('addexpensetype', 'ExpensetypesController@addexpensetype');
Route::post('deleteexpensetype', 'ExpensetypesController@deleteexpensetype');
Route::post('editexpensetype', 'ExpensetypesController@editexpensetype');

Route::get('users', 'UsersController@users');
Route::post('adduser', 'UsersController@adduser');
Route::post('deleteuser', 'UsersController@deleteuser');
Route::post('edituser', 'UsersController@edituser');

Route::get('payments', 'PaymentsController@payments');
Route::post('paymentsReport', 'PaymentsController@paymentsReport');
Route::get('paymentsReport', 'PaymentsController@payments');

Route::get('permissions', 'PermissionsController@permissions');
Route::get('updatepermission', 'PermissionsController@updatepermission');

Route::get('notifications', 'NotificationsController@notifications');
Route::post('addnotification', 'NotificationsController@addnotification');

Route::post('addproduct', 'ProductsController@addproduct')->name('addproduct');
Route::post('editproduct', 'ProductsController@editproduct')->name('editproduct');

Route::get('getProducts', 'ProductsController@getProducts')->name('getProducts');
Route::get('getProduct', 'ProductsController@getProduct')->name('getProduct');
Route::get('getShopProducts', 'ProductsController@getShopProducts')->name('getShopProducts');
Route::get('getShopProductsForSale', 'ProductsController@getShopProductsForSale')->name('getShopProductsForSale');

Route::get('products', 'ProductsController@products');
Route::post('deleteproduct', 'ProductsController@deleteproduct');

Route::post('addshopproduct', 'ShopproductsController@addshopproduct')->name('addshopproduct');
Route::post('editshopproduct', 'ShopproductsController@editshopproduct')->name('editshopproduct');
Route::get('shopproducts', 'ShopproductsController@shopproducts');
Route::post('deleteshopproduct', 'ShopproductsController@deleteshopproduct');

Route::get('customers', 'CustomersController@customers');
Route::get('debtors', 'CustomersController@debtors');
Route::post('addcustomer', 'CustomersController@addcustomer');
Route::post('deletecustomer', 'CustomersController@deletecustomer');
Route::post('editcustomer', 'CustomersController@editcustomer');

Route::get('suppliers', 'SuppliersController@suppliers');
Route::get('creditors', 'SuppliersController@creditors');
Route::post('addsupplier', 'SuppliersController@addsupplier');
Route::post('deletesupplier', 'SuppliersController@deletesupplier');
Route::post('editsupplier', 'SuppliersController@editsupplier');

Route::get('sales', 'SaleController@sales');
Route::get('completesale', 'SaleController@completesale')->name('completesale');
Route::post('postfiltersalereport', 'SaleController@postfiltersalereport');
Route::get('postfiltersalereport', 'SaleController@sales');

Route::get('exportfiltersaleexcel/{fromdate}/{todate}', 'SaleController@exportfiltersaleexcel')->name('exportfiltersaleexcel');
Route::get('exportfiltersalepdf/{fromdate}/{todate}', 'SaleController@exportfiltersalepdf')->name('exportfiltersalepdf');

});

Route::post('flutterwaveverify', 'FlutterwaveController@flutterwaveverify')->name('flutterwaveverify');
Route::post('flutterwaveinitiate', 'FlutterwaveController@flutterwaveinitiate')->name('flutterwaveinitiate');

//Cron
Route::get('cronsms', 'CronsController@cronsms');
