<footer class="page-footer">
    <div class="font-13">©<?php echo date('Y'); ?> <b><?php echo env("APP_NAME"); ?></b> - All rights reserved.</div>
    <a class="px-4" href="https://alternet.co.ke" target="_blank">Alternet Limited</a>
    <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
</footer>
