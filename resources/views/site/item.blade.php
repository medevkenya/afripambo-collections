<?php foreach ($items as $keyitem) {
	$photoUrlItem = \App\Photos::getRandom($keyitem->productId);
	?>
	<div class="product-widget">
		<a href="<?php $url = URL::to("/product/".$keyitem->slug); print_r($url); ?>">
		<div class="product-img">
			<img src="{{ URL::to('/') }}/public/photos/<?php echo $photoUrlItem; ?>" alt="">
		</div>
		<div class="product-body">
			<p class="product-category"><?php echo $keyitem->categoryName; ?></p>
			<h3 class="product-name"><?php echo $keyitem->productName; ?></h3>
			<h4 class="product-price">Ksh. <?php echo $keyitem->outPrice; ?> <?php if($keyitem->discount !=0 && $keyitem->discount !=null) {
				$currentPrice = $keyitem->outPrice;
				$percent = ($keyitem->outPrice * $keyitem->discount) / 100;
				$oldPrice = $currentPrice + $percent;
				?>
			<del class="product-old-price">Ksh. <?php echo $oldPrice; ?></del>
		<?php } ?>
	</h4>
		</div>
	</a>
	</div>
<?php } ?>
