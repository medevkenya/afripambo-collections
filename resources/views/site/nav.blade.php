<?php $fcats = \App\Categories::getAll(0); ?>
<nav id="navigation">
  <!-- container -->
  <div class="container">
    <!-- responsive-nav -->
    <div id="responsive-nav">
      <!-- NAV -->
      <ul class="main-nav nav navbar-nav">
        <li class="active"><a href="{{URL::to('/')}}">Home</a></li>
        <?php foreach ($fcats as $keysf) { ?>
        <li><a href="<?php $url = URL::to("/category/".$keysf->slug); print_r($url); ?>"><?php echo $keysf->categoryName; ?></a></li>
      <?php } ?>
      </ul>
      <!-- /NAV -->
    </div>
    <!-- /responsive-nav -->
  </div>
  <!-- /container -->
</nav>
