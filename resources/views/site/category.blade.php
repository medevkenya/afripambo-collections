<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $categoryDetails->categoryName; ?></title>
    @include('site.headerlinks')
    </head>
	<body>
		<!-- HEADER -->
    @include('site.header')
		<!-- /HEADER -->

		<!-- NAVIGATION -->
    @include('site.nav')
		<!-- /NAVIGATION -->

		<!-- BREADCRUMB -->
<div id="breadcrumb" class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb-tree">
					<li><a href="{{URL::to('/')}}">Home</a></li>
					<li class="active"><?php echo $categoryDetails->categoryName; ?> (<?php echo count($homeproducts); ?> Results)</li>
				</ul>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /BREADCRUMB -->

<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">

			<!-- ASIDE -->
			@include('site.leftbar')
			<!-- /ASIDE -->

			<!-- STORE -->
			<div id="store" class="col-md-9">
				<!-- store top filter -->
				<div class="store-filter clearfix">
					<div class="store-sort">
						<label>
							Sort By:
							<select class="input-select" onchange="sortby()" id="sort_by_id">
								<option value="0">Latest</option>
								<option value="1">Nearest</option>
								<option value="2">High price to Low</option>
								<option value="3">Low price to High</option>
							</select>
						</label>

						<label>
							Show:
							<select class="input-select" onchange="show()" id="show_id">
								<option value="0">All</option>
								<option value="1">New</option>
								<option value="2">Used</option>
							</select>
						</label>
					</div>
					<ul class="store-grid">
						<li class="active"><a href="{{url()->current()}}"><i class="fa fa-th whitecolor"></i></a></li>
						<li><a href="{{URL::to('/')}}"><i class="fa fa-th-list"></i></a></li>
					</ul>
				</div>
				<!-- /store top filter -->

				<!-- store products -->
				<div class="row">

					@include('site.productitem2')

					<!-- <div class="clearfix visible-lg visible-md visible-sm visible-xs"></div>
					<div class="clearfix visible-sm visible-xs"></div> -->

				</div>
				<!-- /store products -->

				<!-- store bottom filter -->
				<div class="store-filter clearfix">
					{!! $homeproducts->render() !!}
					<!-- <span class="store-qty">Showing 20-100 products</span> -->
					<!-- <ul class="store-pagination">
						<li class="active">1</li>
						<li><a href="#">2</a></li>
						<li><a href="#">3</a></li>
						<li><a href="#">4</a></li>
						<li><a href="#"><i class="fa fa-angle-right"></i></a></li>
					</ul> -->
				</div>
				<!-- /store bottom filter -->
			</div>
			<!-- /STORE -->
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /SECTION -->

		<!-- NEWSLETTER -->
    @include('site.newsletter')
		<!-- /NEWSLETTER -->

		<!-- FOOTER -->
		@include('site.footer')
		<!-- /FOOTER -->

    @include('site.footerlinks')

	</body>
</html>
