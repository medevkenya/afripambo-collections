<div id="aside" class="col-md-3">
  <!-- aside Widget -->
  <div class="aside">
    <h3 class="aside-title">Categories</h3>
    <div class="checkbox-filter">

      <?php
        $ffcats = \App\Categories::getAll(0);
        foreach ($ffcats as $keyfd) {
        $count = \App\Shopproducts::countByCategoryId($keyfd->id);
        ?>
      <a href="<?php $url = URL::to("/category/".$keyfd->slug); print_r($url); ?>">
      <div class="input-checkbox">
        <i class="fa fa-th-list"></i>
        <label for="category-1">
          <span></span>
          <?php echo $keyfd->categoryName; ?>
          <small>(<?php echo $count; ?>)</small>
        </label>
      </div>
    </a>
      <?php } ?>

    </div>
  </div>
  <!-- /aside Widget -->

  <!-- aside Widget -->
  <!-- <div class="aside">
    <h3 class="aside-title">Price</h3>
    <div class="price-filter">
      <div id="price-slider"></div>
      <div class="input-number price-min">
        <input id="price-min" type="number">
        <span class="qty-up">+</span>
        <span class="qty-down">-</span>
      </div>
      <span>-</span>
      <div class="input-number price-max">
        <input id="price-max" type="number">
        <span class="qty-up">+</span>
        <span class="qty-down">-</span>
      </div>
    </div>
  </div> -->
  <!-- /aside Widget -->

  <!-- aside Widget -->
  <div class="aside">
    <h3 class="aside-title">Brands</h3>
    <div class="checkbox-filter">

      <?php
        $brandfcats = \App\Brands::getAll(0);
        foreach ($brandfcats as $keybr) {
        $brandCount = \App\Shopproducts::countByBrandId($keybr->id);
        ?>
      <a href="<?php $url = URL::to("/brand/".$keybr->slug); print_r($url); ?>">
      <div class="input-checkbox">
        <i class="fa fa-th-list"></i>
        <label for="brand-1">
          <span></span>
          <?php echo $keybr->brandName; ?>
          <small>(<?php echo $brandCount; ?>)</small>
        </label>
      </div>
    </a>
      <?php
        }
        ?>

    </div>
  </div>
  <!-- /aside Widget -->

  <!-- aside Widget -->
  <div class="aside">
    <h3 class="aside-title">Top selling</h3>

    <?php $items = \App\Shopproducts::topProducts1(); ?>
    <!-- product widget -->
    @include('site.item')
    <!-- /product widget -->

  </div>
  <!-- /aside Widget -->
</div>
