<div class="section">
  <!-- container -->
  <div class="container">
    <!-- row -->
    <div class="row">
      <?php $sponscats = \App\Categories::getAll(3); foreach ($sponscats as $keysp) { ?>
      <!-- shop -->
      <div class="col-md-4 col-xs-6">
        <div class="shop">
          <div class="shop-img">
            <img style="max-height:200px;" src="{{ URL::to('/') }}/public/categoryphotos/<?php echo $keysp->photoUrl; ?>" alt="<?php echo $keysp->categoryName; ?>">
          </div>
          <div class="shop-body">
            <h3><?php echo $keysp->categoryName; ?></h3>
            <a href="<?php $url = URL::to("/category/".$keysp->slug); print_r($url); ?>" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
      <!-- /shop -->
    <?php } ?>
    </div>
    <!-- /row -->
  </div>
  <!-- /container -->
</div>
