<?php
$settings = \App\Settings::getDetails();
?>
<div id="newsletter" class="section">
  <!-- container -->
  <div class="container">
    <!-- row -->
    <div class="row">
      <div class="col-md-12">
        <div class="newsletter">
          <p>Sign Up for the <strong>NEWSLETTER</strong></p>
          <form onsubmit="event.preventDefault();">
            <div id="writeinfo" class="alert alert-success" style="display:none;"></div>
                    <div class="ajax-loader">
                      <img src="{{ URL::to('/') }}/public/images/loading.gif" class="img-responsive" />
                    </div>
            <input class="input" type="email" name="email" id="subscribeemail" placeholder="Enter Your Email">
            <button class="newsletter-btn" id="subscribebutton"><i class="fa fa-envelope"></i> Subscribe</button>
          </form>
          <ul class="newsletter-follow">
            <li>
              <a href="<?php echo $settings->facebook; ?>" target="_blank"><i class="fa fa-facebook"></i></a>
            </li>
            <li>
              <a href="<?php echo $settings->twitter; ?>" target="_blank"><i class="fa fa-twitter"></i></a>
            </li>
            <li>
              <a href="<?php echo $settings->instagram; ?>" target="_blank"><i class="fa fa-instagram"></i></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
    <!-- /row -->
  </div>
  <!-- /container -->
</div>
