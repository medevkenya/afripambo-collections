<?php $fcats = \App\Categories::getAll(0); ?>
<?php
$settings = \App\Settings::getDetails();
?>
<header>
  <!-- TOP HEADER -->
  <div id="top-header">
    <div class="container">
      <ul class="header-links pull-left">
        <li><a href="{{URL::to('/')}}"><i class="fa fa-phone"></i> <?php echo $settings->companyTelephone; ?></a></li>
        <li><a href="{{URL::to('/')}}"><i class="fa fa-envelope-o"></i> <?php echo $settings->companyEmail; ?></a></li>
        <li><a href="{{URL::to('/')}}"><i class="fa fa-map-marker"></i> <?php echo $settings->companyLocation; ?></a></li>
      </ul>
      <ul class="header-links pull-right">
        <li><a href="{{URL::to('/signin')}}"><i class="fa fa-sign-in"></i> Sign In</a></li>
        <li><a href="{{URL::to('/register')}}"><i class="fa fa-user-plus"></i> Create Account</a></li>
      </ul>
    </div>
  </div>
  <!-- /TOP HEADER -->

  <!-- MAIN HEADER -->
  <div id="header">
    <!-- container -->
    <div class="container">
      <!-- row -->
      <div class="row">
        <!-- LOGO -->
        <div class="col-md-3">
          <div class="header-logo">
            <a href="{{URL::to('/')}}" class="logo">
              <img src="{{ URL::asset('images/logo-afripambo.png')}}" width="90%" alt="">
            </a>
          </div>
        </div>
        <!-- /LOGO -->

        <!-- SEARCH BAR -->
        <div class="col-md-5">
          <div class="header-search">
            <form>
              <input class="input" placeholder="Search here">
              <button class="search-btn">Search</button>
            </form>
          </div>
        </div>
        <!-- /SEARCH BAR -->

        <!-- ACCOUNT -->
        <div class="col-md-4 clearfix">
          <div class="header-ctn">
            <!-- Wishlist -->
            <!-- <div>
              <a href="#" onclick="getLocation()">
                <i class="fa fa-map-marker"></i>
                <span>Nearest</span>
              </a>
            </div> -->
            <!-- /Wishlist -->
            <!-- Wishlist -->
            <div>
              <a href="{{URL::to('/hotdeals')}}">
                <i class="fa fa-heart-o"></i>
                <span>Your Deals</span>
                <div class="qty"><?php echo \App\Shopproducts::countHotDeals(); ?></div>
              </a>
            </div>
            <!-- /Wishlist -->

            <!-- Cart -->
            <div class="dropdown">
              <a class="dropdown-toggle my-cart-icon" aria-expanded="true">
                <i class="fa fa-shopping-cart"></i>
                <span>Your Cart</span>
                <div class="qty"><span class="badge-notify my-cart-badge"></span></div>
                <!-- <span class="glyphicon glyphicon-shopping-cart my-cart-icon"><span class="badge badge-notify my-cart-badge"></span></span> -->
              </a>
              <!-- <div class="cart-dropdown">
                <div class="cart-list">
                  <div class="product-widget">
                    <div class="product-img">
                      <img src="{{ URL::asset('site/img/product01.png')}}" alt="">
                    </div>
                    <div class="product-body">
                      <h3 class="product-name"><a href="#">product name goes here</a></h3>
                      <h4 class="product-price"><span class="qty">1x</span>$980.00</h4>
                    </div>
                    <button class="delete"><i class="fa fa-close"></i></button>
                  </div>

                  <div class="product-widget">
                    <div class="product-img">
                      <img src="{{ URL::asset('site/img/product02.png')}}" alt="">
                    </div>
                    <div class="product-body">
                      <h3 class="product-name"><a href="#">product name goes here</a></h3>
                      <h4 class="product-price"><span class="qty">3x</span>$980.00</h4>
                    </div>
                    <button class="delete"><i class="fa fa-close"></i></button>
                  </div>
                </div>
                <div class="cart-summary">
                  <small>3 Item(s) selected</small>
                  <h5>SUBTOTAL: $2940.00</h5>
                </div>
                <div class="cart-btns">
                  <a href="#">View Cart</a>
                  <a href="#">Checkout  <i class="fa fa-arrow-circle-right"></i></a>
                </div>
              </div> -->
            </div>
            <!-- /Cart -->

            <!-- Menu Toogle -->
            <div class="menu-toggle">
              <a href="{{URL::to('/')}}">
                <i class="fa fa-bars"></i>
                <span>Menu</span>
              </a>
            </div>
            <!-- /Menu Toogle -->
          </div>
        </div>
        <!-- /ACCOUNT -->
      </div>
      <!-- row -->
    </div>
    <!-- container -->
  </div>
  <!-- /MAIN HEADER -->
</header>
