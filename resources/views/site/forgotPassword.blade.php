<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Forgot Password</title>
    @include('site.headerlinks')
    </head>
	<body>
		<!-- HEADER -->
    @include('site.header')
		<!-- /HEADER -->

		<!-- NAVIGATION -->
    @include('site.nav')
		<!-- /NAVIGATION -->

		<?php
		$settings = \App\Settings::getDetails();
		?>

		<!-- BREADCRUMB -->
<div id="breadcrumb" class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<h3 class="breadcrumb-header">Forgot Password</h3>
				<ul class="breadcrumb-tree">
					<li><a href="{{URL::to('/')}}"><?php echo $settings->companyName; ?></a></li>
				</ul>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /BREADCRUMB -->

<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">

			@if (count($errors) > 0)
				 <div class="alert alert-danger">
						 <ul>
								 @foreach ($errors->all() as $error)
								 <li>{{ $error }}</li>
								 @endforeach
						 </ul>
				 </div>
				@endif

				@if ($message = Session::get('error'))
						 <div class="alert alert-danger">
								 {{ $message }}
						 </div>
				@endif

				@if ($message = Session::get('success'))
						 <div class="alert alert-success">
								 {{ $message }}
						 </div>
				@endif

				@if (session('status0'))
				<div class="alert alert-danger alert-dismissible alertbox" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ session('status0') }}
				</div>
				@endif

				@if (session('status1'))
				<div class="alert alert-success alert-dismissible alertbox" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ session('status1') }}
				</div>
				@endif

				<div class="col-md-7">
					<!-- Billing Details -->
					<div class="billing-details">
						<div class="section-title">
							<h6 class="title">Provide your email address</h6>
						</div>
						<div class="form-group">
							<input class="input" type="email" name="email" id="email" value="{{ old('email') }}" placeholder="Email Address" required>
							@if ($errors->has('email'))
                <span class="text-danger">{{ $errors->first('email') }}</span>
              @endif
						</div>
						<p>If you have your password <a href="{{URL::to('/signin')}}">Click Here to Sign In</a></p>
						<button class="primary-btn order-submit" type="submit">Recover Password</button>
					</div>
					<!-- /Billing Details -->
				</div>

		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /SECTION -->

		<!-- NEWSLETTER -->
    @include('site.newsletter')
		<!-- /NEWSLETTER -->

		<!-- FOOTER -->
		@include('site.footer')
		<!-- /FOOTER -->

    @include('site.footerlinks')

	</body>
</html>
