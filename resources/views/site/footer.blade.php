<?php
$settings = \App\Settings::getDetails();
?>
<footer id="footer">
  <!-- top footer -->
  <div class="section">
    <!-- container -->
    <div class="container">
      <!-- row -->
      <div class="row">
        <div class="col-md-3 col-xs-6">
          <div class="footer">
            <h3 class="footer-title">About Us</h3>
            <p><?php echo $settings->companyAbout; ?></p>
            <ul class="footer-links">
              <li><a href="#"><i class="fa fa-map-marker"></i><?php echo $settings->companyLocation; ?></a></li>
              <li><a href="#"><i class="fa fa-phone"></i><?php echo $settings->companyTelephone; ?></a></li>
              <li><a href="#"><i class="fa fa-envelope-o"></i><?php echo $settings->companyEmail; ?></a></li>
            </ul>
          </div>
        </div>

        <div class="col-md-3 col-xs-6">
          <div class="footer">
            <h3 class="footer-title">Categories</h3>
            <ul class="footer-links">
              <?php $footercats = \App\Categories::getAll(0); foreach ($footercats as $keyfo) {?>
              <li><a href="<?php $url = URL::to("/category/".$keyfo->slug); print_r($url); ?>"><?php echo $keyfo->categoryName; ?></a></li>
              <?php } ?>
            </ul>
          </div>
        </div>

        <div class="clearfix visible-xs"></div>

        <div class="col-md-3 col-xs-6">
          <div class="footer">
            <h3 class="footer-title">Information</h3>
            <ul class="footer-links">
              <li><a href="{{URL::to('/about')}}">About Us</a></li>
              <li><a href="{{URL::to('/privacy')}}">Privacy Policy</a></li>
              <li><a href="{{URL::to('/orderreturns')}}">Orders and Returns</a></li>
              <li><a href="{{URL::to('/terms')}}">Terms & Conditions</a></li>
            </ul>
          </div>
        </div>

        <div class="col-md-3 col-xs-6">
          <div class="footer">
            <h3 class="footer-title">Service</h3>
            <ul class="footer-links">
              <li><a href="{{URL::to('/account')}}">My Account</a></li>
              <li><a href="{{URL::to('/signin')}}">Sign In</a></li>
              <li><a href="{{URL::to('/signup')}}">Sign Up</a></li>
            </ul>
          </div>
        </div>
      </div>
      <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /top footer -->

  <!-- bottom footer -->
  <div id="bottom-footer" class="section">
    <div class="container">
      <!-- row -->
      <div class="row">
        <div class="col-md-12 text-center">
          <ul class="footer-payments">
                        <li><a href="#"><img src="{{ URL::asset('images/mpesa.jpg')}}" alt="M-PESA" class="mpesa-payment-" style="height:34px;margin-top: -6%;border-radius:6px;"></a></li>
            <li><a href="#"><i class="fa fa-cc-visa whitecolor"></i></a></li>
            <li><a href="#"><i class="fa fa-credit-card whitecolor"></i></a></li>
            <li><a href="#"><i class="fa fa-cc-paypal whitecolor"></i></a></li>
            <li><a href="#"><i class="fa fa-cc-mastercard whitecolor"></i></a></li>
            <li><a href="#"><i class="fa fa-cc-discover whitecolor"></i></a></li>
          </ul>
          <span class="copyright">
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | <a href="{{URL::to('/')}}" class="whitecolor"><?php echo $settings->companyName; ?></a>
          </span>
        </div>
      </div>
        <!-- /row -->
    </div>
    <!-- /container -->
  </div>
  <!-- /bottom footer -->
</footer>
