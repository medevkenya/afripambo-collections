<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Kiondo Size 6,8,12 and 15, Sandals and African Fashion | <?php echo env("APP_NAME"); ?></title>
		<meta name="description" content="At Afripambo we have Kiondo Size 6,8,12 and 15, Sandals and African Fashion. Very Cultural & Stylish, We deliver at a fee. Pay for order on Mpesa: Till No 5902429. For orders and enquiries call, text or whatsapp us on 0790528599">
	  <meta name="keywords" content="Kiondo bags, Kiondo size 6, Kiondo size 8, Kiondo size 12, Kiondo size 15, Kiondo bag for shopping, Stylish bags, African bags, Sandals, African Fashion">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="robots" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
		<link rel="canonical" href="<?php echo url()->current(); ?>" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:title" content="Kiondo Size 6,8,12 and 15, Sandals and African Fashion | <?php echo env("APP_NAME"); ?>" />
		<meta property="og:description" content="At Afripambo we have Kiondo Size 6,8,12 and 15, Sandals and African Fashion. Very Cultural & Stylish, We deliver at a fee. Pay for order on Mpesa: Till No 5902429. For orders and enquiries call, text or whatsapp us on 0790528599" />
		<meta property="og:url" content="<?php echo url()->current(); ?>" />
		<meta property="og:site_name" content="<?php echo env("APP_NAME"); ?>" />
		<meta name="twitter:card" content="summary_large_image">
	  <meta name="twitter:site" content="@afripambo"/>
	  <meta name="twitter:domain" content="afripambo.com"/>
	  <meta name="twitter:creator" content="@afripambo">
		<meta property="og:type" content="website"/>
	  <meta property="og:image" content="{{ URL::asset('images/logo.png')}}"/>
	  <meta name="twitter:image" content="{{ URL::asset('images/logo.png')}}"/>
    @include('site.headerlinks')
    </head>
	<body>
		<!-- HEADER -->
    @include('site.header')
		<!-- /HEADER -->

		<!-- NAVIGATION -->
    @include('site.nav')
		<!-- /NAVIGATION -->

		<!-- SECTION -->
    @include('site.sponsored')
		<!-- /SECTION -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<!-- section title -->
					<div class="col-md-12">
						<div class="section-title">
							<h3 class="title">New Products</h3>
							<div class="section-nav">
								@include('site.featuredcategories')
							</div>
						</div>
					</div>
					<!-- /section title -->

					<!-- Products tab & slick -->
					<div class="col-md-12">
						<div class="row">
							<div class="products-tabs">
								<!-- tab -->
								<div id="tab1" class="tab-pane active">
									<div class="products-slick" data-nav="#slick-nav-1">
										<?php $homeproducts = \App\Shopproducts::getAll(8); ?>
										<!-- product -->
										@include('site.productitem')
										<!-- /product -->
									</div>
									<div id="slick-nav-1" class="products-slick-nav"></div>
								</div>
								<!-- /tab -->
							</div>
						</div>
					</div>
					<!-- Products tab & slick -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- HOT DEAL SECTION -->
    @include('site.hotdeal')
		<!-- /HOT DEAL SECTION -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<!-- section title -->
					<div class="col-md-12">
						<div class="section-title">
							<h3 class="title">Featured</h3>
							<div class="section-nav">
								@include('site.featuredcategories')
							</div>
						</div>
					</div>
					<!-- /section title -->

					<!-- Products tab & slick -->
					<div class="col-md-12">
						<div class="row">
							<div class="products-tabs">
								<!-- tab -->
								<div id="tab2" class="tab-pane fade in active">
									<div class="products-slick" data-nav="#slick-nav-2">

										<?php $homeproducts = \App\Shopproducts::getFeatured(); ?>
										<!-- product -->
										@include('site.productitem')
										<!-- /product -->

										<!-- /product -->
									</div>
									<div id="slick-nav-2" class="products-slick-nav"></div>
								</div>
								<!-- /tab -->
							</div>
						</div>
					</div>
					<!-- /Products tab & slick -->
				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- SECTION -->
		<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">
					<div class="col-md-4 col-xs-6">
						<div class="section-title">
							<h4 class="title">Top selling</h4>
							<div class="section-nav">
								<div id="slick-nav-3" class="products-slick-nav"></div>
							</div>
						</div>

						<div class="products-widget-slick" data-nav="#slick-nav-3">
							<div>

								<?php $items = \App\Shopproducts::topProducts1(); ?>
								<!-- product widget -->
								@include('site.item')
								<!-- /product widget -->

							</div>

							<div>

								<?php $items = \App\Shopproducts::topProducts2(); ?>
								<!-- product widget -->
								@include('site.item')
								<!-- /product widget -->

							</div>
						</div>
					</div>

					<div class="col-md-4 col-xs-6">
						<div class="section-title">
							<h4 class="title">Trending</h4>
							<div class="section-nav">
								<div id="slick-nav-4" class="products-slick-nav"></div>
							</div>
						</div>

						<div class="products-widget-slick" data-nav="#slick-nav-4">
							<div>

								<?php $items = \App\Shopproducts::trendingProducts1(); ?>
								<!-- product widget -->
								@include('site.item')
								<!-- /product widget -->

							</div>

							<div>

								<?php $items = \App\Shopproducts::trendingProducts2(); ?>
								<!-- product widget -->
								@include('site.item')
								<!-- /product widget -->

							</div>
						</div>
					</div>

					<div class="clearfix visible-sm visible-xs"></div>

					<div class="col-md-4 col-xs-6">
						<div class="section-title">
							<h4 class="title">Popular</h4>
							<div class="section-nav">
								<div id="slick-nav-5" class="products-slick-nav"></div>
							</div>
						</div>

						<div class="products-widget-slick" data-nav="#slick-nav-5">
							<div>

								<?php $items = \App\Shopproducts::popularProducts1(); ?>
								<!-- product widget -->
								@include('site.item')
								<!-- /product widget -->

							</div>

							<div>

								<?php $items = \App\Shopproducts::popularProducts2(); ?>
								<!-- product widget -->
								@include('site.item')
								<!-- /product widget -->

							</div>
						</div>
					</div>

				</div>
				<!-- /row -->
			</div>
			<!-- /container -->
		</div>
		<!-- /SECTION -->

		<!-- NEWSLETTER -->
    @include('site.newsletter')
		<!-- /NEWSLETTER -->

		<!-- FOOTER -->
		@include('site.footer')
		<!-- /FOOTER -->

    @include('site.footerlinks')

	</body>
</html>
