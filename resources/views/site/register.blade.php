<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Sign into your account</title>
    @include('site.headerlinks')
    </head>
	<body>
		<!-- HEADER -->
    @include('site.header')
		<!-- /HEADER -->

		<!-- NAVIGATION -->
    @include('site.nav')
		<!-- /NAVIGATION -->

		<?php
		$settings = \App\Settings::getDetails();
		?>

		<!-- BREADCRUMB -->
<div id="breadcrumb" class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<h3 class="breadcrumb-header">Sign In</h3>
				<ul class="breadcrumb-tree">
					<li><a href="{{URL::to('/')}}"><?php echo $settings->companyName; ?></a></li>
				</ul>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /BREADCRUMB -->

<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">

			@if (count($errors) > 0)
				 <div class="alert alert-danger">
						 <ul>
								 @foreach ($errors->all() as $error)
								 <li>{{ $error }}</li>
								 @endforeach
						 </ul>
				 </div>
				@endif

				@if ($message = Session::get('error'))
						 <div class="alert alert-danger">
								 {{ $message }}
						 </div>
				@endif

				@if ($message = Session::get('success'))
						 <div class="alert alert-success">
								 {{ $message }}
						 </div>
				@endif

				@if (session('status0'))
				<div class="alert alert-danger alert-dismissible alertbox" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ session('status0') }}
				</div>
				@endif

				@if (session('status1'))
				<div class="alert alert-success alert-dismissible alertbox" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				{{ session('status1') }}
				</div>
				@endif

				<div class="col-md-7">
					<!-- Billing Details -->
					<div class="billing-details">
						<div class="section-title">
							<h6 class="title">Provide your account details</h6>
						</div>
						<div class="form-group">
							<input class="input" type="text" name="firstName" id="firstName" value="{{ old('firstName') }}" placeholder="First Name" required>
						</div>
						<div class="form-group">
							<input class="input" type="text" name="lastName" id="lastName" value="{{ old('lastName') }}" placeholder="Last Name" required>
						</div>
						<div class="form-group">
							<input class="input" type="tel" name="mobileNo" id="mobileNo" value="{{ old('mobileNo') }}" placeholder="Telephone / Mobile Number">
						</div>
						<div class="form-group">
							<input class="input" type="email" name="email" id="email" value="{{ old('email') }}" placeholder="Email Address" required>
							@if ($errors->has('email'))
                <span class="text-danger">{{ $errors->first('email') }}</span>
              @endif
						</div>
						<div class="form-group">
							<input class="input" type="password" name="password" id="password" value="{{ old('password') }}" placeholder="Enter Your Password" required>
							@if ($errors->has('password'))
                <span class="text-danger">{{ $errors->first('password') }}</span>
              @endif
						</div>
						<p>By continuing you agree that you've read and acceptted the <a href="{{URL::to('/terms')}}">terms & conditions</a></p>
						<button class="primary-btn order-submit" type="submit">Register</button>
					</div>
					<!-- /Billing Details -->
				</div>

		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /SECTION -->

		<!-- NEWSLETTER -->
    @include('site.newsletter')
		<!-- /NEWSLETTER -->

		<!-- FOOTER -->
		@include('site.footer')
		<!-- /FOOTER -->

    @include('site.footerlinks')

	</body>
</html>
