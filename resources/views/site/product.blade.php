<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $details->productName; ?></title>
		<meta name="description" content="<?php echo $details->description; ?>">
    @include('site.headerlinks')
    </head>
	<body>
		<!-- HEADER -->
    @include('site.header')
		<!-- /HEADER -->

		<!-- NAVIGATION -->
    @include('site.nav')
		<!-- /NAVIGATION -->

		<!-- BREADCRUMB -->
<div id="breadcrumb" class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb-tree">
					<li><a href="{{URL::to('/')}}">Home</a></li>
					<li><?php echo $details->categoryName; ?></li>
					<li class="active"><?php echo $details->productName; ?></li>
				</ul>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /BREADCRUMB -->

<?php
$photos = \App\Photos::getPhotos($details->id);
$randomphotoUrl = \App\Photos::getRandom($details->id);
$rates = \App\Productratings::getopenProductratings($details->id);
$limitedrates = \App\Productratings::getLimitedProductratings($details->id);
$rate = \App\Productratings::getRating($details->id);
$shopproductcolors = \App\Shopproductcolors::getAll($details->id);
$shopproductsizes = \App\Shopproductsizes::getAll($details->id);
?>

<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<!-- Product main img -->
			<div class="col-md-5 col-md-push-2">
				<div id="product-main-img">

					<?php foreach ($photos as $keyphoto) { ?>
					<div class="product-preview">
						<img src="{{ URL::to('/') }}/public/photos/<?php echo $keyphoto->photoUrl; ?>" alt="">
					</div>
				  <?php } ?>

				</div>
			</div>
			<!-- /Product main img -->

			<!-- Product thumb imgs -->
			<div class="col-md-2  col-md-pull-5">
				<div id="product-imgs">

					<?php foreach ($photos as $keyphoto) { ?>
					<div class="product-preview">
						<img src="{{ URL::to('/') }}/public/photos/<?php echo $keyphoto->photoUrl; ?>" alt="">
					</div>
					<?php } ?>

				</div>
			</div>
			<!-- /Product thumb imgs -->

			<!-- Product details -->
			<div class="col-md-5">
				<div class="product-details">

					@if (count($errors) > 0)
           <div class="alert alert-danger">
               <ul>
                   @foreach ($errors->all() as $error)
                   <li style="text-align:center;">{{ $error }}</li>
                   @endforeach
               </ul>
           </div>
          @endif

          @if ($message = Session::get('error'))
               <div class="alert alert-danger" style="text-align:center;">
                   {{ $message }}
               </div>
          @endif

          @if ($message = Session::get('success'))
               <div class="alert alert-success" style="text-align:center;">
                   {{ $message }}
               </div>
          @endif

					<h2 class="product-name"><?php echo $details->productName; ?></h2>
					<div>
						<div class="product-rating">
							<?php if($rate==0) { ?>
																		<i class="fa fa-star-o"></i>
																		<i class="fa fa-star-o"></i>
																		<i class="fa fa-star-o"></i>
																		<i class="fa fa-star-o"></i>
																		<i class="fa fa-star-o"></i>
																	<?php } else if($rate==1) { ?>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																		<i class="fa fa-star-o"></i>
																		<i class="fa fa-star-o"></i>
																		<i class="fa fa-star-o"></i>
																	<?php } else if($rate==2) { ?>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																		<i class="fa fa-star-o"></i>
																		<i class="fa fa-star-o"></i>
																	<?php } else if($rate==3) { ?>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																		<i class="fa fa-star-o"></i>
																	<?php } else if($rate==4) { ?>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star-o"></i>
																	<?php } else if($rate==5) { ?>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																	<?php } ?>
						</div>
						<a class="review-link" href="#"><?php echo count($rates); ?> Review(s) | <span data-toggle="tab" href="#tab3">Add your review</span></a>
					</div>
					<div>
						<h3 class="product-price">Ksh. <?php echo number_format($details->outPrice,0); ?>
							<?php if($details->discount !=0 && $details->discount !=null) {
								$currentPrice = number_format($details->outPrice,0);
								$percent = (number_format($details->outPrice,0) * $details->discount) / 100;
								$oldPrice = $currentPrice + $percent;
								?>
							<del class="product-old-price">Ksh. <?php echo $oldPrice; ?></del>
						<?php } ?>
					</h3>
						<span class="product-available"><?php echo $details->status; ?></span>
					</div>

					<p><?php echo $details->description; ?></p>

					<!-- <?php //if(count($shopproductsizes) > 0 || count($shopproductcolors) > 0) { ?>
					<div class="product-options">
						<?php //if(count($shopproductsizes) > 0) { ?>
						<label>
							Size
							<select class="input-select" id="shopproductsizeId">
								<option value="0">Choose</option>
								<?php //foreach ($shopproductsizes as $keysize) { ?>
								<option value="<?php //echo $keysize->sizeId; ?>"><?php //echo $keysize->sizeName; ?></option>
								<?php //} ?>
							</select>
						</label>
					<?php //} if(count($shopproductcolors) > 0) { ?>
						<label>
							Color
							<select class="input-select" id="shopproductcolorId">
								<option value="0">Choose</option>
								<?php //foreach ($shopproductcolors as $keycolor) { ?>
								<option value="<?php //echo $keycolor->colorId; ?>"><?php //echo $keycolor->colorName; ?></option>
								<?php //} ?>
							</select>
						</label>
						<?php //} ?>
					</div>
				<?php //} ?> -->



					<?php if(count($shopproductsizes) > 0) { ?>
					<ul class="product-links">
						<li>Sizes:</li>
						<?php foreach ($shopproductsizes as $keysize) { ?>
							<li><a href="#"><?php echo $keysize->sizeName; ?></a></li>,
						<?php } ?>
					</ul>
				<?php } ?>
				<?php if(count($shopproductcolors) > 0) { ?>
					<ul class="product-links">
						<li>Colors:</li>
						<?php foreach ($shopproductcolors as $keycolor) { ?>
							<li><a href="#"><?php echo $keycolor->colorName; ?></a></li>,
						<?php } ?>
					</ul>
				<?php } ?>

					<ul class="product-links">
						<li>Brand:</li>
						<li><a href="#"><?php echo $details->brandName; ?></a></li>
					</ul>

					<ul class="product-links">
						<li>Category:</li>
						<li><a href="#"><?php echo $details->categoryName; ?></a></li>
					</ul>

					<ul class="product-links">
						<li>Share:</li>
						<li><i onclick="facebook('{{url()->current()}}','<?php echo $details->slug; ?>')" class="fa fa-facebook"></i></li>
						<li><i onclick="twitter('{{url()->current()}}','<?php echo $details->slug; ?>')" class="fa fa-twitter"></i></li>
					</ul>

					<div class="add-to-cart" style="margin-top:4%;">
						<!-- <div class="qty-label">
							Qty
							<div class="input-number">
								<input type="number" id="productQuantity">
								<span class="qty-up">+</span>
								<span class="qty-down">-</span>
							</div>
						</div> -->
						<button class="add-to-cart-btn my-cart-btn" id="addtocartitembutton" data-id="<?php echo $details->id; ?>" data-size="0" data-color="0" data-name="<?php echo $details->productName; ?>" data-summary="<?php echo $details->categoryName; ?>" data-price="<?php echo number_format($details->outPrice,0); ?>" data-quantity="1" data-image="{{ URL::to('/') }}/public/photos/<?php echo $randomphotoUrl; ?>">
							<i class="fa fa-shopping-cart"></i> add to cart</button>
					</div>

				</div>
			</div>
			<!-- /Product details -->

			<!-- Product tab -->
			<div class="col-md-12">
				<div id="product-tab">
					<!-- product tab nav -->
					<ul class="tab-nav">
						<li class="active"><a data-toggle="tab" href="#tab1">Description</a></li>
						<li><a data-toggle="tab" href="#tab3">Reviews (<?php echo count($rates); ?>)</a></li>
					</ul>
					<!-- /product tab nav -->

					<!-- product tab content -->
					<div class="tab-content">
						<!-- tab1  -->
						<div id="tab1" class="tab-pane fade in active">
							<div class="row">
								<div class="col-md-12">
									<p><?php echo $details->description; ?></p>
								</div>
							</div>
						</div>
						<!-- /tab1  -->

						<!-- tab2  -->
						<div id="tab3" class="tab-pane fade in">
							<div class="row">
								<!-- Rating -->
								<div class="col-md-3">
									<div id="rating">
										<div class="rating-avg">
											<span><?php echo $rate; ?></span>
											<div class="rating-stars">
												<?php if($rate==0) { ?>
																							<i class="fa fa-star-o"></i>
																							<i class="fa fa-star-o"></i>
																							<i class="fa fa-star-o"></i>
																							<i class="fa fa-star-o"></i>
																							<i class="fa fa-star-o"></i>
																						<?php } else if($rate==1) { ?>
																							<i class="fa fa-star"></i>
																							<i class="fa fa-star-o"></i>
																							<i class="fa fa-star-o"></i>
																							<i class="fa fa-star-o"></i>
																							<i class="fa fa-star-o"></i>
																						<?php } else if($rate==2) { ?>
																							<i class="fa fa-star"></i>
																							<i class="fa fa-star"></i>
																							<i class="fa fa-star-o"></i>
																							<i class="fa fa-star-o"></i>
																							<i class="fa fa-star-o"></i>
																						<?php } else if($rate==3) { ?>
																							<i class="fa fa-star"></i>
																							<i class="fa fa-star"></i>
																							<i class="fa fa-star"></i>
																							<i class="fa fa-star-o"></i>
																							<i class="fa fa-star-o"></i>
																						<?php } else if($rate==4) { ?>
																							<i class="fa fa-star"></i>
																							<i class="fa fa-star"></i>
																							<i class="fa fa-star"></i>
																							<i class="fa fa-star"></i>
																							<i class="fa fa-star-o"></i>
																						<?php } else if($rate==5) { ?>
																							<i class="fa fa-star"></i>
																							<i class="fa fa-star"></i>
																							<i class="fa fa-star"></i>
																							<i class="fa fa-star"></i>
																							<i class="fa fa-star"></i>
																						<?php } ?>
											</div>
										</div>
										<ul class="rating">
											<?php foreach ($limitedrates as $keyrate) {
												$percentrate = ($keyrate->rate * 100) / 5;
												?>
											<li>
												<div class="rating-stars">
													<?php if($keyrate->rate==0) { ?>
																								<i class="fa fa-star-o"></i>
																								<i class="fa fa-star-o"></i>
																								<i class="fa fa-star-o"></i>
																								<i class="fa fa-star-o"></i>
																								<i class="fa fa-star-o"></i>
																							<?php } else if($keyrate->rate==1) { ?>
																								<i class="fa fa-star"></i>
																								<i class="fa fa-star-o"></i>
																								<i class="fa fa-star-o"></i>
																								<i class="fa fa-star-o"></i>
																								<i class="fa fa-star-o"></i>
																							<?php } else if($keyrate->rate==2) { ?>
																								<i class="fa fa-star"></i>
																								<i class="fa fa-star"></i>
																								<i class="fa fa-star-o"></i>
																								<i class="fa fa-star-o"></i>
																								<i class="fa fa-star-o"></i>
																							<?php } else if($keyrate->rate==3) { ?>
																								<i class="fa fa-star"></i>
																								<i class="fa fa-star"></i>
																								<i class="fa fa-star"></i>
																								<i class="fa fa-star-o"></i>
																								<i class="fa fa-star-o"></i>
																							<?php } else if($keyrate->rate==4) { ?>
																								<i class="fa fa-star"></i>
																								<i class="fa fa-star"></i>
																								<i class="fa fa-star"></i>
																								<i class="fa fa-star"></i>
																								<i class="fa fa-star-o"></i>
																							<?php } else if($keyrate->rate==5) { ?>
																								<i class="fa fa-star"></i>
																								<i class="fa fa-star"></i>
																								<i class="fa fa-star"></i>
																								<i class="fa fa-star"></i>
																								<i class="fa fa-star"></i>
																							<?php } ?>
												</div>
												<div class="rating-progress">
													<div style="width: <?php echo $percentrate; ?>%;"></div>
												</div>
												<span class="sum"><?php echo $keyrate->rate; ?></span>
											</li>
										<?php } ?>

										</ul>
									</div>
								</div>
								<!-- /Rating -->

								<!-- Reviews -->
								<div class="col-md-6">
									<div id="reviews">
										<ul class="reviews">
											<?php foreach ($limitedrates as $keyrates) { ?>
											<li>
												<div class="review-heading">
													<h5 class="name"><?php echo $keyrates->name; ?></h5>
													<p class="date"><?php echo date("d M Y, h:i A", strtotime($keyrates->created_at)); ?></p>
													<div class="review-rating">
														<?php if($keyrates->rate==0) { ?>
																									<i class="fa fa-star-o empty"></i>
																									<i class="fa fa-star-o empty"></i>
																									<i class="fa fa-star-o empty"></i>
																									<i class="fa fa-star-o empty"></i>
																									<i class="fa fa-star-o empty"></i>
																								<?php } else if($keyrates->rate==1) { ?>
																									<i class="fa fa-star"></i>
																									<i class="fa fa-star-o empty"></i>
																									<i class="fa fa-star-o empty"></i>
																									<i class="fa fa-star-o empty"></i>
																									<i class="fa fa-star-o empty"></i>
																								<?php } else if($keyrates->rate==2) { ?>
																									<i class="fa fa-star"></i>
																									<i class="fa fa-star"></i>
																									<i class="fa fa-star-o empty"></i>
																									<i class="fa fa-star-o empty"></i>
																									<i class="fa fa-star-o empty"></i>
																								<?php } else if($keyrates->rate==3) { ?>
																									<i class="fa fa-star"></i>
																									<i class="fa fa-star"></i>
																									<i class="fa fa-star"></i>
																									<i class="fa fa-star-o empty"></i>
																									<i class="fa fa-star-o empty"></i>
																								<?php } else if($keyrates->rate==4) { ?>
																									<i class="fa fa-star"></i>
																									<i class="fa fa-star"></i>
																									<i class="fa fa-star"></i>
																									<i class="fa fa-star"></i>
																									<i class="fa fa-star-o empty"></i>
																								<?php } else if($keyrates->rate==5) { ?>
																									<i class="fa fa-star"></i>
																									<i class="fa fa-star"></i>
																									<i class="fa fa-star"></i>
																									<i class="fa fa-star"></i>
																									<i class="fa fa-star"></i>
																								<?php } ?>
													</div>
												</div>
												<div class="review-body">
													<p><?php echo $keyrates->review; ?></p>
												</div>
											</li>
										<?php } ?>
										</ul>

									</div>
								</div>
								<!-- /Reviews -->

								<!-- Review Form -->
								<div class="col-md-3">
									<div id="review-form">
										 {!! Form::open(['url'=>'submitreview','class'=>'review-form']) !!}
                     {{ csrf_field() }}
										 <p class="stext-102 cl6">
											Note: Your email address will not be published.
										</p>
										<input class="input" type="hidden" name="shopProductId" value="<?php echo $details->id; ?>" required>
											<input class="input" type="text" name="name" value="{{ old('name') }}" placeholder="Your Name" required>
											<input class="input" type="email" name="email" value="{{ old('email') }}" placeholder="Your Email" required>
											<textarea class="input" placeholder="Your Review" name="review" required>{{ old('review') }}</textarea>
											<div class="input-rating">
												<span>Your Rating: </span>
												<div class="stars">
													<input id="star5" name="rate" value="5" type="radio"><label for="star5"></label>
													<input id="star4" name="rate" value="4" type="radio"><label for="star4"></label>
													<input id="star3" name="rate" value="3" type="radio"><label for="star3"></label>
													<input id="star2" name="rate" value="2" type="radio"><label for="star2"></label>
													<input id="star1" name="rate" value="1" type="radio"><label for="star1"></label>
												</div>
											</div>
											<button class="primary-btn" id="rateProduct" type="submit">Submit</button>
										</form>
									</div>
								</div>
								<!-- /Review Form -->
							</div>
						</div>
						<!-- /tab3  -->
					</div>
					<!-- /product tab content  -->
				</div>
			</div>
			<!-- /product tab -->
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /SECTION -->

<!-- Section -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">

			<div class="col-md-12">
				<div class="section-title text-center">
					<h3 class="title">Related Products</h3>
				</div>
			</div>

			<?php $relatedProducts = \App\Shopproducts::getRelatedProducts($details->categoryId);
			foreach ($relatedProducts as $keyrep) {
				$raterep = \App\Productratings::getRating($keyrep->id);
				$photoUrl = \App\Photos::getRandom($keyrep->productId);
				?>
			<!-- product -->
			<div class="col-md-3 col-xs-6">
				<a href="<?php $url = URL::to("/product/".$keyrep->slug); print_r($url); ?>">
				<div class="product">
					<div class="product-img">
						<img src="{{ URL::to('/') }}/public/photos/<?php echo $photoUrl; ?>" alt="">
						<div class="product-label">
							<?php if($keyrep->discount !=0 && $keyrep->discount !=null) { ?>
								<span class="sale">-<?php echo $keyrep->discount; ?>%</span>
							<?php } ?>
							<span class="new"><?php echo $keyrep->productCondition; ?></span>
						</div>
					</div>
					<div class="product-body">
						<p class="product-category"><?php echo $keyrep->categoryName; ?></p>
						<h3 class="product-name"><a href="<?php $url = URL::to("/product/".$keyrep->slug); print_r($url); ?>">
							<?php echo $keyrep->productName; ?>
						</a></h3>
						<h4 class="product-price">Ksh. <?php echo number_format($keyrep->outPrice,0); ?>
							<?php if($keyrep->discount !=0 && $keyrep->discount !=null) {
								$currentPrice = $keyrep->outPrice;
								$percent = ($keyrep->outPrice * $keyrep->discount) / 100;
								$oldPrice = $currentPrice + $percent;
								?>
							<del class="product-old-price">Ksh. <?php echo number_format($oldPrice,0); ?></del>
						<?php } ?>
						</h4>
						<div class="product-rating">
							<?php if($raterep==0) { ?>
																		<i class="fa fa-star whitecolor"></i>
																		<i class="fa fa-star whitecolor"></i>
																		<i class="fa fa-star whitecolor"></i>
																		<i class="fa fa-star whitecolor"></i>
																		<i class="fa fa-star whitecolor"></i>
																	<?php } else if($raterep==1) { ?>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star whitecolor"></i>
																		<i class="fa fa-star whitecolor"></i>
																		<i class="fa fa-star whitecolor"></i>
																		<i class="fa fa-star whitecolor"></i>
																	<?php } else if($raterep==2) { ?>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star whitecolor"></i>
																		<i class="fa fa-star whitecolor"></i>
																		<i class="fa fa-star whitecolor"></i>
																	<?php } else if($raterep==3) { ?>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star whitecolor"></i>
																		<i class="fa fa-star whitecolor"></i>
																	<?php } else if($raterep==4) { ?>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star whitecolor"></i>
																	<?php } else if($raterep==5) { ?>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																		<i class="fa fa-star"></i>
																	<?php } ?>
						</div>
						<div class="product-btns">
							<button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">Like</span></button>
							<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">compare</span></button>
							<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
						</div>
					</div>
					<div class="add-to-cart">
						<button class="add-to-cart-btn my-cart-btn" data-id="<?php echo $keyrep->id; ?>" data-name="<?php echo $keyrep->productName; ?>" data-summary="<?php echo $keyrep->categoryName; ?>" data-price="<?php echo $keyrep->outPrice; ?>" data-quantity="1" data-image="{{ URL::to('/') }}/public/photos/<?php echo $photoUrl; ?>">
							<i class="fa fa-shopping-cart"></i> add to cart</button>
					</div>
				</div>
			</a>
			</div>
			<!-- /product -->
		<?php } ?>

		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /Section -->

		<!-- NEWSLETTER -->
    @include('site.newsletter')
		<!-- /NEWSLETTER -->

		<!-- FOOTER -->
		@include('site.footer')
		<!-- /FOOTER -->

    @include('site.footerlinks')

	</body>
</html>
