<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Terms of Use</title>
    @include('site.headerlinks')
    </head>
	<body>
		<!-- HEADER -->
    @include('site.header')
		<!-- /HEADER -->

		<!-- NAVIGATION -->
    @include('site.nav')
		<!-- /NAVIGATION -->

		<?php
		$settings = \App\Settings::getDetails();
		?>

		<!-- BREADCRUMB -->
<div id="breadcrumb" class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<h3 class="breadcrumb-header">Terms of Use</h3>
				<ul class="breadcrumb-tree">
					<li><a href="{{URL::to('/')}}"><?php echo $settings->companyName; ?></a></li>
				</ul>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /BREADCRUMB -->

<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">

			<p>
							 Welcome to our website. If you continue to browse and use this website you are agreeing to comply
							 with and be bound by the following terms
							 and conditions of use, which together with our privacy policy govern <?php echo $settings->companyName; ?>’s relationship with you in relation to this website.</br>
							 The term “<?php echo $settings->companyName; ?>” or “us” or “we” refers to the owner of the website who is <?php echo $settings->companyName; ?>. The term “you” refers to
							 the user or viewer of our website.</br></br>
							 The use of this website is subject to the following terms of use:</br></br>
							 1. The content of the pages of this website is for your general information and use only. It is subject to change without notice. </br></br>
							 2. Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness
							 or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that
							 such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or
							 errors to the fullest extent permitted by law.</br></br>
							 3. Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable.
							 It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</br></br>
							 4. This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look,
							 appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.</br></br>
							 5. All trademarks reproduced in this website, which are not the property of, or licensed to the operator, are acknowledged on the website.</br></br>
							 6. Unauthorised use of this website may give to a claim for damages and/or be a criminal offence.</br></br>
							 7. From time to time this website may also include links to other websites. These links are provided for your convenience to provide further information.
							 They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).</br></br>
							 8. You may not create a link to this website from another website or document without <?php echo $settings->companyName; ?>’s prior written consent. </br></br>
							 9. Your use of this website and any dispute arising out of such use of the website is subject to the laws of Kenya & the international laws governing such.
						</p>

		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /SECTION -->

		<!-- NEWSLETTER -->
    @include('site.newsletter')
		<!-- /NEWSLETTER -->

		<!-- FOOTER -->
		@include('site.footer')
		<!-- /FOOTER -->

    @include('site.footerlinks')

	</body>
</html>
