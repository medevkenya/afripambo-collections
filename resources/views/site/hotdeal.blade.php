<?php
$promotion = \App\Promotions::getPromotion();
if($promotion) {
  $fromDate = $promotion->fromDate;
  $toDate = $promotion->toDate;
  $today = date('Y-m-d');
  $diff = strtotime($toDate) - strtotime($today);
  if($diff > 0) {
?>
<div id="hot-deal" class="section">
  <!-- container -->
  <div class="container">
    <!-- row -->
    <div class="row">
      <div class="col-md-12">
        <div class="hot-deal">
          <ul class="hot-deal-countdown">
            <li>
              <div>
                <h3 id="countdownDays">0</h3>
                <span>Days</span>
              </div>
            </li>
            <li>
              <div>
                <h3 id="countdownHours">0</h3>
                <span>Hours</span>
              </div>
            </li>
            <li>
              <div>
                <h3 id="countdownMinutes">0</h3>
                <span>Mins</span>
              </div>
            </li>
            <li>
              <div>
                <h3 id="countdownSeconds">0</h3>
                <span>Secs</span>
              </div>
            </li>
          </ul>
          <h2 class="text-uppercase"><?php echo $promotion->label1; ?></h2>
          <p><?php echo $promotion->label2; ?></p>
          <a class="primary-btn cta-btn" href="{{URL::to('/hotdeals')}}"><?php echo $promotion->buttonLabel; ?></a>
        </div>
      </div>
    </div>
    <!-- /row -->
  </div>
  <!-- /container -->
</div>
<script>
var upgradeTime = '<?php echo $diff; ?>';
var seconds = upgradeTime;
function timer() {
  var days        = Math.floor(seconds/24/60/60);
  var hoursLeft   = Math.floor((seconds) - (days*86400));
  var hours       = Math.floor(hoursLeft/3600);
  var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
  var minutes     = Math.floor(minutesLeft/60);
  var remainingSeconds = seconds % 60;
  function pad(n) {
    return (n < 10 ? "0" + n : n);
  }
  document.getElementById('countdownDays').innerHTML = pad(days);
  document.getElementById('countdownHours').innerHTML = pad(hours);
  document.getElementById('countdownMinutes').innerHTML = pad(minutes);
  document.getElementById('countdownSeconds').innerHTML = pad(remainingSeconds);
  if (seconds == 0) {
    clearInterval(countdownTimer);
    document.getElementById('hot-deal').style.display = "none";
  } else {
    seconds--;
  }
}
var countdownTimer = setInterval('timer()', 1000);
</script>
<?php
}
}
?>
