<?php foreach ($homeproducts as $keyvalue) {
$rate = \App\Productratings::getRating($keyvalue->id);
$photoUrl = \App\Photos::getRandom($keyvalue->productId);
	?>
<!-- product -->
<div class="col-md-4 col-xs-6">
	<a href="<?php $url = URL::to("/product/".$keyvalue->slug); print_r($url); ?>">
	<div class="product">
		<div class="product-img">
			<img src="{{ URL::to('/') }}/public/photos/<?php echo $photoUrl; ?>" alt="">
			<div class="product-label">
				<?php if($keyvalue->discount !=0 && $keyvalue->discount !=null) { ?>
					<span class="sale">-<?php echo $keyvalue->discount; ?>%</span>
				<?php } ?>
				<span class="new"><?php echo $keyvalue->productCondition; ?></span>
			</div>
		</div>
		<div class="product-body">
			<p class="product-category"><?php echo $keyvalue->categoryName; ?></p>
			<h3 class="product-name"><a href="<?php $url = URL::to("/product/".$keyvalue->slug); print_r($url); ?>">
				<?php echo $keyvalue->productName; ?>
			</a></h3>
			<h4 class="product-price">Ksh. <?php echo number_format($keyvalue->outPrice,0); ?>
				<?php if($keyvalue->discount !=0 && $keyvalue->discount !=null) {
					$currentPrice = number_format($keyvalue->outPrice,0);
					$percent = (number_format($keyvalue->outPrice,0) * $keyvalue->discount) / 100;
					$oldPrice = $currentPrice + $percent;
					?>
				<del class="product-old-price">Ksh. <?php echo number_format($oldPrice,0); ?></del>
			<?php } ?>
			</h4>
			<div class="product-rating">
				<?php if($rate==0) { ?>
															<i class="fa fa-star whitecolor"></i>
															<i class="fa fa-star whitecolor"></i>
															<i class="fa fa-star whitecolor"></i>
															<i class="fa fa-star whitecolor"></i>
															<i class="fa fa-star whitecolor"></i>
														<?php } else if($rate==1) { ?>
															<i class="fa fa-star"></i>
															<i class="fa fa-star whitecolor"></i>
															<i class="fa fa-star whitecolor"></i>
															<i class="fa fa-star whitecolor"></i>
															<i class="fa fa-star whitecolor"></i>
														<?php } else if($rate==2) { ?>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star whitecolor"></i>
															<i class="fa fa-star whitecolor"></i>
															<i class="fa fa-star whitecolor"></i>
														<?php } else if($rate==3) { ?>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star whitecolor"></i>
															<i class="fa fa-star whitecolor"></i>
														<?php } else if($rate==4) { ?>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star whitecolor"></i>
														<?php } else if($rate==5) { ?>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
															<i class="fa fa-star"></i>
														<?php } ?>
			</div>
			<div class="product-btns">
				<button class="add-to-wishlist"><i class="fa fa-heart-o"></i><span class="tooltipp">Like</span></button>
				<button class="add-to-compare"><i class="fa fa-exchange"></i><span class="tooltipp">compare</span></button>
				<button class="quick-view"><i class="fa fa-eye"></i><span class="tooltipp">quick view</span></button>
			</div>
		</div>
		<div class="add-to-cart">
			<button class="add-to-cart-btn my-cart-btn" data-id="<?php echo $keyvalue->id; ?>" data-name="<?php echo $keyvalue->productName; ?>" data-summary="<?php echo $keyvalue->categoryName; ?>" data-price="<?php echo number_format($keyvalue->outPrice,0); ?>" data-quantity="1" data-image="{{ URL::to('/') }}/public/photos/<?php echo $photoUrl; ?>"><i class="fa fa-shopping-cart"></i> add to cart</button>
		</div>
	</div>
</a>
</div>
<!-- /product -->
<?php }	?>
