<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Checkout</title>
    @include('site.headerlinks')
		<script src="https://checkout.flutterwave.com/v3.js"></script>
    </head>
	<body>
		<!-- HEADER -->
    @include('site.header')
		<!-- /HEADER -->

		<!-- NAVIGATION -->
    @include('site.nav')
		<!-- /NAVIGATION -->

		<!-- BREADCRUMB -->
<div id="breadcrumb" class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">
			<div class="col-md-12">
				<ul class="breadcrumb-tree">
					<li><a href="{{URL::to('/')}}">Home</a></li>
					<li class="active">Checkout (#<?php echo $orderdetails->orderNo; ?>)</li>
				</ul>
			</div>
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /BREADCRUMB -->

<!-- SECTION -->
<div class="section">
	<!-- container -->
	<div class="container">
		<!-- row -->
		<div class="row">

			<div class="col-md-7">
				<!-- Billing Details -->
				<div class="billing-details">
					<div class="section-title">
						<h3 class="title">Billing address</h3>
					</div>
					<div class="form-group">
						<input class="input" type="text" name="firstName" id="firstName" placeholder="First Name" required>
					</div>
					<div class="form-group">
						<input class="input" type="text" name="lastName" id="lastName" placeholder="Last Name" required>
					</div>
					<div class="form-group">
						<input class="input" type="email" name="email" id="email" placeholder="Email" required>
					</div>
					<div class="form-group">
						<input class="input" type="text" name="address" id="address" placeholder="Delivery Location" required>
						<input type="hidden" name="latitude" id="latitude">
            <input type="hidden" name="longitude" id="longitude">
					</div>
					<!-- <div class="form-group">
						<input class="input" type="text" name="city" placeholder="City">
					</div>
					<div class="form-group">
						<input class="input" type="text" name="country" placeholder="Country">
					</div>
					<div class="form-group">
						<input class="input" type="text" name="zip-code" placeholder="ZIP Code">
					</div> -->
					<div class="form-group">
						<input class="input" type="tel" name="mobileNo" id="mobileNo" placeholder="Telephone / Mobile Number">
					</div>
					<div class="form-group">
						<div class="input-checkbox">
							<input type="checkbox" id="create-account">
							<label for="create-account">
								<span></span>
								Create Account?
							</label>
							<div class="caption">
								<p>Please enter your password.</p>
								<input class="input" type="password" name="password" id="password" placeholder="Enter Your Password">
							</div>
						</div>
					</div>
				</div>
				<!-- /Billing Details -->

				<!-- Shiping Details -->
				<!-- <div class="shiping-details">
					<div class="section-title">
						<h3 class="title">Shiping address</h3>
					</div>
					<div class="input-checkbox">
						<input type="checkbox" id="shiping-address">
						<label for="shiping-address">
							<span></span>
							Ship to a diffrent address?
						</label>
						<div class="caption">
							<div class="form-group">
								<input class="input" type="text" name="first-name" placeholder="First Name">
							</div>
							<div class="form-group">
								<input class="input" type="text" name="last-name" placeholder="Last Name">
							</div>
							<div class="form-group">
								<input class="input" type="email" name="email" placeholder="Email">
							</div>
							<div class="form-group">
								<input class="input" type="text" name="address" placeholder="Address">
							</div>
							<div class="form-group">
								<input class="input" type="tel" name="tel" placeholder="Telephone">
							</div>
						</div>
					</div>
				</div> -->
				<!-- /Shiping Details -->

				<!-- Order notes -->
				<div class="order-notes">
					<textarea class="input" name="notes" id="notes" placeholder="Order Notes"></textarea>
				</div>
				<!-- /Order notes -->
			</div>

			<!-- Order Details -->
			<div class="col-md-5 order-details">
				<div class="section-title text-center">
					<h3 class="title">Your Order #<?php echo $orderdetails->orderNo; ?></h3>
				</div>
				<div class="order-summary">
					<div class="order-col">
						<div><strong>PRODUCT</strong></div>
						<div><strong>TOTAL</strong></div>
					</div>
					<div class="order-products">
						<?php foreach ($cartlist as $keycart) { ?>
						<div class="order-col">
							<div><?php echo $keycart->quantity; ?>x <?php echo $keycart->itemName; ?></div>
							<div>Ksh. <?php echo $keycart->cost; ?></div>
						</div>
					<?php } ?>
					</div>
					<div class="order-col">
						<div>Delivery Fee</div>
						<div><strong>Ksh. <span id="STANDARD_SHIPPING_COST"><?php echo $standardShippingCost; ?></span></strong></div>
					</div>
					<div class="order-col">
						<div><strong>TOTAL</strong></div>
						<div><strong class="order-total">Ksh. <b id="total_cost"><?php echo $total; ?></b></strong></div>
					</div>
				</div>

				<div class="payment-method">

					@if (count($errors) > 0)
             <div class="alert alert-danger">
                 <ul>
                     @foreach ($errors->all() as $error)
                     <li>{{ $error }}</li>
                     @endforeach
                 </ul>
             </div>
            @endif

            @if ($message = Session::get('error'))
                 <div class="alert alert-danger">
                     {{ $message }}
                 </div>
            @endif

            @if ($message = Session::get('success'))
                 <div class="alert alert-success">
                     {{ $message }}
                 </div>
            @endif

            @if (session('status0'))
            <div class="alert alert-danger alert-dismissible alertbox" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ session('status0') }}
            </div>
            @endif

            @if (session('status1'))
            <div class="alert alert-success alert-dismissible alertbox" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ session('status1') }}
            </div>
            @endif

						<div class="alert alert-success alert-dismissible" id="successfulFlutterPayment" style="display:none;margin-top:4%;">
             <p>Success! You will get a confirmation email once its confirmed.</p>
            </div>

            <div class="alert alert-danger" role="alert" id="failedFlutterPayment" style="display:none;margin-top:4%;">
             <p>Failed! Your payment was not successful. Please check your details and try again</p>
            </div>

					<div id="writeinfo-checkout" class="alert alert-success" style="display:none;"></div>
					<div class="ajax-loader-checkout">
						<img src="{{ URL::to('/') }}/public/images/loading.gif" class="img-responsive" />
					</div>

					<div class="input-radio">
						<input type="radio" name="payment" id="payment-1">
						<label for="payment-1">
							<span></span>
							Lipa Na M-PESA <img src="{{ URL::asset('images/mpesa.jpg')}}" alt="M-PESA" class="mpesa-payment">
						</label>
						<div class="caption">

							<p>Enter your M-PESA number and click on the <strong>Pay Now</strong> button to proceed. We will send an M-PESA payment prompt to this number.</p>
							<div class="input-checkbox">
								<input type="checkbox" name="terms" id="mpesaterms">
								<label for="terms">
									<span></span>
									I've read and accept the <a href="{{URL::to('/terms')}}">terms & conditions</a>
								</label>
							</div>
							<div class="container-pay" style="width: 100%;">
							  <input type="tel" class="text_input-pay" id="mpesa-mobileNo" placeholder="0712XXXXXX" required/>
							  <button value="Save" class="btn-pay" id="paynow-mpesa">Pay Now <i class="fa fa-arrow-circle-right"></i></button>
							</div>

						</div>
					</div>
					<div class="input-radio">
						<input type="radio" name="payment" id="payment-2">
						<label for="payment-2">
							<span></span>
							Credit / Debit Card Payment <img src="{{ URL::asset('images/creditcards.png')}}" alt="Credit Cards" class="creditcards-payment">
						</label>
						<div class="caption">
							<p>Complete your purchase with Credit Card payment. Click on the <strong>Pay Now</strong> button to proceed.</p>
							<div class="input-checkbox">
								<input type="checkbox" name="terms" id="creditcardsterms">
								<label for="terms">
									<span></span>
									I've read and accept the <a href="{{URL::to('/terms')}}">terms & conditions</a>
								</label>
							</div>
							<button class="primary-btn order-submit" id="paynow-creditcards" onclick="initiatePayment()">Pay Now</button>
						</div>
					</div>
					<!-- <div class="input-radio">
						<input type="radio" name="payment" id="payment-3">
						<label for="payment-3">
							<span></span>
							Paypal System <img src="{{ URL::asset('images/paypal.png')}}" alt="Paypal" class="paypal-payment">
						</label>
						<div class="caption">
							<p>Complete your purchase with Paypal payment. Click on the <strong>Pay Now</strong> button to proceed.</p>
							<div class="input-checkbox">
								<input type="checkbox" name="terms" id="paypalterms">
								<label for="terms">
									<span></span>
									I've read and accept the <a href="{{URL::to('/terms')}}">terms & conditions</a>
								</label>
							</div>
							<button class="primary-btn order-submit" id="paynow-paypal">Pay Now</button>
						</div>
					</div> -->
					<div class="input-radio">
						<input type="radio" name="payment" id="payment-4">
						<label for="payment-4">
							<span></span>
							Cash on Delivery <img src="{{ URL::asset('images/cash.jpg')}}" alt="Cash" class="cash-payment">
						</label>
						<div class="caption">
							<p>The delivery agent will receive cash from you once you received your ptoduct(s). Click on the <strong>Place Order</strong> button to proceed. <a href="{{URL::to('/terms')}}">Terms & Conditions</a> apply</p>
							<div class="input-checkbox">
								<input type="checkbox" name="terms" id="cashterms" checked>
								<label for="terms">
									<span></span>
									I've read and accept the <a href="{{URL::to('/terms')}}">terms & conditions</a>
								</label>
							</div>
							<button class="primary-btn order-submit" id="paynow-cash">Place Order</button>
						</div>
					</div>
				</div>


			</div>
			<!-- /Order Details -->
		</div>
		<!-- /row -->
	</div>
	<!-- /container -->
</div>
<!-- /SECTION -->

<!-- Modal -->
<div class="modal fade" id="validationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header bg-warning">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Missing Information</h4>
      </div>
      <div class="modal-body">
        <p id="validationErrorMessage"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

		<!-- NEWSLETTER -->
    @include('site.newsletter')
		<!-- /NEWSLETTER -->

		<!-- FOOTER -->
		@include('site.footer')
		<!-- /FOOTER -->

    @include('site.footerlinks')

		<script type="text/javascript">
		$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
		</script>
		<script>
		        $(document).ready(function(){
		            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		            $("#paynow-mpesa").click(function(){

		              var mpesamobileNo = $("#mpesa-mobileNo").val();
									var mobileNo = $("#mobileNo").val();
									var orderId = '<?php echo $orderdetails->id; ?>';
									var orderNo = '<?php echo $orderdetails->orderNo; ?>';
									var total = $('#total_cost').text();
		              //console.log("subscribe clicked---"+email+"---");

									var email = $("#email").val();
									var firstName = $("#firstName").val();
									var lastName = $("#lastName").val();
									var address = $("#address").val();

									var atpos = email.indexOf("@");
									var dotpos = email.lastIndexOf(".");

									if (document.getElementById('create-account').checked)
									{
										 var password = $("#password").val();
										if($("#password").val().length < 6)
										{
											document.getElementById("validationErrorMessage").innerHTML = "Password must be at least 6 characters long";
											$('#validationModal').modal('show');
											return false;
										}

					        }
									else
									{
					            var password = "";
					        }

									var mobileNumber=parseInt(mobileNo);

									if($("#mpesa-mobileNo").val().length < 10){// || mobileNumber.toString().length!=10
										document.getElementById("validationErrorMessage").innerHTML = "Please enter M-PESA Mobile number";
										$('#validationModal').modal('show');
									}
									else if($("#mobileNo").val().length < 10){// || mobileNumber.toString().length!=10
									document.getElementById("validationErrorMessage").innerHTML = "Please provide your Telephone or Mobile number";
									$('#validationModal').modal('show');
									}
									else if (atpos < 1 || ( dotpos - atpos < 2 ))
									{
										document.getElementById("validationErrorMessage").innerHTML = "Please provide a valid email address";
										$('#validationModal').modal('show');
									}
									else if($("#firstName").val().length < 3)
									{
										document.getElementById("validationErrorMessage").innerHTML = "Please provide a valid first name";
										$('#validationModal').modal('show');
									}
									else if($("#lastName").val().length < 3)
									{
										document.getElementById("validationErrorMessage").innerHTML = "Please provide a valid last name";
										$('#validationModal').modal('show');
									}
									else if($("#address").val().length < 3)
									{
										document.getElementById("validationErrorMessage").innerHTML = "Please provide a valid delivery location address";
										$('#validationModal').modal('show');
									}
									else
									{

		              $('.ajax-loader').css("visibility", "visible");

									var latitude = $("#latitude").val();
									var longitude = $("#longitude").val();
									var notes = $("#notes").val();

									var mpesamobileNo = mpesamobileNo.replace(/<\/?[^>]+(>|$)/g, "");
									var mobileNo = mobileNo.replace(/<\/?[^>]+(>|$)/g, "");
									var email = email.replace(/<\/?[^>]+(>|$)/g, "");
									var notes = notes.replace(/<\/?[^>]+(>|$)/g, "");
									var firstName = firstName.replace(/<\/?[^>]+(>|$)/g, "");
									var lastName = lastName.replace(/<\/?[^>]+(>|$)/g, "");
									var address = address.replace(/<\/?[^>]+(>|$)/g, "");
									var password = password.replace(/<\/?[^>]+(>|$)/g, "");

		              $.ajax({
		                  url: "{{ route('requestMpesaStk') }}?mpesamobileNo="+mpesamobileNo+"&mobileNo=" + mobileNo + "&orderId="+orderId+"&orderNo="+orderNo+"&total="+total+'&mobileNo='+mobileNo+'&email='+email+'&firstName='+firstName+'&lastName='+lastName+'&address='+address+'&latitude='+latitude+'&longitude='+longitude+'&password='+password+"&notes="+notes,
		                  method: 'GET',
		                  success: function(data) {
		                      //console.log("js html--"+data.message);
		                      $('.ajax-loader').css("visibility", "hidden");
		                      document.getElementById("writeinfo-checkout").innerHTML = "";
		                      $("#writeinfo-checkout").append(data.message);
		                      document.getElementById("writeinfo-checkout").style.display = "block";

													if(data.status==1) {
														clearFields();
														sendPaymentInvoices(orderNo);
													}

		                  },
		            			complete: function(){
		            				$('.ajax-loader').css("visibility", "hidden");
		            			}
		              });

								}

		            });

		       });
	</script>
	<script>
					$(document).ready(function(){
							var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
							$("#paynow-cash").click(function(){

								var mobileNo = $("#mpesa-mobileNo").val();
								var orderId = '<?php echo $orderdetails->id; ?>';
								var orderNo = '<?php echo $orderdetails->orderNo; ?>';
								var total = $('#total_cost').text();
								//console.log("subscribe clicked---"+email+"---");

								if (document.getElementById('create-account').checked)
								{
									 var password = $("#password").val();
									if($("#password").val().length < 6)
									{
										document.getElementById("validationErrorMessage").innerHTML = "Password must be at least 6 characters long";
										$('#validationModal').modal('show');
										return false;
									}

								}
								else
								{
										var password = "";
								}

								var email = $("#email").val();
								var firstName = $("#firstName").val();
								var lastName = $("#lastName").val();
								var address = $("#address").val();
								var password = $("#password").val();

								var atpos = email.indexOf("@");
								var dotpos = email.lastIndexOf(".");

								//var mobileNumber=parseInt(mobileNo);

								console.log("mobileNo--"+$("#mobileNo").val().length);

								if($("#mobileNo").val().length < 10){// || mobileNumber.toString().length!=10
								document.getElementById("validationErrorMessage").innerHTML = "Please provide your Telephone or Mobile number";
								$('#validationModal').modal('show');
								}
								else if (atpos < 1 || ( dotpos - atpos < 2 ))
								{
									document.getElementById("validationErrorMessage").innerHTML = "Please provide a valid email address";
									$('#validationModal').modal('show');
								}
								else if($("#firstName").val().length < 3)
								{
									document.getElementById("validationErrorMessage").innerHTML = "Please provide a valid first name";
									$('#validationModal').modal('show');
								}
								else if($("#lastName").val().length < 3)
								{
									document.getElementById("validationErrorMessage").innerHTML = "Please provide a valid last name";
									$('#validationModal').modal('show');
								}
								else if($("#address").val().length < 3)
								{
									document.getElementById("validationErrorMessage").innerHTML = "Please provide a valid delivery location address";
									$('#validationModal').modal('show');
								}
								else
								{

									var latitude = $("#latitude").val();
									var longitude = $("#longitude").val();
									var notes = $("#notes").val();

									var mobileNo = mobileNo.replace(/<\/?[^>]+(>|$)/g, "");
									var email = email.replace(/<\/?[^>]+(>|$)/g, "");
									var notes = notes.replace(/<\/?[^>]+(>|$)/g, "");
									var firstName = firstName.replace(/<\/?[^>]+(>|$)/g, "");
									var lastName = lastName.replace(/<\/?[^>]+(>|$)/g, "");
									var address = address.replace(/<\/?[^>]+(>|$)/g, "");
									var password = password.replace(/<\/?[^>]+(>|$)/g, "");

								$('.ajax-loader').css("visibility", "visible");

								$.ajax({
										url: "{{ route('cashondelivery') }}?mobileNo=" + mobileNo + "&orderId="+orderId+"&orderNo="+orderNo+"&total="+total+'&mobileNo='+mobileNo+'&email='+email+'&firstName='+firstName+'&lastName='+lastName+'&address='+address+'&latitude='+latitude+'&longitude='+longitude+'&password='+password+"&notes="+notes,
										method: 'GET',
										success: function(data) {
												//console.log("js html--"+data.message);
												$('.ajax-loader').css("visibility", "hidden");
												document.getElementById("writeinfo-checkout").innerHTML = "";
												$("#writeinfo-checkout").append(data.message);
												document.getElementById("writeinfo-checkout").style.display = "block";

												if(data.status == 1) {
													clearFields();
													sendPaymentInvoices(orderNo);
												}

										},
										complete: function(){
											$('.ajax-loader').css("visibility", "hidden");
										}
								});

							}

							});

				 });
	</script>

	<script>

  function initiatePayment()
  {

		var mobileNo = $("#mobileNo").val();
		var email = $("#email").val();
		var firstName = $("#firstName").val();
		var lastName = $("#lastName").val();
		var address = $("#address").val();
		var password = $("#password").val();

		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");

		var mobileNumber=parseInt(mobileNo);

		if($("#mobileNo").val().length < 10){// || mobileNumber.toString().length!=10
		document.getElementById("validationErrorMessage").innerHTML = "Please provide your Telephone or Mobile number";
		$('#validationModal').modal('show');
		}
		else if (atpos < 1 || ( dotpos - atpos < 2 ))
		{
			document.getElementById("validationErrorMessage").innerHTML = "Please provide a valid email address";
			$('#validationModal').modal('show');
		}
		else if($("#firstName").val().length < 3)
		{
			document.getElementById("validationErrorMessage").innerHTML = "Please provide a valid first name";
			$('#validationModal').modal('show');
		}
		else if($("#lastName").val().length < 3)
		{
			document.getElementById("validationErrorMessage").innerHTML = "Please provide a valid last name";
			$('#validationModal').modal('show');
		}
		else if($("#address").val().length < 3)
		{
			document.getElementById("validationErrorMessage").innerHTML = "Please provide a valid delivery location address";
			$('#validationModal').modal('show');
		}
		else
		{

			if (document.getElementById('create-account').checked)
			{
				 var password = $("#password").val();
				if($("#password").val().length < 6)
				{
					document.getElementById("validationErrorMessage").innerHTML = "Password must be at least 6 characters long";
					$('#validationModal').modal('show');
					return false;
				}

			}
			else
			{
					var password = "";
			}

			var latitude = $("#latitude").val();
			var longitude = $("#longitude").val();
			var notes = $("#notes").val();

		var orderId = '<?php echo $orderdetails->id; ?>';
		var orderNo = '<?php echo $orderdetails->orderNo; ?>';
		var amount = $('#total_cost').text();

			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      jQuery.ajax({
      type: 'POST',
			headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
      url:"{{ route('flutterwaveinitiate') }}",
      data: 'amount='+amount+'&orderNo='+orderNo+'&mobileNo='+mobileNo+'&email='+email+'&firstName='+firstName+'&lastName='+lastName+'&address='+address+'&latitude='+latitude+'&longitude='+longitude+'&password='+password+"&notes="+notes,
      cache: false,
      success: function(response){
        //console.log('initiatePayment---'+JSON.stringify(response));
        if(response != null) {
          var results = JSON.parse(response);
          //console.log('initiatePayment parsed---'+JSON.stringify(results));
          //console.log('initiatePayment billRefNumber---'+results.billRefNumber);
          makePayment(amount,results.billRefNumber,results.public_key);
        }
        else {

        }

      }
      });

		}

  }

  function makePayment(amount,billRefNumber,public_key)
  {

		var phone_number = $("#mobileNo").val();
		var email = $("#email").val();
		var orderId = '<?php echo $orderdetails->id; ?>';
		var orderNo = '<?php echo $orderdetails->orderNo; ?>';
		var amount = $('#total_cost').text();

    var name = $("#firstName").val()+" "+$("#lastName").val();

    var x = FlutterwaveCheckout({
      public_key: public_key,
      tx_ref: billRefNumber,//Generate a random id for the transaction reference
      amount: amount,
      currency: "KES",
      country: "KE",
      payment_options: "card, mpesa",
      customer: {
        email: email,
        phone_number: phone_number,
        name: name,
      },
      callback: function(response) {

          //console.log("This is the response returned after a charge", JSON.stringify(response));
          if (response.status == "successful")
          {
              var transaction_id = response.transaction_id; // collect txRef returned and pass to a server page to complete status check.
              var tx_ref = response.tx_ref;

              // redirect to a success page
              document.getElementById("successfulFlutterPayment").style.display = "block";
              document.getElementById("failedFlutterPayment").style.display = "none";
              //document.getElementById("cardOptionsText").style.display = "none";

							clearFields();

							$("html, body").animate({ scrollTop: 0 }, "slow");

							sendPaymentInvoices(billRefNumber);

              //verify transaction
              verifyFlutterTransaction(transaction_id,tx_ref);

          }
          else
          {
              // redirect to a failure page
              document.getElementById("failedFlutterPayment").style.display = "block";
              document.getElementById("successfulFlutterPayment").style.display = "none";
              //document.getElementById("cardOptionsText").style.display = "none";

							$("html, body").animate({ scrollTop: 0 }, "slow");
          }

          x.close(); // use this to close the modal immediately after payment.
      },
      onclose: function() {
        // close modal
      },
      customizations: {
        title: "<?php echo env("APP_NAME"); ?>",
        description: "Purchase Products",
        logo: "{{ URL::asset('site/img/logo.png')}}",
      },
    });
  }

  function verifyFlutterTransaction(transaction_id,tx_ref)
  {

			var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

      jQuery.ajax({
      type: 'POST',
			headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
      url:"{{ route('flutterwaveverify') }}",
      data: 'transaction_id='+transaction_id+'&tx_ref='+tx_ref,
      cache: false,
      success: function(response){
        console.log('verifyFlutterTransaction---'+JSON.stringify(response));
        if(response != null) {
          var results = JSON.parse(response);
          //console.log('verifyFlutterTransaction parsed---'+JSON.stringify(results));
          //console.log('verifyFlutterTransaction ResultDesc---'+results.ResultDesc);

					$("html, body").animate({ scrollTop: 0 }, "slow");

          if (results.ResultCode == 0)
          {

              // redirect to a success page
              document.getElementById("successfulFlutterPayment").style.display = "block";
              document.getElementById("successfulFlutterPayment").innerHTML = results.ResultDesc;
              document.getElementById("successfulFlutterPayment").style.borderColor = "#FAB348";
              document.getElementById("failedFlutterPayment").style.display = "none";
              //document.getElementById("cardOptionsText").style.display = "none";

							location.reload();
							return false;

          }

        }

      }
      });

  }

	function sendPaymentInvoices(orderNo)
	{

			// var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
			//
			// jQuery.ajax({
			// type: 'POST',
			// headers: {
			// 	'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			// },
			// url:"{{ route('sendPaymentInvoice') }}",
			// data: 'orderNo='+orderNo,
			// cache: false,
			// success: function(response){
			// 	console.log('sendPaymentInvoices---'+JSON.stringify(response));
			// 	if(response != null) {
			// 		var results = JSON.parse(response);
			// 		//console.log('sendPaymentInvoices parsed---'+JSON.stringify(results));
			// 	}
			// }
			// });

	}

	function calculateDeliveryFee(latitude,longitude) {
		var orderNo = '<?php echo $orderdetails->orderNo; ?>';
		console.log("calculateDeliveryFee--orderNo--"+orderNo);
		jQuery.ajax({
		type: 'POST',
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		url:"{{ route('calculateDeliveryFee') }}",
		data: 'latitude='+latitude+"&longitude="+longitude+"&orderNo="+orderNo,
		cache: false,
		success: function(response){
			console.log('calculateDeliveryFee---'+JSON.stringify(response));

			if(response.status==1)
			{
				document.getElementById("STANDARD_SHIPPING_COST").innerHTML = response.fee;
				var tt = $('#total_cost').text();//document.getElementById('total_cost').value;
				console.log("tt---"+tt);
				var total_cost = parseFloat(tt) + parseFloat(response.fee);
				console.log("total_cost---"+total_cost);
				document.getElementById("total_cost").innerHTML = total_cost;
			}

			// if(response != null) {
			// 	var results = JSON.parse(response);
			// 	console.log('calculateDeliveryFee parsed---'+JSON.stringify(results));
			// }
		}
		});
	}

	function clearFields() {
		$("#mobileNo").val("");
		$("#email").val("");
		$("#firstName").val("");
		$("#lastName").val("");
		$("#address").val("");
		$("#latitude").val("");
		$("#longitude").val("");
		$("#notes").val("");
		$("#password").val("");
	}
</script>
<script>
function initMap() {
  var input = document.getElementById('address');
  var autocomplete = new google.maps.places.Autocomplete(input);
  autocomplete.addListener('place_changed', function() {
      var place = autocomplete.getPlace();
      //document.getElementById('geoData').style.display = "block";
      //document.getElementById('location-snap').innerHTML = place.formatted_address;
      //document.getElementById('lat-span').innerHTML = place.geometry.location.lat();
      //document.getElementById('lon-span').innerHTML = place.geometry.location.lng();
      //document.getElementById('geoData').style.display = "block";
      document.getElementById('address').innerHTML = place.formatted_address;
      //document.getElementById('lat-span').innerHTML = place.geometry.location.lat();
      //document.getElementById('lon-span').innerHTML = place.geometry.location.lng();

      document.getElementById('latitude').value = place.geometry.location.lat();
      document.getElementById('longitude').value = place.geometry.location.lng();

			calculateDeliveryFee(place.geometry.location.lat(),place.geometry.location.lng());

  });
}
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCVUsI4pzJdzWmgOqv9RZ-Qwzo1o3e_Cwg&libraries=places&callback=initMap" async defer></script>

	</body>
</html>
