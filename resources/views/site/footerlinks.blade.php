<!-- jQuery Plugins -->
<script src="{{ URL::asset('site/js/jquery.min.js')}}"></script>
<script src="{{ URL::asset('site/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('site/js/slick.min.js')}}"></script>
<script src="{{ URL::asset('site/js/nouislider.min.js')}}"></script>
<script src="{{ URL::asset('site/js/jquery.zoom.min.js')}}"></script>
<script src="{{ URL::asset('site/js/main.js')}}"></script>
<script src="{{ URL::asset('site/js/custom.js')}}"></script>
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<script>
        $(document).ready(function(){
            var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            $("#subscribebutton").click(function(){

              var email = $("#subscribeemail").val();
              //console.log("subscribe clicked---"+email+"---");

              $('.ajax-loader').css("visibility", "visible");

              $.ajax({
                  url: "{{ route('subscribe') }}?email=" + email,
                  method: 'GET',
                  success: function(data) {
                      //console.log("js html--"+data.message);
                      $('.ajax-loader').css("visibility", "hidden");
                      document.getElementById("writeinfo").innerHTML = "";
                      $("#writeinfo").append(data.message);
                      document.getElementById("subscribeemail").value = "";
                      document.getElementById("writeinfo").style.display = "block";
                  },
            			complete: function(){
            				$('.ajax-loader').css("visibility", "hidden");
            			}
              });

            });
       });

       $('#sort_by_id').change(function(){
            var sort_by_id = $(this).val();
            var show_id = $("#show_id").val();

            window.location.href = "{{url()->current()}}?sort_by=" + sort_by_id +"&show="+ show_id;

       });

       $('#show_id').change(function(){
            var show_id = $(this).val();
            var sort_by_id = $("#sort_by_id").val();

            window.location.href = "{{url()->current()}}?sort_by=" + sort_by_id +"&show="+ show_id;

       });

       $('#shopproductsizeId').change(function(){
            var size = $(this).val(); console.log("size--"+size);
            $('#addtocartitembutton').attr('data-size', size);
            //document.getElementById('addtocartitembutton').setAttribute("data-size", size);//.dataset.size = size;
            //el.setAttribute('data-size', size);
       });

       $('#shopproductcolorId').change(function(){
            var color = $(this).val(); console.log("color--"+color);
            document.getElementById('addtocartitembutton').dataset.color = color;
       });

       function saveCartItems(allitems)
    {
      var items = localStorage.getItem("mystorecart");
      var main_site="{{ url('/') }}";
      var _token = $('meta[name="csrf-token"]').attr('content');
        var dataString = 'items='+items+'&_token='+_token;
        jQuery.ajax({
            url:main_site+'/savecart',
            data: dataString,
            type: "POST",
            success: function(data){
                //console.log("js savecart--"+JSON.stringify(data));
                //$('#upvoteId'+id).html(data);
                if(data.status==1) {
                  window.location.href = main_site+'/checkout';
                }
            },
            error: function (){

            }
        });

    }

    </script>
    <script type='text/javascript' src="{{ URL::asset('site/js/jquery.mycart.js')}}"></script>
    <script type="text/javascript">
    $(function () {

      var goToCartIcon = function($addTocartBtn){
        var $cartIcon = $(".my-cart-icon");
        var $image = $('<img width="30px" height="30px" src="' + $addTocartBtn.data("image") + '"/>').css({"position": "fixed", "z-index": "999"});
        $addTocartBtn.prepend($image);
        var position = $cartIcon.position();
        $image.animate({
          top: position.top,
          right: position.right
        }, 500 , "linear", function() {
          $image.remove();
        });
      }

      $('.my-cart-btn').myCart({
        currencySymbol: 'Ksh. ',
        classCartIcon: 'my-cart-icon',
        classCartBadge: 'my-cart-badge',
        classProductQuantity: 'my-product-quantity',
        classProductRemove: 'my-product-remove',
        classCheckoutCart: 'my-cart-checkout',
        affixCartIcon: true,
        showCheckoutModal: true,
        numberOfDecimals: 2,
        // cartItems: [
        //
        // ],
        cartItems: localStorage.getItem("mystorecart"),
        clickOnAddToCart: function($addTocart){
          goToCartIcon($addTocart);
        },
        afterAddOnCart: function(products, totalPrice, totalQuantity) {
          localStorage.setItem("mystorecart", JSON.stringify(products));
          console.log("afterAddOnCart---"+ JSON.stringify(products));
          //console.log("afterAddOnCart", products, totalPrice, totalQuantity);
        },
        clickOnCartIcon: function($cartIcon, products, totalPrice, totalQuantity) {
          console.log("cart icon clicked", $cartIcon, products, totalPrice, totalQuantity);
        },
        checkoutCart: function(products, totalPrice, totalQuantity) {
          var checkoutString = "Total Price: " + totalPrice + "\nTotal Quantity: " + totalQuantity;
          checkoutString += "\n\n id \t size \t color \t name \t summary \t price \t quantity \t image path";
          $.each(products, function(){
            checkoutString += ("\n " + this.id + " \t " + this.size + " \t " + this.color + " \t " + this.name + " \t " + this.summary + " \t " + this.price + " \t " + this.quantity + " \t " + this.image);
          });
          saveCartItems(checkoutString);
          //alert(checkoutString)
          //console.log("checking out", products, totalPrice, totalQuantity);
        },
        getDiscountPrice: function(products, totalPrice, totalQuantity) {
          console.log("calculating discount", products, totalPrice, totalQuantity);
          return totalPrice * 0; //if you want to apply coupon, do it here, now discount is 0%
        }
      });

      // $("#addNewProduct").click(function(event) {
      //   var currentElementNo = $(".row").children().length + 1;
      //   $(".row").append('<div class="col-md-3 text-center"><img src="images/img_empty.png" width="150px" height="150px"><br>product ' + currentElementNo + ' - <strong>$' + currentElementNo + '</strong><br><button class="btn btn-danger my-cart-btn" data-id="' + currentElementNo + '" data-name="product ' + currentElementNo + '" data-summary="summary ' + currentElementNo + '" data-price="' + currentElementNo + '" data-quantity="1" data-image="images/img_empty.png">Add to Cart</button><a href="#" class="btn btn-info">Details</a></div>')
      // });
    });

    function getLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
      } else {
        alert("Geolocation is not supported by this browser");
      }
    }

    function showPosition(position) {
      var main_site="{{ url('/') }}";
      window.location.href = main_site+'/nearest?latitude='+position.coords.latitude+"&longitude="+position.coords.longitude;
    }
    </script>
