<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Permissions</title>
    @include('headerlink')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Permissions</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All roles</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Permissions</div>

                                          </div>
                                          <div class="ibox-body">

                      <div id="failElem" class="alert alert-danger alert-dismissible alertbox" permission="alert">
                      </div>

                      <div id="passElem" class="alert alert-success alert-dismissible alertbox" permission="alert">
                      </div>

                      <p>Check permissions to give rights and uncheck to deny permission and click save</p>

                            <table class="table table-striped table-hover">
                              <thead>
                                <tr>
                                  <th>Role Name</th>
                                  <th>Permissions</th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php foreach ($roles as $key) { ?>
                                <tr>
                                  <td><?php echo $key->roleName; ?></td>
                                  <td><div class="row"><?php
                                  $permissions = \App\Permissions::getAll();
                                  foreach ($permissions as $keyp) {

                                    $check = \App\Userpermissions::checkPermissionRole($key->id,$keyp->id);
                                    if($check) {
                                        $status = $check->status;
                                    }
                                    else {
                                        $status = 0;
                                    }

                                    ?>
                                    <div class="col-md-6">
                                        <input type="checkbox" name="permission[]" <?php if($status==1) { echo "checked"; } ?> id="permission<?php echo $keyp->id; ?>" onclick="permissionChange('<?php echo $key->id; ?>','<?php echo $keyp->id; ?>');" value="<?php echo $keyp->id; ?>"> <?php echo $keyp->permissionName; ?>
                                    </div>
                                  <?php
                                  }
                                  ?></div></td>
                                </tr>
                                <?php } ?>
                              </tbody>
                            </table>

                            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js "></script>
                  <script>
                  function permissionChange(roleId,user_permission_id)
                  {
                    if (document.getElementById('permission'+user_permission_id).checked)
                    {
                        var status = 1;
                    } else {
                        var status = 0;
                    }

                    $.ajax({
                    type : 'get',
                    url : '{{URL::to('updatepermission')}}',
                    data:{'roleId':roleId,'status':status,'user_permission_id':user_permission_id},
                    success:function(data){
                      console.log("updatepermission data---"+data);
                      if (data == 200){
                        if(status==1) {
                          document.getElementById("passElem").innerHTML = "Permission saved successfully";
                          $("#passElem").show("slow").delay(5000).hide("slow");
                        }
                        else {
                          document.getElementById("failElem").innerHTML = "Permission removed successfully";
                          $("#failElem").show("slow").delay(5000).hide("slow");
                        }
                        return false;
                      }
                      else {
                        document.getElementById("failElem").innerHTML = "Failed to update permission";
                        $("#failElem").show("slow").delay(5000).hide("slow");
                      }
                    }
                    });

                  }

                  $("#passElem").hide();
                  $("#failElem").hide();
                  </script>
                  <script type="text/javascript">
                  $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
                  </script>

                                          </div>
                                        </div>
                                      </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
  </body>

  </html>
