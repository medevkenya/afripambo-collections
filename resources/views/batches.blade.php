<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Batches</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Batches</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All Batches</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Batches</div>
                                              <div class="ibox-tools">
                                                  <a href="<?php $url = URL::to("/stockreport/"); print_r($url); ?>">
                                                    <button type="button" class="btn btn-warning"><i class="fa fa-list"></i> Get Report as at <?php echo date('d-m-Y H:i'); ?>
                                                    </button>
                                                    </a>
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addrole"><i class="fa fa-plus"></i> Create New</button>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="modal-addrole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                {!! Form::open(['url' => 'addbatch']) !!}
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1">Create New Batch</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">

                                                  <?php
                                                  $categories = \App\Categories::getAll(0);
                                                  $suppliers = \App\Suppliers::getAll();
                                                  $shops = \App\Shops::getAll(0);
                                                  ?>

                                                  <div class="col-sm-12 form-group">
                                                      <label>Supplier</label>
                                                      <select class="form-control" name="supplierId" required>
                                                     <option value=""></option>
                                                     <?php foreach ($suppliers as $keyfs) { ?>
                                                       <option value="<?php echo $keyfs->id; ?>"><?php echo $keyfs->supplierName; ?></option>
                                                     <?php } ?>
                                                   </select>
                                                  </div>

                                                  <div class="col-sm-12 form-group">
                                                      <label>Shop</label>
                                                      <select class="form-control" name="shopId" id="shopId" required>
                                                     <option value=""></option>
                                                     <?php foreach ($shops as $keyfshop) { ?>
                                                       <option value="<?php echo $keyfshop->id; ?>"><?php echo $keyfshop->shopName; ?></option>
                                                     <?php } ?>
                                                   </select>
                                                  </div>

                                                  <div class="col-sm-12 form-group">
                                                      <label>Shop Product</label>
                                                      <select class="form-control" name="shopProductId" id="shopProductId" required>
                                                   </select>
                                                  </div>

                                                </div>
                                                <div class="row">

                                              <div class="col-sm-6 form-group">
                                                  <label>Quantity</label>
                                                  <input class="form-control" type="number" name="quantity" required>
                                              </div>

                                              <div class="col-sm-6 form-group">
                                                  <label>Type</label>
                                                  <select class="form-control" name="type" id="type" required>
                                                   <option value="Cash">Cash</option>
                                                   <option value="Credit">Credit</option>
                                               </select>
                                              </div>

                                            </div>
                                            <div class="row">

                                          <div class="col-sm-6 form-group">
                                              <label>Batch No.</label>
                                              <input class="form-control" type="number" name="batchNo">
                                          </div>

                                          <div class="col-sm-6 form-group">
                                              <label>Expiry Date</label>
                                              <input class="form-control" type="date" name="expiryDate">
                                          </div>

                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                              </div>
                                              {!! Form::close() !!}
                                              </div>
                                            </div>

                                            @if (count($errors) > 0)
                                             <div class="alert alert-danger">
                                                 <ul>
                                                     @foreach ($errors->all() as $error)
                                                     <li>{{ $error }}</li>
                                                     @endforeach
                                                 </ul>
                                             </div>
                                            @endif

                                            @if ($message = Session::get('error'))
                                                 <div class="alert alert-danger">
                                                     {{ $message }}
                                                 </div>
                                            @endif

                                            @if ($message = Session::get('success'))
                                                 <div class="alert alert-success">
                                                     {{ $message }}
                                                 </div>
                                            @endif

                                            @if (session('status0'))
                                            <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            {{ session('status0') }}
                                            </div>
                                            @endif

                                            @if (session('status1'))
                                            <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            {{ session('status1') }}
                                            </div>
                                            @endif

                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                          <th>Supplier</th>
                                          <th>Product</th>
                                          <th>Qty</th>
                                          <th>Shop</th>
                                          <th>Type</th>
                                          <th>Batch No.</th>
                                          <th>Expiry</th>
                                          <th>Paid</th>
                                          <th>Created On</th>
                                          <th width="15%">Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Supplier</th>
                                          <th>Product</th>
                                          <th>Qty</th>
                                          <th>Shop</th>
                                          <th>Type</th>
                                          <th>Batch No.</th>
                                          <th>Expiry</th>
                                          <th>Paid</th>
                                          <th>Created On</th>
                                          <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($list as $batch) {
                                        ?>
                                        <tr>
                                          <td><?php echo $batch->supplierName; ?></td>
                                          <td><?php echo $batch->productName." ".$batch->unit; ?></td>
                                          <td><?php echo $batch->quantity; ?></td>
                                          <td><?php echo $batch->shopName; ?></td>
                                          <td><?php echo $batch->type; ?></td>
                                          <td><?php echo $batch->batchNo; ?></td>
                                          <td><?php echo $batch->expiryDate; ?></td>
                                          <td><?php echo $batch->isPaid; ?></td>
                                          <td><?php echo $batch->created_at; ?></td>
                                          <td>
                                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-eject<?php echo $batch->id; ?>"><i class="fa fa-edit"></i> Eject</button>
                                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editrole<?php echo $batch->id; ?>"><i class="fa fa-edit"></i></button>
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deleterole<?php echo $batch->id; ?>"><i class="fa fa-trash"></i></button>
                                        </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-editrole<?php echo $batch->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'editbatch']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Batch</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $batch->id; ?>" class="form-control" required>
                                          </div>
                                          <div class="col-sm-12 form-group">
                                              <label>Supplier</label>
                                              <select class="form-control" name="supplierId" required>
                                             <option value="<?php echo $batch->supplierId; ?>"><?php echo $batch->supplierName; ?></option>
                                             <?php foreach ($suppliers as $keyfs) { ?>
                                               <option value="<?php echo $keyfs->id; ?>"><?php echo $keyfs->supplierName; ?></option>
                                             <?php } ?>
                                           </select>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Shop</label>
                                              <select class="form-control" name="shopId" onchange="shopIde('<?php echo $batch->id; ?>')" id="shopIde<?php echo $batch->id; ?>" required>
                                               <option value="<?php echo $batch->shopId; ?>"><?php echo $batch->shopName; ?></option>
                                             <?php foreach ($shops as $keyfshop) { ?>
                                               <option value="<?php echo $keyfshop->id; ?>"><?php echo $keyfshop->shopName; ?></option>
                                             <?php } ?>
                                           </select>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Shop Product</label>
                                              <select class="form-control" name="shopProductId" id="shopProductIde<?php echo $batch->id; ?>" required>
                                                <option value="<?php echo $batch->shopProductId; ?>"><?php echo $batch->productName; ?></option>
                                           </select>
                                          </div>

                                        </div>
                                        <div class="row">
                                          <div class="col-sm-6 form-group">
                                              <label>Payment</label>
                                              <select class="form-control" name="isPaid" required>
                                                <option value="<?php echo $batch->type; ?>"><?php echo $batch->type; ?></option>
                                               <option value="Paid">Paid</option>
                                               <option value="Pending">Pending</option>
                                           </select>
                                          </div>
                                          <div class="col-sm-6 form-group">
                                              <label>Type</label>
                                              <select class="form-control" name="type" required>
                                                <option value="<?php echo $batch->type; ?>"><?php echo $batch->type; ?></option>
                                               <option value="Cash">Cash</option>
                                               <option value="Credit">Credit</option>
                                           </select>
                                          </div>

                                        </div>

                                        <div class="row">

                                      <div class="col-sm-6 form-group">
                                          <label>Batch No.</label>
                                          <input class="form-control" type="number" name="batchNo" value="<?php echo $batch->batchNo; ?>">
                                      </div>

                                      <div class="col-sm-6 form-group">
                                          <label>Expiry Date</label>
                                          <input class="form-control" type="date" name="expiryDate" value="<?php echo $batch->expiryDate; ?>">
                                      </div>



                                    </div>


                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-eject<?php echo $batch->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'ejectbatch']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Eject Batch Quantity</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                        <div class="row">
                                          <div class="col-sm-12 form-group">
                                          <p>The quantity will be deducted from the current quantity balance for this shop product</p>
                                        </div>
                                          <input type="hidden" name="id" value="<?php echo $batch->id; ?>" class="form-control" required>
                                          <input type="hidden" name="shopProductId" value="<?php echo $batch->shopProductId; ?>" class="form-control" required>
                                          <div class="col-sm-6 form-group">
                                              <label>Quantity</label>
                                              <input class="form-control" type="number" name="quantity" required>
                                          </div>
                                        </div>

                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-deleterole<?php echo $batch->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deletebatch']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Batch</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $batch->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this batch</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>

                                          </div>
                                        </div>
                                      </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
  </body>

  </html>
