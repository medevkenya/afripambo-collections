<?php
$settings = \App\Settings::getDetails();
?>

<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<style>
    html, body {font-size: 16px;}
</style>
<table style="background:#00AEED;width:100%;border-radius:4px;padding-left:2%;">

<p style="color:#000000;">Hi {{ $firstName }} {{ $lastName }},</p>

<p style="color:#000000;">We hope you’re well! Notification for invoice number {{$orderNo}} for your purchase. You will receive a confirmation once payment is received, and don’t hesitate to reach out if you have any questions or concerns.</p>

<p style="color:#000000;">Kind regards,</p>

<p style="color:#000000;"><?php echo $settings->companyName; ?></p>

<p style="color:#000000;">
Attached is your invoice with more details.
</p>
<p>We request you to add our email to your address book to avoid missing out on information that we send to you from time to time.</p>
<p style="color:#000000;">
Thank you for choosing <?php echo $settings->companyName; ?>.
</p>
<p style="color:#000000;">All emails sent to or from <?php echo $settings->companyName; ?> are subject to <?php echo $settings->companyName; ?>'s Terms & Conditions of use.</p>
<p style="color:#000000;">&copy; All rights reserved</p>
</table>
