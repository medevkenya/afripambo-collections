<?php
$settings = \App\Settings::getDetails();
?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
body {
    background: grey;
    margin-top: 120px;
    margin-bottom: 120px;
}
</style>
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="row p-5">
                        <div class="col-md-6">
                            <img src="{{ URL::asset('site/img/logo.png')}}">
                        </div>

                        <div class="col-md-6 text-right">
                            <p class="font-weight-bold mb-1">Invoice #{{$orderNo}}</p>
                            <p class="text-muted">Due to: {{date("d M, Y", strtotime($orderdetails->created_at))}}</p>
                        </div>
                    </div>

                    <hr class="my-5">

                    <div class="row pb-5 p-5">
                        <div class="col-md-6">
                            <p class="font-weight-bold mb-4">Client Information</p>
                            <p class="mb-1">{{$name}}</p>
                            <p>{{$orderdetails->email}}</p>
                            <p class="mb-1">{{$orderdetails->mobileNo}}</p>
                            <p class="mb-1">{{$orderdetails->address}}</p>
                        </div>

                        <div class="col-md-6 text-right">
                            <p class="font-weight-bold mb-4">Payment Details</p>
                            <p class="mb-1"><span class="text-muted">From: </span> <?php echo $settings->companyName; ?></p>
														<p class="mb-1"><span class="text-muted">Location: </span> <?php echo $settings->companyLocation; ?></p>
                            <p class="mb-1"><span class="text-muted">Address: </span> <?php echo $settings->companyAddress; ?></p>
                            <p class="mb-1"><span class="text-muted">Email: </span> <?php echo $settings->companyEmail; ?></p>
                            <p class="mb-1"><span class="text-muted">Telephone: </span> <?php echo $settings->companyTelephone; ?></p>
                        </div>
                    </div>

                    <div class="row p-5">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="border-0 text-uppercase small font-weight-bold">ID</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Item</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Quantity</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Unit Cost</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
																	<?php foreach ($cartlist as $keyvalue) { ?>
                                    <tr>
                                        <td><?php echo $keyvalue->id; ?></td>
                                        <td><?php echo $keyvalue->itemName; ?></td>
                                        <td><?php echo $keyvalue->quantity; ?></td>
                                        <td><?php echo $keyvalue->cost; ?></td>
                                        <td><?php echo $keyvalue->cost * $keyvalue->quantity; ?></td>
                                    </tr>
																	<?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="d-flex flex-row-reverse bg-dark text-white p-4">
                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Grand Total</div>
                            <div class="h2 font-weight-light">Ksh. <?php echo $total; ?></div>
                        </div>

                        <!-- <div class="py-3 px-5 text-right">
                            <div class="mb-2">Discount</div>
                            <div class="h2 font-weight-light">10%</div>
                        </div> -->

                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Sub - Total amount</div>
                            <div class="h2 font-weight-light">Ksh. <?php echo $total; ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- <div class="text-light mt-5 mb-5 text-center small">by : <a class="text-light" target="_blank" href="http://totoprayogo.com">totoprayogo.com</a></div> -->

</div>
