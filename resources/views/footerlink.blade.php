<!-- CORE PLUGINS-->
<script src="{{ URL::asset('assets/vendors/jquery/dist/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/popper.js/dist/umd/popper.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/metisMenu/dist/metisMenu.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<!-- PAGE LEVEL PLUGINS-->
<script src="{{ URL::asset('assets/vendors/chart.js/dist/Chart.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/vendors/jvectormap/jquery-jvectormap-us-aea-en.js')}}" type="text/javascript"></script>
<!-- CORE SCRIPTS-->
<script src="{{ URL::asset('assets/js/app.min.js')}}" type="text/javascript"></script>
<!-- PAGE LEVEL SCRIPTS-->
<script src="{{ URL::asset('assets/js/scripts/dashboard_1_demo.js')}}" type="text/javascript"></script>
<script>
$("#categoryId").change(function(){
  $.ajax({
      url: "{{ route('getProducts') }}?categoryId=" + $(this).val(),
      method: 'GET',
      success: function(data) {
        //console.log("js products--"+data.products);
          $('#productId').html(data.products);
      }
  });
});

$("#categoryIdBatch").change(function(){
  $.ajax({
      url: "{{ route('getProducts') }}?categoryId=" + $(this).val(),
      method: 'GET',
      success: function(data) {
        //console.log("js products--"+data.products);
          $('#productId').html(data.products);
      }
  });
});

$("#shopIdsale").change(function(){
  var categoryId = document.getElementById("categoryIdsale").value;
  $.ajax({
      url: "{{ route('getShopProductsForSale') }}?shopId=" + $(this).val() + "&categoryId="+categoryId,
      method: 'GET',
      success: function(data) {
        //console.log("js products--"+data.products);
          $('#productId').html(data.products);
      }
  });
});

$("#categoryIdsale").change(function(){
  var shopId = document.getElementById("shopIdsale").value;
  $.ajax({
      url: "{{ route('getShopProductsForSale') }}?categoryId=" + $(this).val() + "&shopId="+shopId,
      method: 'GET',
      success: function(data) {
        //console.log("js products--"+data.products);
          $('#productId').html(data.products);
      }
  });
});

$("#shopId").change(function(){
  $.ajax({
      url: "{{ route('getShopProducts') }}?shopId=" + $(this).val(),
      method: 'GET',
      success: function(data) {
        //console.log("js products--"+data.products);
          $('#shopProductId').html(data.products);
      }
  });
});

function shopIde(id) { //alert(id);
  var d = document.getElementById("shopIde"+id+"").value;
  var id = document.getElementById("shopProductIde"+id+"").value;
  $.ajax({
      url: "{{ route('getShopProducts') }}?shopId=" + d + "&id="+id,
      method: 'GET',
      success: function(data) {
        //console.log("js products--"+data.products);
          $('#shopProductIde'+id+'').html(data.products);
      }
  });
}

$("#categoryIdbe").change(function(){
  $.ajax({
      url: "{{ route('getProducts') }}?categoryId=" + $(this).val(),
      method: 'GET',
      success: function(data) {
        //console.log("js products--"+data.products);
          $('#productIde').html(data.products);
      }
  });
});

function categoryIdbe(id) { //alert(id);
  var d = document.getElementById("categoryIdbe"+id+"").value;
  $.ajax({
      url: "{{ route('getProducts') }}?categoryId=" + d,
      method: 'GET',
      success: function(data) {
        //console.log("js products--"+data.products);
          $('#productIde'+id+'').html(data.products);
      }
  });
}

$("#productId").change(function(){
  $.ajax({
      url: "{{ route('getProduct') }}?productId=" + $(this).val(),
      method: 'GET',
      success: function(data)
      {
        console.log("js outPrice--"+data.outPrice);
        console.log("js availableStock--"+data.quantity);
        // if(data.availableStock > "0")
        // {
          $('#outPrice').html(data.outPrice);
        // }
        // else
        // {
        //   $('#lowStockAlert').modal('show');
        // }
      }
  });
});
$(document).ready(function (e) {
$('#photoUrl').change(function(){
let reader = new FileReader();
reader.onload = (e) => {
  $('#preview-image-before-upload').attr('src', e.target.result);
}
reader.readAsDataURL(this.files[0]);
});
$('#photoUrl1').change(function(){
let reader = new FileReader();
reader.onload = (e) => {
  $('#preview-image-before-upload1').attr('src', e.target.result);
}
reader.readAsDataURL(this.files[0]);
});
$('#photoUrl2').change(function(){
let reader = new FileReader();
reader.onload = (e) => {
  $('#preview-image-before-upload2').attr('src', e.target.result);
}
reader.readAsDataURL(this.files[0]);
});
$('#photoUrl3').change(function(){
let reader = new FileReader();
reader.onload = (e) => {
  $('#preview-image-before-upload3').attr('src', e.target.result);
}
reader.readAsDataURL(this.files[0]);
});
$('#photoUrl4').change(function(){
let reader = new FileReader();
reader.onload = (e) => {
  $('#preview-image-before-upload4').attr('src', e.target.result);
}
reader.readAsDataURL(this.files[0]);
});
$('#photoUrl11').change(function(){
let reader = new FileReader();
reader.onload = (e) => {
  $('#preview-image-before-upload11').attr('src', e.target.result);
}
reader.readAsDataURL(this.files[0]);
});
$('#photoUrl22').change(function(){
let reader = new FileReader();
reader.onload = (e) => {
  $('#preview-image-before-upload22').attr('src', e.target.result);
}
reader.readAsDataURL(this.files[0]);
});
$('#photoUrl33').change(function(){
let reader = new FileReader();
reader.onload = (e) => {
  $('#preview-image-before-upload33').attr('src', e.target.result);
}
reader.readAsDataURL(this.files[0]);
});
$('#photoUrl44').change(function(){
let reader = new FileReader();
reader.onload = (e) => {
  $('#preview-image-before-upload44').attr('src', e.target.result);
}
reader.readAsDataURL(this.files[0]);
});
});
</script>
