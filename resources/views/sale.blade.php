<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Make Sales</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Make Sales</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="#"><i class="la la-home font-20"></i></a>
                    </li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                  <div class="col-md-12">
                      <div class="ibox">
                          <div class="ibox-head">
                              <div class="ibox-title">Make Sales</div>
                          </div>
                          <div class="ibox-body">
                            @if (count($errors) > 0)
                                   <div class="alert alert-danger">
                                       <ul>
                                           @foreach ($errors->all() as $error)
                                           <li>{{ $error }}</li>
                                           @endforeach
                                       </ul>
                                   </div>
                                  @endif

                                  @if ($message = Session::get('error'))
                                       <div class="alert alert-danger">
                                           {{ $message }}
                                       </div>
                                  @endif

                                  @if ($message = Session::get('success'))
                                       <div class="alert alert-success">
                                           {{ $message }}
                                       </div>
                                  @endif

                                  @if (session('status0'))
                                  <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  {{ session('status0') }}
                                  </div>
                                  @endif

                                  @if (session('status1'))
                                  <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  {{ session('status1') }}
                                  </div>
                                  @endif

                                  <?php
                                  $categories = \App\Categories::getAll(0);
                                  $customers = \App\Customers::getAll(0);
                                  $shops = \App\Shops::getAll(0);
                                  ?>

                                  <div class="row">
                                    <div class="col-sm-5 form-group">
                                        <label>Shop</label>
                                        <select class="form-control" name="shopId" id="shopIdsale" required>
                                       <option value=""></option>
                                       <?php foreach ($shops as $keyfshop) { ?>
                                         <option value="<?php echo $keyfshop->id; ?>"><?php echo $keyfshop->shopName; ?></option>
                                       <?php } ?>
                                     </select>
                                    </div>
                                  </div>

                                    <div class="row">
                                    <div class="col-sm-2 form-group">
                                        <label>Category</label>
                                        <select class="form-control" name="categoryId" id="categoryIdsale" required>
                                       <option></option>
                                       <?php foreach ($categories as $keyf) { ?>
                                         <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->categoryName; ?></option>
                                       <?php } ?>
                                     </select>
                                    </div>

                                    <div class="col-sm-3 form-group">
                                        <label>Product</label> <strong>(Unit Cost: Ksh. <span id="outPrice">_</span>)</strong>
                                        <select class="form-control" name="productId" id="productId" required>
                                     </select>
                                    </div>

                                  <div class="col-sm-2 form-group">
                                      <label>Customer</label>
                                      <select class="form-control" name="customerId" id="customerId" required>
                                     <option></option>
                                     <?php foreach ($customers as $keyfcus) { ?>
                                       <option value="<?php echo $keyfcus->id; ?>"><?php echo $keyfcus->customerName; ?></option>
                                     <?php } ?>
                                   </select>
                                  </div>

                                  <div class="form-group col-md-2">
                                    <label>Quantity</label>
                                    <input type="number" class="form-control" name="quantity" id="saleQuantity" required>
                                  </div>

                                  <div class="form-group col-md-2">
                                    <label>Discount</label>
                                    <input type="number" class="form-control" name="discount" id="saleDiscount" required>
                                  </div>

                                  <div class="col-sm-1 form-group">
                                      <label>Type</label>
                                      <select class="form-control" name="type" id="type" required>
                                       <option value="Cash">Cash</option>
                                       <option value="Credit">Credit</option>
                                   </select>
                                  </div>

                                </div>
                                <div class="row">

                                  <div class="form-group col-md-2">
                                    <button type="button" id="completesale" class="btn btn-block btn-success">SUBMIT</button>
                                  </div>

                                  <div class="form-group col-md-3">
                                    <h3>Total: Ksh. <span id="totalCost">0</span></h3>
                                  </div>
                                  <div class="form-group col-md-7">
                                  <div class="ajax-loader">
                              			<img src="{{ URL::to('/') }}/public/images/loading.gif" class="img-responsive" />
                              		</div>
                                </div>

                                  </div>


                          </div>
                        </div>
                      </div>

                    <div class="col-md-12">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Sales List <?php if(isset($fromdate)) { echo "From: ".$fromdate." To: ".$todate; } ?></div>
                                <?php if(isset($fromdate) && !empty($fromdate)) { ?>
                                <div class="ibox-tools">
                                    <a href="<?php $url = URL::to("/exportfiltersaleexcel/".$fromdate."/".$todate."/"); print_r($url); ?>">
                                      <button type="button" class="btn btn-warning"><i class="fa fa-file-excel-o"></i> Generate Excel
                                      </button>
                                      </a>
                                      <!-- <a href="<?php //$url = URL::to("/exportfiltersalepdf/".$fromdate."/".$todate."/"); print_r($url); ?>">
                                        <button type="button" class="btn btn-warning"><i class="fa fa-file-pdf-o"></i> Generate PDF
                                        </button>
                                        </a> -->
                                </div>
                              <?php } ?>
                            </div>
                            <div class="ibox-body">

                                    {!! Form::open(['url' => 'postfiltersalereport']) !!}
                                    {{ csrf_field() }}
                                    <div class="row">

                                    <div class="form-group col-md-3">
                                      <label>From Date</label>
                                      <input type="date" class="form-control" name="fromdate" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                      <label>To Date</label>
                                      <input type="date" class="form-control" name="todate" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                      <label>.</label>
                                      <button type="submit" class="btn btn-block btn-primary">Search</button>
                                    </div>
                                    </div>
                                    {!! Form::close() !!}

                                    <?php if(isset($list) && !empty($list)) { ?>
                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                          <th>Product</th>
                                          <th>Quantity</th>
                                          <th>Amount (Ksh.)</th>
                                          <th>Discount (Ksh.)</th>
                                          <th>Customer</th>
                                          <th>Type</th>
                                          <th>Created On</th>
                                          <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Product</th>
                                          <th>Quantity</th>
                                          <th>Amount (Ksh.)</th>
                                          <th>Discount (Ksh.)</th>
                                          <th>Customer</th>
                                          <th>Type</th>
                                          <th>Created On</th>
                                          <th>Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($list as $sale) {
                                        ?>
                                        <tr>
                                          <td><?php echo $sale->productName." ".$sale->unit; ?></td>
                                          <td><?php echo $sale->quantity; ?></td>
                                          <td><?php echo $sale->total; ?></td>
                                          <td><?php echo $sale->discount; ?></td>
                                          <td><?php echo $sale->customerName; ?></td>
                                          <td><?php echo $sale->type; ?></td>
                                          <td><?php echo $sale->created_at; ?></td>
                                          <td>
                                            <!-- <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-edit<?php //echo $sale->id; ?>"><i class="fa fa-edit"></i> Edit</button> -->
                                            <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete<?php echo $sale->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                                          </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-edit<?php echo $sale->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'editsale']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Sale</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-12 col-lg-12 col-md-12">
                                              <input type="hidden" name="id" value="<?php echo $sale->id; ?>" class="form-control" required>
                                          </div>

                                          <div class="col-sm-12 form-group">
                                              <label>Quantity</label>
                                              <input class="form-control" type="number" name="meterSale" value="<?php echo $sale->quantity; ?>" required>
                                            </div>

                                            </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-delete<?php echo $sale->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deletesale']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Sale</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $sale->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this sale</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>
                              <?php } ?>

                              <!-- Modal -->
                              <div class="modal fade" id="lowStockAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <!-- <div class="modal-header bg-success">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title">Completed</h4>
                                    </div> -->
                                    <div class="modal-body">
                                      <p>Not enough stock for this product</p>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <!-- Modal -->
                              <div class="modal fade" id="successmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <!-- <div class="modal-header bg-success">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title">Completed</h4>
                                    </div> -->
                                    <div class="modal-body">
                                      <p>New sale submited successfully</p>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                                      <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                    </div>
                                  </div>
                                </div>
                              </div>


                              <!-- Modal -->
                              <div class="modal fade" id="invalidsale" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                  <div class="modal-content">
                                    <!-- <div class="modal-header bg-success">
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                      <h4 class="modal-title">Completed</h4>
                                    </div> -->
                                    <div class="modal-body">
                                      <p>Invalid sale. Check the fields and try again</p>
                                    </div>
                                    <div class="modal-footer">
                                      <a href="{{URL::to('/sales')}}" class="btn btn-default">Close</a>
                                      <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
                                    </div>
                                  </div>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')

    <script>
    $(document).ready(function(){
        //$("#discount").on("input", function(){
    			$("#saleQuantity").on("keyup", function(event) {
            // Print entered value in a div box
            var quantity = $(this).val();
    				var productId = document.getElementById("productId").value;
    				var totalCost = document.getElementById("totalCost").innerHTML;
            var outPrice = document.getElementById("outPrice").innerHTML;
    				var discount = document.getElementById("saleDiscount").value;

    				//if(subtotal > discount) {
    				//var newsubtotal = +subtotal - +discount;
    				var newtotal = +quantity * +outPrice;
    				$("#totalCost").html(parseInt(newtotal));
    				//$("#subTotal").html(parseInt(newsubtotal));
    			// }
    			// else {
    			// 	alert("Invalid discount");
    			// 	$("#discount").html("0");
    			// }
        });
        $("#saleDiscount").on("keyup", function(event) {
          // Print entered value in a div box
          var discount = $(this).val();
          var productId = document.getElementById("productId").value;
          var totalCost = document.getElementById("totalCost").innerHTML;
          var outPrice = document.getElementById("outPrice").innerHTML;
          var quantity = document.getElementById("saleQuantity").value;

          if(quantity > 0) {
          //var newsubtotal = +subtotal - +discount;
          var oldtotal = +quantity * +outPrice;
          var newtotal = +oldtotal - +discount;

          if(newtotal > 0) {

          $("#totalCost").html(parseInt(newtotal));
          //$("#subTotal").html(parseInt(newsubtotal));
        }
         }
        // else {
        // 	alert("Invalid discount");
        // 	$("#discount").html("0");
        // }
      });
    });

    $(document).ready(function(){
    		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    		$("#completesale").click(function(){

          var categoryId = document.getElementById("categoryIdsale").value;
          var customerId = document.getElementById("customerId").value;
          var productId = document.getElementById("productId").value;
          var totalCost = document.getElementById("totalCost").innerHTML;
          var outPrice = document.getElementById("outPrice").innerHTML;
          var quantity = document.getElementById("saleQuantity").value;
          var discount = document.getElementById("saleDiscount").value;
          var type = document.getElementById("type").value;

          var oldtotal = +quantity * +outPrice;
          var newtotal = +oldtotal - +discount;

          if(quantity > 0 && newtotal > 0) {

    			$('.ajax-loader').css("visibility", "visible");

    			$.ajax({
    					url: "{{ route('completesale') }}?discount=" + discount+"&total="+totalCost+"&type="+type+"&customerId="+customerId+"&quantity="+quantity+"&categoryId="+categoryId+"&productId="+productId+"&outPrice="+outPrice,
    					method: 'GET',
    					success: function(data) {
    							//console.log("js html--"+data.message);
    							$('.ajax-loader').css("visibility", "hidden");

                  if(data.status==1)
                  {
    							  $('#successmodal').modal('show');
                  }
                  else
                  {
                    alert(data.message);
                  }

    					},
    					complete: function(){
    						$('.ajax-loader').css("visibility", "hidden");
    					}
    			});

        }
        else
        {
          $('#invalidsale').modal('show');
        }

    		});
    });
  </script>

  </body>

  </html>
