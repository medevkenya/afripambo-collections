<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Shop Products</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Shop Products</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">All Shop Products</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                                  <div class="col-md-12">
                                      <div class="ibox">
                                          <div class="ibox-head">
                                              <div class="ibox-title">Shop Products</div>
                                              <div class="ibox-tools">
                                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addrole"><i class="fa fa-plus"></i> Create New</button>
                                              </div>
                                          </div>
                                          <div class="ibox-body">

                                            <!-- Modal -->
                                            <div class="modal fade text-left" id="modal-addrole" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                              <div class="modal-dialog modal-lg" role="document">
                                              <form action="{{ route('addshopproduct') }}" method="post" enctype="multipart/form-data">
                                              {{ csrf_field() }}
                                              <div class="modal-content">
                                                <div class="modal-header">
                                                <h4 class="modal-title" id="myModalLabel1">Create New Product</h4>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                                </div>
                                                <div class="modal-body">
                                                <div class="row">

                                                  <?php
                                                  $categories = \App\Categories::getAll(0);
                                                  $shops = \App\Shops::getAll(0);
                                                  ?>

                                                  <div class="col-sm-6 form-group">
                                                      <label>Shop</label>
                                                      <select class="form-control" name="shopId" id="shopId" required>
                                                     <option></option>
                                                     <?php foreach ($shops as $keyfshop) { ?>
                                                       <option value="<?php echo $keyfshop->id; ?>"><?php echo $keyfshop->shopName; ?></option>
                                                     <?php } ?>
                                                   </select>
                                                  </div>

                                                  <div class="col-sm-6 form-group">
                                                      <label>Condition</label>
                                                      <select class="form-control" name="productCondition" id="productCondition" required>
                                                     <option></option>
                                                       <option value="New">New</option>
                                                       <option value="Used">Used</option>
                                                   </select>
                                                  </div>

                                                </div>
                                                <div class="row">
                                                  <div class="col-sm-6 form-group">
                                                      <label>Category</label>
                                                      <select class="form-control" name="categoryId" id="categoryId" required>
                                                     <option></option>
                                                     <?php foreach ($categories as $keyf) { ?>
                                                       <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->categoryName; ?></option>
                                                     <?php } ?>
                                                   </select>
                                                  </div>

                                                  <div class="col-sm-6 form-group">
                                                      <label>Product</label>
                                                      <select class="form-control" name="productId" id="productId" required>
                                                   </select>
                                                  </div>

                                                </div>
                                                <div class="row">

                                                  <div class="col-sm-6 form-group">
                                                      <label>In Price</label>
                                                      <input class="form-control" type="number" name="inPrice" required>
                                                  </div>

                                                  <div class="col-sm-6 form-group">
                                                      <label>Out Price</label>
                                                      <input class="form-control" type="number" name="outPrice" required>
                                                  </div>

                                            </div>
                                            <div class="row">

                                              <div class="col-sm-6 form-group">
                                                  <label>Quantity</label>
                                                  <input class="form-control" type="number" name="quantity" required>
                                              </div>

                                              <div class="col-sm-6 form-group">
                                                  <label>Discount</label>
                                                  <input class="form-control" type="number" name="discount" value="0" required>
                                              </div>

                                            </div>

                                            <div class="row">

                                            <div class="col-sm-6 form-group">
                                              <label>Alert Level</label>
                                              <input class="form-control" type="number" name="alertLevel" required>
                                            </div>

                                            </div>

                                                </div>
                                                <div class="modal-footer">
                                                <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                </div>
                                              </div>
                                              {!! Form::close() !!}
                                              </div>
                                            </div>

                                            @error('photoUrl')
                                            <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                            @enderror

                                            @if (count($errors) > 0)
                                             <div class="alert alert-danger">
                                                 <ul>
                                                     @foreach ($errors->all() as $error)
                                                     <li>{{ $error }}</li>
                                                     @endforeach
                                                 </ul>
                                             </div>
                                            @endif

                                            @if ($message = Session::get('error'))
                                                 <div class="alert alert-danger">
                                                     {{ $message }}
                                                 </div>
                                            @endif

                                            @if ($message = Session::get('success'))
                                                 <div class="alert alert-success">
                                                     {{ $message }}
                                                 </div>
                                            @endif

                                            @if (session('status0'))
                                            <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            {{ session('status0') }}
                                            </div>
                                            @endif

                                            @if (session('status1'))
                                            <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            {{ session('status1') }}
                                            </div>
                                            @endif

                                    <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Product Name</th>
                                            <th>Shop</th>
                                            <th>Qty</th>
                                            <th>In Price</th>
                                            <th>Out Price</th>
                                            <th>Discount</th>
                                            <th>Condition</th>
                                            <th>Alert</th>
                                            <th width="15%">Actions</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                          <th>Product Name</th>
                                          <th>Shop</th>
                                          <th>Qty</th>
                                          <th>In Price</th>
                                          <th>Out Price</th>
                                          <th>Discount</th>
                                          <th>Condition</th>
                                          <th>Alert</th>
                                          <th width="15%">Actions</th>
                                        </tr>
                                    </tfoot>
                                    <tbody>
                                      <?php foreach ($list as $shopproduct) {
                                        ?>
                                        <tr>
                                          <td><?php echo $shopproduct->productName; ?></td>
                                          <td><?php echo $shopproduct->shopName; ?></td>
                                          <td><?php echo $shopproduct->quantity." ".$shopproduct->unit; ?></td>
                                          <td><?php echo $shopproduct->inPrice; ?></td>
                                          <td><?php echo $shopproduct->outPrice; ?></td>
                                          <td><?php echo $shopproduct->discount; ?></td>
                                          <td><?php echo $shopproduct->productCondition; ?></td>
                                          <td><?php echo $shopproduct->alertLevel; ?></td>
                                          <td>
                                          <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-editrole<?php echo $shopproduct->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                                          <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-deleterole<?php echo $shopproduct->id; ?>"><i class="fa fa-trash"></i> Delete</button>
                                        </td>
                                        </tr>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-editrole<?php echo $shopproduct->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog modal-lg" role="document">
                                            <form action="{{ route('editshopproduct') }}" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Edit Product</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">

                                              <input type="hidden" name="id" value="<?php echo $shopproduct->id; ?>" class="form-control" required>

                                              <div class="row">

                                                <?php
                                                // $categories = \App\Categories::getAll(0);
                                                // $shops = \App\Shops::getAll(0);
                                                ?>

                                                <div class="col-sm-6 form-group">
                                                    <label>Shop</label>
                                                    <select class="form-control" name="shopId" id="shopId" required>
                                                   <option value="<?php echo $shopproduct->shopId; ?>"><?php echo $shopproduct->shopName; ?></option>
                                                   <?php foreach ($shops as $keyfshop) { ?>
                                                     <option value="<?php echo $keyfshop->id; ?>"><?php echo $keyfshop->shopName; ?></option>
                                                   <?php } ?>
                                                 </select>
                                                </div>

                                                <div class="col-sm-6 form-group">
                                                    <label>Condition</label>
                                                    <select class="form-control" name="productCondition" id="productCondition" required>
                                                     <option value="<?php echo $shopproduct->productCondition; ?>"><?php echo $shopproduct->productCondition; ?></option>
                                                     <option value="New">New</option>
                                                     <option value="Used">Used</option>
                                                 </select>
                                                </div>

                                              </div>

                                              <div class="row">

                                                <div class="col-sm-6 form-group">
                                                    <label>Category</label>
                                                    <select class="form-control" name="categoryId" onchange="categoryIdbe('<?php echo $shopproduct->id; ?>')" id="categoryIdbe<?php echo $shopproduct->id; ?>"  required>
                                                   <option value="<?php echo $shopproduct->categoryId; ?>"><?php echo $shopproduct->categoryName; ?></option>
                                                   <?php foreach ($categories as $keyf) { ?>
                                                     <option value="<?php echo $keyf->id; ?>"><?php echo $keyf->categoryName; ?></option>
                                                   <?php } ?>
                                                 </select>
                                                </div>

                                                <div class="col-sm-6 form-group">
                                                    <label>Product</label>
                                                    <select class="form-control" name="productId" id="productIde<?php echo $shopproduct->id; ?>" required>
                                                      <option value="<?php echo $shopproduct->productId; ?>"><?php echo $shopproduct->productName; ?></option>
                                                   </select>
                                                </div>

                                              </div>

                                              <div class="row">

                                                <div class="col-sm-6 form-group">
                                                    <label>In Price</label>
                                                    <input class="form-control" type="number" name="inPrice" value="<?php echo $shopproduct->inPrice; ?>" required>
                                                </div>

                                                <div class="col-sm-6 form-group">
                                                    <label>Out Price</label>
                                                    <input class="form-control" type="number" name="outPrice" value="<?php echo $shopproduct->outPrice; ?>" required>
                                                </div>

                                          </div>
                                          <div class="row">

                                            <!-- <div class="col-sm-6 form-group">
                                                <label>Quantity</label>
                                                <input class="form-control" type="number" name="quantity" value="<?php //echo $shopproduct->quantity; ?>" required>
                                            </div> -->

                                            <div class="col-sm-6 form-group">
                                                <label>Discount</label>
                                                <input class="form-control" type="number" name="discount" value="<?php echo $shopproduct->discount; ?>" required>
                                            </div>

                                          </div>

                                          <div class="row">

                                          <div class="col-sm-6 form-group">
                                            <label>Alert Level</label>
                                            <input class="form-control" type="number" name="alertLevel" value="<?php echo $shopproduct->alertLevel; ?>" required>
                                          </div>

                                          </div>

                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save Changes</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                        <!-- Modal -->
                                        <div class="modal fade text-left" id="modal-deleterole<?php echo $shopproduct->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            {!! Form::open(['url' => 'deleteshopproduct']) !!}
                                          <div class="modal-content">
                                            <div class="modal-header">
                                            <h4 class="modal-title" id="myModalLabel1">Delete Product</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                            </div>
                                            <div class="modal-body">
                                            <div class="row">
                                            <div class="col-xl-6 col-lg-6 col-md-6">
                                              <input type="hidden" name="id" value="<?php echo $shopproduct->id; ?>" class="form-control" required>
                                          </div>
                                          <h5 style="margin-left:2%;">Confirm that you want to delete this shopproduct</h5>
                                        </div>
                                            </div>
                                            <div class="modal-footer">
                                            <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Delete</button>
                                            </div>
                                          </div>
                                          {!! Form::close() !!}
                                          </div>
                                        </div>

                                      <?php } ?>
                                    </tbody>
                                </table>

                                          </div>
                                        </div>
                                      </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
  </body>

  </html>
