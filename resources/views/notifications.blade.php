<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Notifications</title>
    @include('headerlink')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Notifications</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">List of Notifications</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="ibox">
                          <div class="ibox-head">
                              <div class="ibox-title"><i class="ti-announcement"></i> Notifications</div>
                              <div class="ibox-tools">
                                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-addnotification"><i class="fa fa-plus"></i> Create New</button>
                              </div>
                          </div>
                            <div class="ibox-body">
                                <div class="tab-content">

                                  <!-- Modal -->
                                  <div class="modal fade text-left" id="modal-addnotification" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                    <div class="modal-lg modal-dialog" role="document">
                                      {!! Form::open(['url' => 'addnotification']) !!}
                                    <div class="modal-content">
                                      <div class="modal-header">
                                      <h4 class="modal-title" id="myModalLabel1">Add notification</h4>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                      </div>
                                      <div class="modal-body">
                                      <div class="row">
                                    <div class="col-sm-12 form-group">
                                        <textarea row="6" style="min-height:100px;" class="form-control" placeholder="Your message..." name="message" required></textarea>
                                    </div>
                                  </div>
                                      </div>
                                      <div class="modal-footer">
                                      <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-primary">Submit</button>
                                      </div>
                                    </div>
                                    {!! Form::close() !!}
                                    </div>
                                  </div>

                                      @if (count($errors) > 0)
                                         <div class="alert alert-danger">
                                             <ul>
                                                 @foreach ($errors->all() as $error)
                                                 <li>{{ $error }}</li>
                                                 @endforeach
                                             </ul>
                                         </div>
                                        @endif

                                        @if ($message = Session::get('error'))
                                             <div class="alert alert-danger">
                                                 {{ $message }}
                                             </div>
                                        @endif

                                        @if ($message = Session::get('success'))
                                             <div class="alert alert-success">
                                                 {{ $message }}
                                             </div>
                                        @endif

                                        @if (session('status0'))
                                        <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{ session('status0') }}
                                        </div>
                                        @endif

                                        @if (session('status1'))
                                        <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{ session('status1') }}
                                        </div>
                                        @endif

                                    <div class="tab-pane fade show active" id="tab-3">

                                        <ul class="media-list media-list-divider m-0">
                                          <?php $notifications = \App\Notifications::getAll();
                                          foreach ($notifications as $nt) { ?>
                                            <li class="media">
                                                <div class="media-img"><i class="ti-user font-18 text-muted"></i></div>
                                                <div class="media-body">
                                                    <div class="media-heading"><?php echo $nt->firstName; ?> <?php echo $nt->lastName; ?> <small class="float-right text-muted"><?php echo $nt->created_at->diffForHumans(); ?></small></div>
                                                    <div class="font-13"><?php echo $nt->message; ?></div>
                                                </div>
                                            </li>
                                          <?php } ?>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
  </body>

  </html>
