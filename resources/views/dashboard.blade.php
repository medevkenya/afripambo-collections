<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Dashboard</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-success color-white widget-stat">
                          <a href="{{URL::to('/batches')}}" class="color-white">
                            <div class="ibox-body">
                                <h2 class="m-b-5 font-strong"><?php echo $totaldebtors; ?></h2>
                                <div class="m-b-5">STOCK VALUE</div><i class="fa fa-money widget-stat-icon"></i>
                                <div><i class="fa fa-level-up m-r-5"></i><small>View More</small></div>
                            </div>
                          </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-info color-white widget-stat">
                          <a href="{{URL::to('/suppliers')}}" class="color-white">
                            <div class="ibox-body">
                                <h2 class="m-b-5 font-strong"><?php echo $totalcreditors; ?></h2>
                                <div class="m-b-5">TOTAL CREDIT VALUE</div><i class="fa fa-money widget-stat-icon"></i>
                                <div><i class="fa fa-level-up m-r-5"></i><small>View More</small></div>
                            </div>
                          </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-warning color-white widget-stat">
                          <a href="{{URL::to('/customers')}}" class="color-white">
                            <div class="ibox-body">
                                <h2 class="m-b-5 font-strong"><?php echo $stockvalue; ?></h2>
                                <div class="m-b-5">TOTAL DEBT VALUE</div><i class="fa fa-money widget-stat-icon"></i>
                                <div><i class="fa fa-level-up m-r-5"></i><small>View More</small></div>
                            </div>
                          </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="ibox bg-danger color-white widget-stat">
                          <a href="{{URL::to('/expenses')}}" class="color-white">
                            <div class="ibox-body">
                                <h2 class="m-b-5 font-strong"><?php echo $totalexpenses; ?></h2>
                                <div class="m-b-5">MONTHLY EXPENSES</div><i class="fa fa-money widget-stat-icon"></i>
                                <div><i class="fa fa-level-down m-r-5"></i><small>View More</small></div>
                            </div>
                          </a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class="ibox">
                            <div class="ibox-body">
                                <div class="flexbox mb-4">
                                    <div>
                                        <h3 class="m-0">Statistics</h3>
                                        <div>Your <?php echo  env("APP_NAME"); ?> sales analytics</div>
                                    </div>
                                    <div class="d-inline-flex">
                                        <div class="px-3" style="border-right: 1px solid rgba(0,0,0,.1);">
                                            <div class="text-muted">WEEKLY REVENUE</div>
                                            <div>
                                                <span class="h2 m-0">Ksh. <?php echo $thismonth; ?></span>
                                                <span class="text-success ml-2"><i class="fa fa-money"></i> <?php echo $weeksign."".$wpercentage; ?>%</span>
                                            </div>
                                        </div>
                                        <div class="px-3">
                                            <div class="text-muted">MONTHLY REVENUE</div>
                                            <div>
                                                <span class="h2 m-0">Ksh. <?php echo $thismonth; ?></span>
                                                <span class="text-warning ml-2"><i class="fa fa-money"></i> <?php echo $monthsign."".$mpercentage; ?>%</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <canvas id="bar_chart" style="height:260px;"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Statistics</div>
                            </div>
                            <div class="ibox-body">
                                <div class="row align-items-center">
                                    <div class="col-md-6">
                                        <canvas id="doughnut_chart" style="height:160px;"></canvas>
                                    </div>
                                    <div class="col-md-6">
                                        <a href="{{URL::to('/customers')}}"><div class="m-b-20 text-success"><i class="fa fa-circle-o m-r-10"></i>Customers <span style="float:right;">(<?php echo $customers; ?>)</span></div></a>
                                        <a href="{{URL::to('/suppliers')}}"><div class="m-b-20 text-success"><i class="fa fa-circle-o m-r-10"></i>Suppliers <span style="float:right;">(<?php echo $suppliers; ?>)</span></div></a>
                                        <a href="{{URL::to('/debtors')}}"><div class="m-b-20 text-success"><i class="fa fa-circle-o m-r-10"></i>Debtors <span style="float:right;">(<?php echo $debtors; ?>)</span></div></a>
                                        <a href="{{URL::to('/creditors')}}"><div class="m-b-20 text-success"><i class="fa fa-circle-o m-r-10"></i>Creditors <span style="float:right;">(<?php echo $creditors; ?>)</span></div></a>
                                    </div>
                                </div>
                                <ul class="list-group list-group-divider list-group-full">
                                    <li class="list-group-item">Products Count
                                        <span class="float-right text-success"><i class="fa fa-caret-up"></i> <?php echo $countproducts; ?></span>
                                    </li>
                                    <li class="list-group-item">Shop Products Count
                                        <span class="float-right text-success"><i class="fa fa-caret-up"></i> <?php echo $countshopproducts; ?></span>
                                    </li>
                                    <li class="list-group-item">Sales Count
                                        <span class="float-right text-success"><i class="fa fa-caret-up"></i> <?php echo $countsales; ?></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-lg-8">
                      <div class="ibox">
                          <div class="ibox-head">
                              <div class="ibox-title">Low Stock Products</div>
                              <div class="ibox-tools">
                                  <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                                  <a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-v"></i></a>
                                  <div class="dropdown-menu dropdown-menu-right">
                                      <a href="{{URL::to('/products')}}" class="dropdown-item">Manage Products</a>
                                      <a href="{{URL::to('/batches')}}" class="dropdown-item">Manage Stock</a>
                                  </div>
                              </div>
                          </div>
                          <div class="ibox-body">

                            @if (count($errors) > 0)
                             <div class="alert alert-danger">
                                 <ul>
                                     @foreach ($errors->all() as $error)
                                     <li>{{ $error }}</li>
                                     @endforeach
                                 </ul>
                             </div>
                            @endif

                            @if ($message = Session::get('error'))
                                 <div class="alert alert-danger">
                                     {{ $message }}
                                 </div>
                            @endif

                            @if ($message = Session::get('success'))
                                 <div class="alert alert-success">
                                     {{ $message }}
                                 </div>
                            @endif

                            @if (session('status0'))
                            <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ session('status0') }}
                            </div>
                            @endif

                            @if (session('status1'))
                            <div class="alert alert-success alert-dismissible alertbox" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {{ session('status1') }}
                            </div>
                            @endif

                            <?php $list = \App\Shopproducts::getLowStockAll(0); ?>
                            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Product Name</th>
                                    <th>Shop</th>
                                    <th>Qty</th>
                                    <th width="15%">Actions</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                  <th>Product Name</th>
                                  <th>Shop</th>
                                  <th>Qty</th>
                                  <th width="15%">Actions</th>
                                </tr>
                            </tfoot>
                            <tbody>
                              <?php $lowstock = 0; foreach ($list as $shopproduct) {
                                $quantity = \App\Batches::getProductQuantity($shopproduct->id);
                                if($quantity <= $shopproduct->alertLevel) { $lowstock++;
                                ?>
                                <tr>
                                  <td><?php echo $shopproduct->productName; ?></td>
                                  <td><?php echo $shopproduct->shopName; ?></td>
                                  <td><?php echo $shopproduct->quantity." ".$shopproduct->unit; ?></td>
                                  <td>
                                  <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-edit<?php echo $shopproduct->id; ?>"><i class="fa fa-edit"></i> Edit</button>
                                </td>
                                </tr>

                                <!-- Modal -->
                                    <div class="modal fade text-left" id="modal-edit<?php echo $shopproduct->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
                                      <div class="modal-dialog" role="document">
                                        {!! Form::open(['url' => 'updatequantity']) !!}
                                      <div class="modal-content">
                                        <div class="modal-header">
                                        <h4 class="modal-title" id="myModalLabel1">Re-Stock Product</h4>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body">
                                        <div class="row">
                                          <input type="hidden" name="shopProductId" value="<?php echo $shopproduct->id; ?>" class="form-control" required>
                                          <?php
                                            $suppliers = \App\Suppliers::getAll();
                                          ?>

                                          <div class="col-sm-12 form-group">
                                              <label>Supplier</label>
                                              <select class="form-control" name="supplierId" required>
                                             <option value=""></option>
                                             <?php foreach ($suppliers as $keyfs) { ?>
                                               <option value="<?php echo $keyfs->id; ?>"><?php echo $keyfs->supplierName; ?></option>
                                             <?php } ?>
                                           </select>
                                          </div>

                                          <div class="col-sm-6 form-group">
                                              <label>Quantity</label>
                                              <input class="form-control" type="number" name="quantity" required>
                                          </div>

                                          <div class="col-sm-6 form-group">
                                              <label>Type</label>
                                              <select class="form-control" name="type" required>
                                                <option value=""></option>
                                               <option value="Cash">Cash</option>
                                               <option value="Credit">Credit</option>
                                           </select>
                                          </div>

                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" class="btn grey btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                      </div>
                                      {!! Form::close() !!}
                                      </div>
                                    </div>

                              <?php } } ?>
                            </tbody>
                        </table>

                      <?php if($lowstock == 0) { ?>
                        <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        No products are in low stock!
                        </div>
                      <?php } ?>

                          </div>
                      </div>
                  </div>
                    <div class="col-lg-4">
                        <div class="ibox">
                            <div class="ibox-head">
                                <div class="ibox-title">Notifications</div>
                            </div>
                            <div class="ibox-body">
                                <ul class="media-list media-list-divider m-0">
                                  <?php $notifications = \App\Notifications::getLatest(); foreach ($notifications as $keyn) { ?>
                                    <li class="media">
                                        <a class="media-img" href="javascript:;">
                                            <img class="img-circle" src="{{ URL::to('/') }}/profiles/<?php echo $keyn->profilePic; ?>" width="40" />
                                        </a>
                                        <div class="media-body">
                                            <div class="media-heading"><?php echo $keyn->firstName; ?> <?php echo $keyn->lastName; ?><small class="float-right text-muted"><?php echo $keyn->created_at; ?></small></div>
                                            <div class="font-13"><?php echo $keyn->message; ?></div>
                                        </div>
                                    </li>
                                  <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
</body>

</html>
