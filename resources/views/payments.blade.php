<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title><?php echo env("APP_NAME"); ?> | Payments</title>
    @include('headerlink')
    @include('datatables')
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        @include('header')
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        @include('nav')
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-heading">
                <h1 class="page-title">Payments</h1>
                <ol class="breadcrumb">
                    <li class="breadcrumb-item">
                        <a href="index.html"><i class="la la-home font-20"></i></a>
                    </li>
                    <li class="breadcrumb-item">By default shows today's</li>
                </ol>
            </div>
            <div class="page-content fade-in-up">

                <div class="row">

                        <div class="col-md-12">
                            <div class="ibox">
                                <div class="ibox-head">
                                    <div class="ibox-title">Payments
                                      <?php if(isset($fromdate)) { echo "From: ".$fromdate." To: ".$todate; } ?>
                                    </div>

                                </div>
                                <div class="ibox-body">

                                  @if (count($errors) > 0)
                                         <div class="alert alert-danger">
                                             <ul>
                                                 @foreach ($errors->all() as $error)
                                                 <li>{{ $error }}</li>
                                                 @endforeach
                                             </ul>
                                         </div>
                                        @endif

                                        @if ($message = Session::get('error'))
                                             <div class="alert alert-danger">
                                                 {{ $message }}
                                             </div>
                                        @endif

                                        @if ($message = Session::get('success'))
                                             <div class="alert alert-success">
                                                 {{ $message }}
                                             </div>
                                        @endif

                                        @if (session('status0'))
                                        <div class="alert alert-danger alert-dismissible alertbox" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{ session('status0') }}
                                        </div>
                                        @endif

                                        @if (session('status1'))
                                        <div class="alert alert-success alert-dismissible alertbox" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        {{ session('status1') }}
                                        </div>
                                        @endif

                                        {!! Form::open(['url' => 'paymentsReport']) !!}
                                        {{ csrf_field() }}
                                        <div class="row">

                                        <div class="form-group col-md-3">
                                          <label>From Date</label>
                                          <input type="date" class="form-control" name="fromdate" required>
                                        </div>
                                        <div class="form-group col-md-3">
                                          <label>To Date</label>
                                          <input type="date" class="form-control" name="todate" required>
                                        </div>
                                        <div class="form-group col-md-3">
                                          <label>.</label>
                                          <button type="submit" class="btn btn-block btn-primary">Search</button>
                                        </div>
                                        </div>
                                        {!! Form::close() !!}


                          <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0" width="100%">
                          <thead>
                              <tr>
                                  <th>Appointment No.</th>
                                  <th>Patient</th>
                                  <th>Transaction ID</th>
                                  <th>Reference</th>
                                  <th>Amount</th>
                                  <th>Method</th>
                                  <th>MSISDN</th>
                                  <th>Paid By</th>
                                  <th>Date</th>
                              </tr>
                          </thead>
                          <tfoot>
                              <tr>
                                <th>Appointment No.</th>
                                <th>Patient</th>
                                <th>Transaction ID</th>
                                <th>Reference</th>
                                <th>Amount</th>
                                <th>Method</th>
                                <th>MSISDN</th>
                                <th>Paid By</th>
                                <th>Date</th>
                              </tr>
                          </tfoot>
                          <tbody>
                            <?php foreach ($list as $app) {
                              ?>
                              <tr>
                                <td><a href="<?php $url = URL::to("/viewAppointment/".$app->appointmentId); print_r($url); ?>" class="btn btn-primary"><?php echo $app->appointmentNo; ?></a></td>
                                <td><a href="<?php $url = URL::to("/viewPatient/".$app->patientId); print_r($url); ?>" class="btn btn-primary"><?php echo $app->firstName; ?> <?php echo $app->lastName; ?></a></td>
                                  <td><?php echo $app->transaction_id; ?></td>
                                  <td><?php echo $app->reference; ?></td>
                                  <td><?php echo $app->amount; ?></td>
                                  <td><?php echo $app->method; ?></td>
                                  <td><?php echo $app->MSISDN; ?></td>
                                  <td><?php echo $app->paidBy; ?></td>
                                  <td><?php echo $app->created_at; ?></td>
                              </tr>

                            <?php } ?>
                          </tbody>
                      </table>

                                </div>
                              </div>
                            </div>

                </div>

            </div>
            <!-- END PAGE CONTENT-->
            @include('footer')
        </div>
    </div>
    <!-- BEGIN THEME CONFIG PANEL-->
    @include('config')
    <!-- END THEME CONFIG PANEL-->
    <!-- BEGIN PAGA BACKDROPS-->
    @include('backdrop')
    <!-- END PAGA BACKDROPS-->
    @include('footerlink')
    @include('datatablesfooter')
  </body>

  </html>
