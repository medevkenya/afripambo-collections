<!-- GLOBAL MAINLY STYLES-->
<link href="{{ URL::asset('assets/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
<link href="{{ URL::asset('assets/vendors/themify-icons/css/themify-icons.css')}}" rel="stylesheet" />
<!-- THEME STYLES-->
<link href="{{ URL::asset('assets/css/main.css')}}" rel="stylesheet" />
<!-- PAGE LEVEL STYLES-->
<link href="{{ URL::asset('assets/css/pages/auth-light.css')}}" rel="stylesheet" />
