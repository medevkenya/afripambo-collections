<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="Bootstrap-ecommerce by Vosidiy">
<title><?php echo env("APP_NAME"); ?> POS</title>
<link rel="shortcut icon" type="image/x-icon" href="{{ URL::asset('sales/images/logos/squanchy.jpg')}}" >
<link rel="apple-touch-icon" sizes="180x180" href="{{ URL::asset('sales/images/logos/squanchy.jpg')}}">
<link rel="icon" type="image/png" sizes="32x32" href="{{ URL::asset('sales/images/logos/squanchy.jpg')}}">
<!-- jQuery -->
<!-- Bootstrap4 files-->
<link href="{{ URL::asset('sales/css/bootstrap.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::asset('sales/css/ui.css')}}" rel="stylesheet" type="text/css"/>
<link href="{{ URL::asset('sales/fonts/fontawesome/css/fontawesome-all.min.css')}}" type="text/css" rel="stylesheet">
<link href="{{ URL::asset('sales/css/OverlayScrollbars.css')}}" type="text/css" rel="stylesheet"/>
<!-- Font awesome 5 -->
<style>
	.avatar {
  vertical-align: middle;
  width: 35px;
  height: 35px;
  border-radius: 50%;
}
.bg-default, .btn-default{
	background-color: #f2f3f8;
}
.btn-error{
	color: #ef5f5f;
}
</style>
<!-- custom style -->
</head>
<body>
<section class="header-main">
	<div class="container">
<div class="row align-items-center">
	<div class="col-lg-3">
	<div class="brand-wrap">
		<img class="logo" src="{{ URL::to('/') }}/images/logo.png">
		<h2 class="logo-text"><?php echo env("APP_SHORT_NAME"); ?></h2>
	</div> <!-- brand-wrap.// -->
	</div>
	<div class="col-lg-6 col-sm-6">
		{!! Form::open(['url'=>'search','class'=>'search-wrap']) !!}
 		 {{ csrf_field() }}
			<div class="input-group">
			    <input type="text" class="form-control" name="search" placeholder="Search" required>
			    <div class="input-group-append">
			      <button class="btn btn-primary" type="submit">
			        <i class="fa fa-search"></i>
			      </button>
			    </div>
		    </div>
		</form> <!-- search-wrap .end// -->
	</div> <!-- col.// -->
	<div class="col-lg-3 col-sm-6">
		<div class="widgets-wrap d-flex justify-content-end">
			<div class="widget-header">
				<a href="{{URL::to('/dashboard')}}" class="icontext">
					<a href="{{URL::to('/dashboard')}}" class="btn btn-primary m-btn m-btn--icon m-btn--icon-only">
															<i class="fa fa-home"></i>
														</a>
				</a>
			</div> <!-- widget .// -->
			<div class="widget-header dropdown">
				<a href="#" class="ml-3 icontext" data-toggle="dropdown" data-offset="20,10">
					<img src="{{ URL::to('/') }}/profiles/<?php echo Auth::user()->profilePic; ?>" class="avatar" alt="">
				</a>
				<div class="dropdown-menu dropdown-menu-right">
          <a class="dropdown-item" href="#"><i class="fa fa-user"></i> {{Auth::user()->firstName}} {{Auth::user()->lastName}}</a>
						<a class="dropdown-item" href="{{URL::to('/logout')}}"><i class="fa fa-sign-out-alt"></i> Logout</a>
				</div> <!--  dropdown-menu .// -->
			</div> <!-- widget  dropdown.// -->
		</div>	<!-- widgets-wrap.// -->
	</div> <!-- col.// -->
</div> <!-- row.// -->
	</div> <!-- container.// -->
</section>

<?php $categories = \App\Categories::getAll(); ?>

<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content padding-y-sm bg-default ">
<div class="container-fluid">
<div class="row">
	<div class="col-md-7 card padding-y-sm card ">
		<ul class="nav bg radius nav-pills nav-fill mb-3 bg" role="tablist">
	<li class="nav-item">
		<a class="nav-link active show" href="{{URL::to('/sales')}}">
		<i class="fa fa-tags"></i> All</a></li>
    <?php foreach ($categories as $keyct) { ?>
      <li class="nav-item">
    		<a class="nav-link" href="<?php $url = URL::to("/sales?id=".$keyct->id); print_r($url); ?>">
    		<i class="fa fa-tags "></i>  <?php echo $keyct->categoryName; ?></a></li>
    <?php } ?>
</ul>
<span   id="items">
<div class="row">

<?php foreach ($list as $item) {
$quantity = \App\Batches::getProductQuantity($item->id);
   ?>
<div class="col-md-3">
	<figure class="card card-product">
		<span class="badge-new"> <span id="availableStock<?php echo $item->id; ?>"><?php echo $quantity; ?></span> In Stock</span>
		<div class="img-wrap">
			<img src="{{ URL::to('/') }}/public/photos/<?php echo $item->photoUrl; ?>">
			<a class="btn-overlay" href="#"><i class="fa fa-search-plus"></i> <?php echo $item->categoryName; ?> - <?php echo $item->brandName; ?></a>
		</div>
		<figcaption class="info-wrap">
			<a href="#" class="title"><?php echo $item->productName; ?></a>
			<div class="action-wrap">
				<button class="btn btn-primary btn-sm float-right my-cart-btn" data-id="<?php echo $item->id; ?>" data-name="<?php echo $item->productName; ?>" data-summary="<?php echo $item->categoryName; ?>" data-cost="<?php echo number_format($item->cost,0); ?>" data-quantity="<?php echo $quantity; ?>" data-image="{{ URL::to('/') }}/public/photos/<?php echo $item->photoUrl; ?>"> <i class="fa fa-cart-plus"></i> Add </button>
				<div class="price-wrap h5">
					<span class="price-new">Ksh. <?php echo $item->cost; ?></span>
				</div> <!-- price-wrap.// -->
			</div> <!-- action-wrap -->
		</figcaption>
	</figure> <!-- card // -->
</div> <!-- col // -->
<?php } ?>
{{ $list->links() }}
</div> <!-- row.// -->

</span>
	</div>
	<div class="col-md-5">
<div class="card">
	<span id="cart">

		<div class="ajax-loader">
			<img src="{{ URL::to('/') }}/public/images/loading.gif" class="img-responsive" />
		</div>

<table class="table table-hover shopping-cart-wrap">
<thead class="text-muted">
<tr>
  <th scope="col" width="60%">Item</th>
  <th scope="col">Qty</th>
  <th scope="col">Price</th>
  <!-- <th scope="col" class="text-right" width="200">Delete</th> -->
</tr>
</thead>
<tbody id="cartMain">
</tbody>
</table>
</span>
</div> <!-- card.// -->
<div class="box">
<dl class="dlist-align">
  <dt>Tax: </dt>
  <dd class="text-right">Ksh. <span id="taxamount">0</span> (<span id="tax">0</span>%)</dd>
</dl>
<dl class="dlist-align">
  <dt>Discount:</dt>
  <dd class="text-right">Ksh. <input type="number" value="0" id="discount" style="width:20%"></dd>
</dl>
<dl class="dlist-align">
  <dt>Sub Total:</dt>
  <dd class="text-right">Ksh. <span id="subTotal">0</span></dd>
</dl>
<dl class="dlist-align">
  <dt>Total: </dt>
  <dd class="text-right h4 b"> Ksh. <span id="totalCharge">0</span> </dd>
</dl>
<div class="row">
	<div class="col-md-3">
		<a href="{{URL::to('/sales')}}" class="btn  btn-default btn-error btn-lg btn-block"><i class="fa fa-times-circle "></i> Cancel </a>
	</div>
	<div class="col-md-6">
		<button class="btn  btn-primary btn-lg btn-block" id="completePrint"><i class="fa fa-check-circle"></i> Complete</button>
	</div>
	 <div class="col-md-3">
		<button class="btn  btn-warning btn-lg btn-block" id="btnPrint" class="hidden-print"><i class="fa fa-print"></i> Print </button>
	</div>
</div>
</div> <!-- box.// -->
	</div>
</div>
</div><!-- container //  -->
</section>

<!-- Modal -->
<div class="modal fade" id="validationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- <div class="modal-header bg-success">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Completed</h4>
      </div> -->
      <div class="modal-body">
        <p>Completed sale successfully</p>
      </div>
      <div class="modal-footer">
        <a href="{{URL::to('/sales')}}" class="btn btn-default">Close</a>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="notenoughtstock" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <!-- <div class="modal-header bg-success">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Completed</h4>
      </div> -->
      <div class="modal-body">
        <p>Not enough stock for this product</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>

<div class="ticket" id="ticket" style="display:none;">
            <img src="{{ URL::to('/') }}/images/<?php echo $settings->companyPhoto; ?>" alt="<?php echo env("APP_NAME"); ?>">
            <p class="centered"><?php echo strtoupper($settings->companyName); ?>
                <br>Email: <?php echo $settings->companyEmail; ?>
                <br>Phone: <?php echo $settings->companyTelephone; ?></p>
            <table>
                <thead>
                    <tr>
                        <th class="quantity">Qty</th>
                        <th class="description">Description</th>
                        <th class="price">Ksh.</th>
                    </tr>
                </thead>
                <tbody id="cartMainPrint">

                </tbody>
								<tfoot>
									<tr>
											<td class="quantity"></td>
											<td class="description">TAX</td>
											<td class="price">Ksh. <span id="taxamountPrint">0</span> (<span id="taxPrint">0</span>%)</td>
									</tr>
									<tr>
											<td class="quantity"></td>
											<td class="description">DISCOUNT</td>
											<td class="price">Ksh. <span id="discountPrint">0</span></td>
									</tr>
									<tr>
											<td class="quantity"></td>
											<td class="description">SUB TOTAL</td>
											<td class="price">Ksh. <span id="subTotalPrint">0</span></td>
									</tr>
									<tr>
											<td class="quantity"></td>
											<td class="description">TOTAL</td>
											<td class="price">Ksh. <span id="totalChargePrint">0</span></td>
									</tr>
								</tfoot>
            </table>
            <p class="centered">Thanks for your purchase!<br><?php echo $settings->companyName; ?></p>
        </div>

<!-- ========================= SECTION CONTENT END// ========================= -->
<script src="{{ URL::asset('sales/js/jquery-2.0.0.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('sales/js/bootstrap.bundle.min.js')}}" type="text/javascript"></script>
<script src="{{ URL::asset('sales/js/OverlayScrollbars.js')}}" type="text/javascript"></script>
<script type="text/javascript">
$.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
</script>
<script>
	$(function() {
	//The passed argument has to be at least a empty object or a object with your desired options
	//$("body").overlayScrollbars({ });
	$("#items").height(552);
	$("#items").overlayScrollbars({overflowBehavior : {
		x : "hidden",
		y : "scroll"
	} });
	$("#cart").height(445);
	$("#cart").overlayScrollbars({ });
});
$(document).on('click', '.my-cart-btn', function() {
  var productId = $(this).data('id');
  var cost = $(this).data('cost');
  var productName = $(this).data('name');
  var image = $(this).data('image');
	var instock = $(this).data('quantity');

	var availableStock = document.getElementById("availableStock"+productId+"").innerHTML;

	if(availableStock == 0 || instock == 0)
	{
		$('#notenoughtstock').modal('show');
		//alert("Not enough stock for this product");
	}
	else
	{

	document.getElementById("availableStock"+productId+"").innerHTML = availableStock - 1;

  var newarrayitem = {'productId':productId,'productName':productName,'cost':cost,'image':image};
  var cartitems = [];
  cartitems.push(newarrayitem);

  var main_site="{{ url('/') }}";
  var _token = $('meta[name="csrf-token"]').attr('content');
  var dataString = 'productId='+productId+'&productName='+productName+'cost='+cost+'&_token='+_token;
  jQuery.ajax({
      url:main_site+'/savecart',
      data: dataString,
      type: "POST",
      success: function(data){
          //console.log("js savecart--"+JSON.stringify(data));
          //$('#upvoteId'+id).html(data);
          if(data.status==1) {
            refreshCart();
          }
      },
      error: function (){

      }
  });
}
});


function refreshCart() { console.log("loading cart...");
    $.ajax({
        url: "{{ route('getCart') }}",
        method: 'GET',
        success: function(data) {
					console.log("refreshCart---"+JSON.stringify(data));
					if(data.total > 0) {
          $("#totalCharge").html(parseInt(data.total));
					$("#subTotal").html(parseInt(data.subtotal));
					$("#discount").html(parseInt(data.discount));
					$("#tax").html(parseInt(data.tax));
					$("#taxamount").html(parseInt(data.taxamount));
          prepareCart(data.data);
				}
				else {
					document.getElementById("cartMain").innerHTML = '<tr><td><div class="alert alert-danger alert-dismissible alertbox" role="alert">'+
					'Your cart is empty!'+
					'</div></td></tr>';
				}
        }
    });
  }



$(document).ready(function() {
  refreshCart();
});

function prepareCart(data) {

var main_site="{{ url('/') }}";

$('#cartMain').empty();

  for (var key in data) {
  if (data.hasOwnProperty(key)) {
    var val = data[key];
		//console.log("prepareCart photoUrl..."+main_site+"/public/"+val.photoUrl);
    var id = val.id;
    var itemName = val.itemName;
    var productId = val.productId;
    var cost = val.cost;
    var quantity = val.quantity;
    var photoUrl = val.photoUrl;
    var totalCost = val.totalCost;
		var tt = parseInt(totalCost);

    var liData = '<tr><td><figure class="media">'+
			'<div class="img-wrap"><img src="'+main_site+'/public/photos/'+photoUrl+'" class="img-thumbnail img-xs"></div>'+
			'<figcaption class="media-body"><h6 class="title" style="width:90% !important">'+itemName+' </h6></figcaption></figure></td>'+
			'<td class="text-center">'+
				'<div class="m-btn-group m-btn-group--pill btn-group mr-2" role="group" aria-label="...">'+
					'<button type="button" class="m-btn btn btn-default" onclick="decreaseCart('+productId+')"><i class="fa fa-minus"></i></button>'+
					'<button type="button" class="m-btn btn btn-default" disabled>'+quantity+'</button>'+
					'<button type="button" class="m-btn btn btn-default" onclick="increaseCart('+productId+')"><i class="fa fa-plus"></i></button></div>'+
			'</td><td><div class="price-wrap"><var class="price">'+tt+'</var></div></td>'+
			//'<td class="text-right"><a href="" class="btn btn-outline-danger"> <i class="fa fa-trash"></i></a></td>'+
			'</tr>';

    $('#cartMain').append(liData);

  }
}
}

function prepareCartPrint(data) {

var main_site="{{ url('/') }}";

$('#cartMainPrint').empty();

  for (var key in data) {
  if (data.hasOwnProperty(key)) {
    var val = data[key];
		//console.log("prepareCart photoUrl..."+main_site+"/public/"+val.photoUrl);
    var id = val.id;
    var itemName = val.itemName;
    var productId = val.productId;
    var cost = val.cost;
    var quantity = val.quantity;
    var photoUrl = val.photoUrl;
    var totalCost = val.totalCost;
		var tt = parseInt(totalCost);

    var liData = '<tr><td class="quantity">'+quantity+'</td><td class="description">'+itemName+'</td><td class="price">Ksh. '+totalCost+'</td></tr>';

    $('#cartMainPrint').append(liData);

  }
}

document.getElementById('ticket').style.display = "block";

var divToPrint = document.getElementById('ticket');
var htmlToPrint = '' +
		'<style type="text/css">' +
		'* { font-size: 12px;font-family: Times New Roman;}' +
'td,th,tr,table { border-top: 1px solid black; border-collapse: collapse;}td.description,th.description { width: 75px; max-width: 75px;}' +
'td.quantity,th.quantity { width: 40px; max-width: 40px; word-break: break-all;}' +
'td.price,th.price { width: 40px; max-width: 40px; word-break: break-all;}' +
'.centered { text-align: center; align-content: center;}.ticket { width: 155px; max-width: 155px;}img { max-width: inherit; width: inherit;}'+
		'</style>';
htmlToPrint += divToPrint.outerHTML;
newWin = window.open("");
//newWin.document.write("<h3 align='center'>Print Page</h3>");
newWin.document.write(htmlToPrint);
newWin.print();
newWin.close();

document.getElementById('ticket').style.display = "none";

}

</script>
<script>
$(document).ready(function(){
    //$("#discount").on("input", function(){
			$("#discount").on("keyup", function(event) {
        // Print entered value in a div box
        var discount = $(this).val();
				var subtotal = document.getElementById("subTotal").innerHTML;
				var totalCharge = document.getElementById("totalCharge").innerHTML;
				var tax = document.getElementById("tax").value;
				var taxamount = document.getElementById("taxamount").innerHTML;
//alert(subtotal);
				//if(subtotal > discount) {
				var newsubtotal = +subtotal - +discount;
				var newtotal = +newsubtotal + +taxamount;
				$("#totalCharge").html(parseInt(newtotal));
				$("#subTotal").html(parseInt(newsubtotal));
			// }
			// else {
			// 	alert("Invalid discount");
			// 	$("#discount").html("0");
			// }
    });
});

function increaseCart(productId) {
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  $.ajax({
            url: "{{ route('increaseCart') }}",
            type: 'POST',
            data: {_token: CSRF_TOKEN, productId:productId},
            dataType: 'JSON',
            success: function (data) {
							  var availableStock = document.getElementById("availableStock"+productId+"").innerHTML;
							  document.getElementById("availableStock"+productId+"").innerHTML = availableStock - 1;
                refreshCart();
            },
            error: function(request, status, error) {
                console.log("error");
            }
       });
}

function decreaseCart(productId) { console.log("decreaseCart---productId--"+productId);
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  $.ajax({
            url: "{{ route('decreaseCart') }}",
            type: 'POST',
            data: {_token: CSRF_TOKEN, productId:productId},
            dataType: 'JSON',
            success: function (data) {
							  var availableStock = document.getElementById("availableStock"+productId+"").innerHTML;
								document.getElementById("availableStock"+productId+"").innerHTML = parseInt(+availableStock + 1);
                refreshCart();
            },
            error: function(request, status, error) {
                console.log("error");
            }
       });
}

$(document).ready(function(){
		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
		$("#completePrint").click(function(){

			var discount = document.getElementById("discount").innerHTML;
			var subtotal = document.getElementById("subTotal").innerHTML;
			var totalCharge = document.getElementById("totalCharge").innerHTML;
			var tax = document.getElementById("tax").value;
			var taxamount = document.getElementById("taxamount").innerHTML;
			//console.log("subscribe clicked---"+email+"---");

			$('.ajax-loader').css("visibility", "visible");

			$.ajax({
					url: "{{ route('completePrint') }}?discount=" + discount+"&subtotal="+subtotal+"&total="+totalCharge+"&tax="+tax+"&taxamount="+taxamount,
					method: 'GET',
					success: function(data) {
							//console.log("js html--"+data.message);
							$('.ajax-loader').css("visibility", "hidden");

							$("#totalCharge").html(0);
							$("#subTotal").html(0);
							$("#discount").html(0);
							$("#tax").html(0);
							$("#taxamount").html(0);

							refreshCart();

							$('#validationModal').modal('show');
											//return false;

							// document.getElementById("completemessage").innerHTML = '<div class="alert alert-success alert-dismissible alertbox" role="alert">'+
							// 'Completed sale successfully. Printed receipt'+
							// '</div>';

					},
					complete: function(){
						$('.ajax-loader').css("visibility", "hidden");
					}
			});

		});
});

// function refreshCartPrint() {
//
// 	}

const $btnPrint = document.querySelector("#btnPrint");
$btnPrint.addEventListener("click", () => {
    //window.print();

		$.ajax({
				url: "{{ route('getCart') }}",
				method: 'GET',
				success: function(data)
				{

					$("#totalChargePrint").html(parseInt(data.total));
					$("#subTotalPrint").html(parseInt(data.subtotal));
					$("#discountPrint").html(parseInt(data.discount));
					$("#taxPrint").html(parseInt(data.tax));
					$("#taxamountPrint").html(parseInt(data.taxamount));
					prepareCartPrint(data.data);

				}
		});

});

// $(document).ready(function(){ //https://parzibyte.me/blog/en/2019/10/10/print-receipt-thermal-printer-javascript-css-html/
// 		var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
// 		$("#printonly").click(function(){
//
// 			var discount = document.getElementById("discount").innerHTML;
// 			var subtotal = document.getElementById("subTotal").innerHTML;
// 			var totalCharge = document.getElementById("totalCharge").innerHTML;
// 			var tax = document.getElementById("tax").value;
// 			var taxamount = document.getElementById("taxamount").innerHTML;
// 			//console.log("subscribe clicked---"+email+"---");
//
// 			$('.ajax-loader').css("visibility", "visible");
//
// 			$.ajax({
// 					url: "{{ route('completePrint') }}?discount=" + discount+"&subtotal="+subtotal+"&total="+totalCharge+"&tax="+tax+"&taxamount="+taxamount,
// 					method: 'GET',
// 					success: function(data) {
// 							//console.log("js html--"+data.message);
// 							$('.ajax-loader').css("visibility", "hidden");
//
// 							$("#totalCharge").html(0);
// 							$("#subTotal").html(0);
// 							$("#discount").html(0);
// 							$("#tax").html(0);
// 							$("#taxamount").html(0);
//
// 							refreshCart();
//
// 							$('#validationModal').modal('show');
// 											//return false;
//
// 							// document.getElementById("completemessage").innerHTML = '<div class="alert alert-success alert-dismissible alertbox" role="alert">'+
// 							// 'Completed sale successfully. Printed receipt'+
// 							// '</div>';
//
// 					},
// 					complete: function(){
// 						$('.ajax-loader').css("visibility", "hidden");
// 					}
// 			});
//
// 		});
// });
</script>
</body>
</html>
