function whatsapp(url,title)
  {
    var res = encodeURI(title);
    var urlencoded = encodeURI(url);
    var url = "https://api.whatsapp.com/send/?phone&text="+res+"+"+urlencoded+"&app_absent=0";
    var win = window.open(url, '_blank');
    win.focus();
  }

  function facebook(url,title)
  {
    var res = encodeURI(title);
    var url = "https://www.facebook.com/sharer/sharer.php?u="+url+"";
    var win = window.open(url, '_blank');
    win.focus();
  }

  function twitter(url,title)
  {
    var res = encodeURI(title);
    var urlencoded = encodeURI(url);
    var url = "https://twitter.com/intent/tweet?text="+res+"%20"+url+"%20via%20@mapenzidigital";
    var win = window.open(url, '_blank');
    win.focus();
  }
